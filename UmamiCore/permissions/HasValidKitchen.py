from rest_framework.permissions import IsAuthenticated
from django.utils import timezone
from UmamiCore.models import KitchenVerification


class HasValidKitchen(IsAuthenticated):
    """ Add the requirement that the authenticated user be a chef. """

    def has_permission(self, request, view):
        return (
            super().has_permission(request, view)
            and KitchenVerification.objects.filter(kitchen__chef=request.user)
            .exclude(date_expires__lte=timezone.now())
            .exists()
        )
