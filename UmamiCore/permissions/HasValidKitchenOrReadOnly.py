from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.permissions import SAFE_METHODS
from django.utils import timezone
from UmamiCore.models import KitchenVerification


class HasValidKitchenOrReadOnly(IsAuthenticatedOrReadOnly):
    """ Add the requirement that the authenticated user be a chef. """

    def has_permission(self, request, view):
        if request.method in SAFE_METHODS:
            return True
        else:
            return (
                super().has_permission(request, view)
                and KitchenVerification.objects.filter(kitchen__chef=request.user)
                .exclude(date_expires__lte=timezone.now())
                .exists()
            )
