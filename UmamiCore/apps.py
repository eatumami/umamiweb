from django.apps import AppConfig


class UmamiCoreConfig(AppConfig):
    name = "UmamiCore"
