from allauth.account.adapter import DefaultAccountAdapter
from allauth.socialaccount.adapter import DefaultSocialAccountAdapter
from allauth.account.models import EmailAddress


class AccountAdapter(DefaultAccountAdapter):
    def __init__(self, request=None):
        super().__init__(request)

    def save_user(self, request, user, form, commit=True):
        """ In addition to the email, first_name, and last_name fields (taken
            care of in super().save_user, we need to add phone. """
        user = super().save_user(request, user, form, commit=False)
        data = form.cleaned_data
        phone = data.get("phone")
        user.phone = phone
        if commit:
            user.save()
        return user


class MySocialAccountAdapter(DefaultSocialAccountAdapter):
    def pre_social_login(self, request, sociallogin):
        # Ignore existing social accounts, just do this stuff for new ones
        if sociallogin.is_existing:
            return

        # some social logins don't have an email address, e.g. facebook accounts
        # with mobile numbers only, but allauth takes care of this case so just
        # ignore it
        if "email" not in sociallogin.account.extra_data:
            return

        try:
            email = sociallogin.account.extra_data["email"].lower()
            email_address = EmailAddress.objects.get(email__iexact=email)

            # if it does not, let allauth take care of this new social account
        except EmailAddress.DoesNotExist:
            return
        user = email_address.user
        sociallogin.connect(request, user)
