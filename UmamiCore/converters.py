from rest_framework.urlpatterns import register_converter
from django.conf import settings

HASHID_PATTERN = settings.HASHID_FIELD_ALPHABET + settings.HASHID_FIELD_ALPHABET.upper()


class HashidsConverter(object):
    regex = "[{}]+".format(HASHID_PATTERN)

    def to_python(self, value):
        return value.lower()

    def to_url(self, value):
        return value


class HashidsListConverter(object):
    regex = "([{}]+)(,[{}]+)*".format(HASHID_PATTERN, HASHID_PATTERN)

    def to_python(self, value):
        return str(value.lower()).split(",")

    def to_url(self, value):
        return ",".join(value)


# Feels sloppy. Don't even know if its right
_REGISTERED = False


def register_converters():
    global _REGISTERED
    if not _REGISTERED:
        register_converter(HashidsConverter, "hashid")
        register_converter(HashidsListConverter, "hashid_list")
        _REGISTERED = True
