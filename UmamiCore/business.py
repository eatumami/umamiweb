""" This module lies between the views and the models. """

import UmamiCore.models as models
import logging
import stripe

from collections import defaultdict, namedtuple
from contextlib import ExitStack
from django.conf import settings
from django.core.exceptions import ValidationError
from django.db import transaction

from django.utils.translation import gettext_lazy as _
from djmoney.money import Money

stripe.api_key = settings.STRIPE_SECRET_KEY
logger = logging.getLogger(__name__)


@transaction.atomic()
def process_transaction(transactable, token, user):
    """ Perform the transaction described in order_data for the given user.
        """
    with ExitStack() as on_fail:
        # Claim ownership of the transaction's resources.
        logger.info("Claiming transaction resources.")
        resource = transactable.accquire()
        on_fail.callback(transactable.release, resource)
        on_fail.callback(
            logger.error, "Releasing transaction resources due to an error."
        )

        # Update customer data
        #
        # There should be support for more customer actions. They should be able
        # to add payment options and select the payment option that they want to
        # use. How can we support that?

        logger.info("Updating customer data.")

        # token could be a Stripe charge token from a new card, or a card_id from a card
        customer_m, created = models.Customer.objects.get_or_create(user=user)
        card_token = token

        # We cannot use the token twice so after customer creation we get their default card

        if created:
            logger.info("Created customer object. This is a new customer.")
            stripe_customer = stripe.Customer.create(source=token, email=user.email)
            card_token = stripe_customer.default_source
            customer_m.stripe_customer_token = stripe_customer.id
            customer_m.save()

        # We always use the Stripe card_id to make a charge, since we always want to save the payment method
        card_token = _create_stripe_card_for_customer(customer_m, card_token)

        charges = []
        # Create the charge
        for order in transactable.delivery_orders:
            charge = _create_stripe_charge(
                transactable=order,
                card_token=card_token,
                buyer_token=customer_m.stripe_customer_token,
                seller_token=order.seller.stripe_account_token,
            )
            charges.append((charge, order, True))
        for order in transactable.pickup_orders:
            charge = _create_stripe_charge(
                transactable=order,
                card_token=card_token,
                buyer_token=customer_m.stripe_customer_token,
                seller_token=order.seller.stripe_account_token,
            )
            charges.append((charge, order, False))

        # If we made it this far, the resources are no longer available for
        # purchase, and the customer was charged for the items.
        #
        # Any error going forward is only an issue with the receipts. Instead`
        # of automatically issuing a refund we should probably just manually
        # try to recon the recept from the logs.
        #
        # On the other hand if the recept is messed up how does the kitchen find out
        # that they need to cook something?
        #
        persisted_list = []
        for charge, order, delivery in charges:
            on_fail.callback(charge.refund)
            on_fail.callback(logger.error, "Issuing a refund due to an error.")
            # on_fail.pop_all()

            # Save result into the DB
            logger.info("Saving transaction record.")
            persisted = order.persist(charge.id, delivery)
            persisted_list.append(persisted)
            on_fail.callback(order.abandon, persisted)
            on_fail.callback(
                logger.error, "Removing transaction record due to an error."
            )

            # Everything went right! No need to undo anything.
            on_fail.pop_all()
        return persisted_list


def process_refund(order, amount=None):
    args = {
        "charge": order.stripe_charge_token,
        "amount": round(amount * 100),
        "reverse_transfer": True,
    }
    refund = stripe.Refund.create(**args)
    logger.info("Refund processed. Refund ID is", refund.id)
    order.set_refunded_amount(refund.amount)
    order.save()
    return refund


def _create_stripe_card_for_customer(customer_m, token):
    if token.startswith("tok_"):
        stripe_customer = stripe.Customer.retrieve(customer_m.stripe_customer_token)
        stripe_card = stripe_customer.sources.create(card=token)
        return stripe_card.id
    elif token.startswith("card_"):
        return token


def _create_stripe_charge(transactable, card_token, buyer_token, seller_token):
    """ Charge the buyer on behalf of the seller for the given transactable.
        """
    args = dict(
        amount=transactable.total_in_cents(),
        currency=transactable.currency(),
        source=card_token,
        customer=buyer_token,
        transfer_data=dict(
            destination=seller_token, amount=transactable.total_minus_fees_in_cents()
        ),
        on_behalf_of=seller_token,
        description=transactable.description(),
    )

    logger.info("Telling Stripe to create a charge with the arguments {}", args)
    charge = stripe.Charge.create(**args)
    logger.info('Charge was accepted. The charge ID is "{}"', charge.id)
    return charge
