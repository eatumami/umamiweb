# Generated by Django 2.1.2 on 2019-04-26 03:34

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [("UmamiCore", "0015_auto_20190426_0333")]

    operations = [
        migrations.AlterField(
            model_name="offer",
            name="deliver_time",
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.PROTECT,
                related_name="delivery_offers",
                to="UmamiCore.OfferTime",
            ),
        ),
        migrations.AlterField(
            model_name="offer",
            name="pickup_time",
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.PROTECT,
                related_name="pickup_offers",
                to="UmamiCore.OfferTime",
            ),
        ),
    ]
