# Generated by Django 2.1.2 on 2019-05-19 06:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [("UmamiCore", "0025_auto_20190519_0613")]

    operations = [
        migrations.RemoveField(model_name="offeravailable", name="total_available"),
        migrations.AddField(
            model_name="offer",
            name="total_available",
            field=models.PositiveSmallIntegerField(default=10),
            preserve_default=False,
        ),
    ]
