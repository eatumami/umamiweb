# Generated by Django 2.1.2 on 2019-06-29 16:45

from django.db import migrations
import hashid_field.field


class Migration(migrations.Migration):

    dependencies = [("UmamiCore", "0038_auto_20190629_1632")]

    operations = [
        migrations.AlterField(
            model_name="offer",
            name="id",
            field=hashid_field.field.HashidAutoField(
                alphabet="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890",
                min_length=7,
                primary_key=True,
                serialize=False,
            ),
        )
    ]
