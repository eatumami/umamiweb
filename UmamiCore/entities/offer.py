import datetime

import UmamiCore.models as models
from collections import defaultdict, OrderedDict

from UmamiCore.helpers import combine_date_and_time
from django.contrib.gis.db.models.functions import Distance
from django.contrib.gis.measure import D
from django.core.exceptions import ValidationError
from django.db import transaction
from django.db.models import Count, F, Q
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from . import dish as dish
from . import kitchen as kitchen

from UmamiSite.helper.OfferHelper import *

MAX_DISH_IN_OFFER = 3
OFFER_DAYS_LIMIT = 14


class Offer(object):
    def __init__(
        self,
        kitchen=None,
        dishes=None,
        _model=None,
        deliver_time=None,
        pickup_time=None,
    ):
        if _model is not None:
            self._init_with_model(_model)
        else:
            self._init_with_init_values(kitchen, dishes, deliver_time, pickup_time)

    def _init_with_model(self, _model):
        self._model = _model
        self._dishes = None
        self._dishes_count = self._model.dishes.count()
        self._dirty = False

    def _init_with_init_values(self, kitchen, dishes, deliver_time, pickup_time):
        dishes = set(dishes)
        dishes_count = len(dishes)
        # Enough dishes?
        if dishes_count == 0:
            raise ValidationError(_("Offers must contain at least one dish."))
        # Dishes belong to kitchen?
        valid_count = len(
            dish.Dish.fetch_with_chef_and_pks(chef=kitchen.chef, pks=dishes)
        )
        if dishes_count != valid_count:
            raise ValidationError(_("The given dishes are invalid"))
        if valid_count > MAX_DISH_IN_OFFER:
            raise ValidationError(
                _(
                    f"Offers can only contain a max of {MAX_DISH_IN_OFFER} dishes at a time"
                )
            )
        # Ok.
        self._model = models.Offer(kitchen=kitchen.model)
        self._dishes = dish.Dish.fetch_with_pks(dishes)
        self._dishes_count = dishes_count
        self.pickup_time = pickup_time
        self.deliver_time = deliver_time
        self._dirty = True

    @property
    def model(self):
        return self._model

    @property
    def pk(self):
        return self._model.pk

    def _get_name(self):
        if self.is_bundle and self._model.name:
            return self._model.name.title()
        return self.dishes0.name.title()

    def _set_name(self, name):
        self._model.name = name.title()
        self._dirty = True

    name = property(_get_name, _set_name)

    def _get_price(self):
        return self._model.price.amount if self.is_bundle else self.dishes0.price

    def _set_price(self, price):
        self._model.price = price
        self._dirty = True

    price = property(_get_price, _set_price)

    @property
    def kitchen(self):
        return kitchen.Kitchen(_model=self._model.kitchen)

    @property
    def dishes0(self):
        return self.dishes[0]

    @property
    def is_deleted(self):
        return self._model.is_deleted

    @property
    def dishes(self):
        if self._dishes is None:
            self._dishes = [dish.Dish(_identity=d) for d in self._model.dishes.all()]
        return self._dishes

    def _get_availables(self):
        return self._model.availables

    availables = property(_get_availables)

    def _set_total_available(self, total_available):
        self._model.total_available = total_available
        self._dirty = True

    def _get_total_available(self):
        return self._model.total_available

    total_available = property(_get_total_available, _set_total_available)

    def _get_date(self):
        return self._model.date

    def _set_date(self, date):
        self._model.date = date
        self._dirty = True

    date = property(_get_date, _set_date)

    def _get_lead_time(self):
        return self._model.lead_time

    def _set_lead_time(self, lead_time):
        self._model.lead_time = lead_time
        self._dirty = True

    lead_time = property(_get_lead_time, _set_lead_time)

    def _get_end_date(self):
        return self._model.end_date

    def _set_end_date(self, end_date):
        self._model.end_date = end_date
        self._dirty = True

    end_date = property(_get_end_date, _set_end_date)

    def _get_can_deliver(self):
        return self._model.can_deliver

    def _set_can_deliver(self, can_deliver):
        self._model.can_deliver = can_deliver
        self._dirty = True

    can_deliver = property(_get_can_deliver, _set_can_deliver)

    def _get_deliver_time(self):
        return self._model.deliver_time

    def _set_deliver_time(self, deliver_time):
        if not deliver_time:
            return
        if isinstance(deliver_time, models.OfferTime):
            self._model.deliver_time = deliver_time
        else:
            self._model.deliver_time = models.OfferTime.objects.create(
                kitchen_id=self.kitchen.pk,
                start=deliver_time["start"],
                end=deliver_time["end"],
            )
        self._dirty = True

    deliver_time = property(_get_deliver_time, _set_deliver_time)

    def _get_can_pickup(self):
        return self._model.can_pickup

    def _set_can_pickup(self, can_pickup):
        self._model.can_pickup = can_pickup
        self._dirty = True

    can_pickup = property(_get_can_pickup, _set_can_pickup)

    def _get_pickup_time(self):
        return self._model.pickup_time

    def _set_pickup_time(self, pickup_time):
        if not pickup_time:
            return
        if isinstance(pickup_time, models.OfferTime):
            self._model.pickup_time = pickup_time
        else:
            self._model.pickup_time = models.OfferTime.objects.create(
                kitchen_id=self.kitchen.pk,
                start=pickup_time["start"],
                end=pickup_time["end"],
            )
        self._dirty = True

    pickup_time = property(_get_pickup_time, _set_pickup_time)

    def _get_order_by(self):
        return self._model.order_by

    def _set_order_by(self, order_by):
        self._model.order_by = order_by
        self._dirty = True

    order_by = property(_get_order_by, _set_order_by)

    def set_order_by(self):
        if self._model.pickup_time and self._model.deliver_time:
            start_times = [
                self._model.pickup_time.start,
                self._model.deliver_time.start,
            ]
        elif self._model.pickup_time:
            start_times = [self._model.pickup_time.start]
        else:
            start_times = [self._model.deliver_time.start]
        time_string = str(min(time for time in start_times if time is not None))
        order_by = datetime.datetime.strptime(time_string, "%H:%M:%S") - self.lead_time
        self._model.order_by = order_by.time()

    @property
    def is_bundle(self):
        return self._dishes_count > 1

    def _get_is_recurring(self):
        return self._model.is_recurring

    def _set_is_recurring(self, is_recurring):
        self._model.is_recurring = is_recurring
        self._dirty = True

    is_recurring = property(_get_is_recurring, _set_is_recurring)

    def _get_repeat_every(self):
        return self._model.repeat_every

    def _set_repeat_every(self, repeat_every):
        self._model.repeat_every = repeat_every
        self._dirty = True

    repeat_every = property(_get_repeat_every, _set_repeat_every)

    @property
    def is_expired(self):
        if not self.is_recurring:
            return (
                    combine_date_and_time(self.date, self.order_by) < timezone.now()
            )
        return (
                combine_date_and_time(self.end_date, self.order_by) < timezone.now()
        )

    def is_available_for_order(self, handoff_date):
        future_available_dates = get_future_available_date_with_display(
            vars(self._model)
        )
        client_adjusted_server_time = get_current_client_adjusted_datetime()
        date_strings = [date["name"] for date in future_available_dates]

        if handoff_date.strftime("%Y-%m-%d") not in date_strings:
            # Handoff date is not in any of the available dates
            return False
        if handoff_date == client_adjusted_server_time.date():
            # Handoff date is today, check if time is past order by time
            return self.order_by > client_adjusted_server_time.time()
        return True

    def copy_to(self, date):
        """ Create a copy of this Offer on the given date. """
        # It would be nice if the ctor accepted the list of dishes

        cpy = Offer(
            kitchen=self.kitchen,
            dishes=[str(dish.pk) for dish in self._model.dishes.all()],
        )
        cpy.name = self.name
        cpy.price = self.price
        cpy.total_available = self.total_available
        cpy.date = date
        cpy.lead_time = self.lead_time
        cpy.can_deliver = self.can_deliver
        cpy.deliver_time = self.deliver_time
        cpy.can_pickup = self.can_pickup
        cpy.is_recurring = self.is_recurring
        cpy.repeat_every = self.repeat_every
        cpy.end_date = self.end_date
        cpy.pickup_time = self.pickup_time
        cpy.order_by = self.order_by
        cpy.save()

        return cpy

    def get_available_for_date(self, date):
        return self._model.get_available_for_date(date)

    @transaction.atomic
    def move_to(self, date):
        """ Move this offer to the given date. This works by creating a copy
            on that date, and draining this. """
        moved_offer = self.copy_to(date)
        self.drain_available(self.pk, self.date.strftime("%Y-%m-%d"), "False")
        return moved_offer

    def save(self):
        if not self._dirty:
            return
        self._save()

    @transaction.atomic()
    def _save(self):
        create = not self.pk

        self._model.full_clean()
        self._model.save()

        if create:
            models.OfferDish.objects.bulk_create(
                [
                    models.OfferDish(offer_id=self._model.pk, dish_id=_dish.pk)
                    for _dish in self._dishes
                ]
            )

    @classmethod
    def drain_available(cls, pk, date, multi_delete):
        try:
            models.Offer.drain_available(pk, date, multi_delete)
        except models.Offer.DoesNotExist:
            raise cls.DoesNotExist

    @classmethod
    def _query(cls):
        return models.Offer.objects.select_related(
            "kitchen", "kitchen__latest_verification"
        ).prefetch_related("dishes")

    class DoesNotExist(Exception):
        pass

    @classmethod
    def fetch_for_kitchen(cls, kitchen, offset=0, limit=250):
        assert offset >= 0, "offset must be non-negative"
        assert limit > 0, "limit must be positive"
        return [
            cls(_model=model)
            for model in cls._query()
            .exclude(is_deleted=True)
            .filter(kitchen=kitchen)
            .all()[offset : offset + limit]
        ]

    @classmethod
    def fetch_upcoming_offers_queryset(cls, chef):
        client_adjusted_server_time = get_current_client_adjusted_datetime()

        base_q = (
            cls._query()
            .prefetch_related("pickup_time", "deliver_time")
            .exclude(is_deleted=True)
            .filter(kitchen__chef=chef)
        )

        two_weeks_from_now = client_adjusted_server_time + datetime.timedelta(
            OFFER_DAYS_LIMIT
        )

        non_recurring_q = base_q.filter(
            is_recurring=False,
            date__range=(client_adjusted_server_time.date(), two_weeks_from_now),
        )
        daily_recurring_q = base_q.filter(
            is_recurring=True,
            repeat_every="day",
            end_date__gte=client_adjusted_server_time.date(),
        )
        weekly_recurring_q = base_q.filter(
            is_recurring=True,
            repeat_every="week",
            date__gte=client_adjusted_server_time.date(),
            date__week_day=((client_adjusted_server_time.weekday() + 1) % 7) + 1,
        )

        return {
            "daily_offers": daily_recurring_q,
            "weekly_offers": weekly_recurring_q,
            "non_recurring_offers": non_recurring_q,
        }

    @classmethod
    def fetch_upcoming_offers_mapped_to_date(cls, chef):
        active_offers = cls.fetch_upcoming_offers_queryset(chef=chef)

        active_daily_offers = [
            cls(_model=offer) for offer in active_offers["daily_offers"]
        ]
        active_weekly_offers = [
            cls(_model=offer) for offer in active_offers["weekly_offers"]
        ]
        active_none_recurring_offers = [
            cls(_model=offer) for offer in active_offers["non_recurring_offers"]
        ]
        # Group data by date
        current_offers = defaultdict(list)

        for offer in active_none_recurring_offers:
            current_offers[offer.date.strftime("%Y-%m-%d")].append(offer)
        for daily_offer in active_daily_offers:
            future_offer_dates = get_future_available_date_with_display(
                vars(daily_offer.model), OFFER_DAYS_LIMIT
            )
            for future_date in future_offer_dates:
                current_offers[future_date['name']].append(daily_offer)
        for weekly_offer in active_weekly_offers:
            future_offer_dates = get_future_available_date_with_display(
                vars(weekly_offer.model), OFFER_DAYS_LIMIT
            )
            for future_date in future_offer_dates:
                current_offers[future_date['name']].append(weekly_offer)

        sorted_offers = OrderedDict()
        for key in sorted(current_offers.keys()):
            sorted_offers[key] = current_offers[key]
        return sorted_offers

    @classmethod
    def fetch_upcoming_offers(cls, chef):
        active_offers = cls.fetch_upcoming_offers_queryset(chef=chef)

        return {
            "daily_offers": [
                cls(_model=offer) for offer in active_offers["daily_offers"]
            ],
            "weekly_offers": [
                cls(_model=offer) for offer in active_offers["weekly_offers"]
            ],
            "non_recurring_offers": [
                cls(_model=offer) for offer in active_offers["non_recurring_offers"]
            ],
        }

    @classmethod
    def fetch_past(cls, chef, offset=0, limit=6):
        assert offset >= 0, "offset must be non-negative"
        assert limit > 0, "limit must be positive"
        # TODO this is shit
        # return [
        #     dish.Dish(_identity=model.dish)
        #     for model in cls._query()
        #     .filter(kitchen__chef=chef, order_by__lt=timezone.now())
        #     .all()[offset : offset + limit]
        # ]
        return []

    @classmethod
    def fetch_with_pk(cls, pk):
        try:
            return cls(_model=cls._query().get(pk=pk))
        except models.Offer.DoesNotExist:
            raise cls.DoesNotExist()

    @classmethod
    def fetch_with_pk_and_kitchen(cls, chef, pk):
        try:
            return cls(_model=cls._query().get(pk=pk, kitchen__chef=chef))
        except models.Offer.DoesNotExist:
            raise cls.DoesNotExist()

    @classmethod
    def fetch_with_chef_and_pks(cls, chef, pks):
        # This is literally copy pasted from the dish entity. Maybe there is some
        # refactoring to be done.
        try:
            ids = {
                i.pk: i
                for i in cls._query().filter(kitchen__chef=chef, pk__in=pks).all()
            }
            return [cls(_model=ids[pk]) for pk in pks]
        except KeyError:
            raise cls.DoesNotExist()

    @classmethod
    def fetch_near(
        cls, reference_point, date, offset=0, limit=24, pickup_within=20, flag="page"
    ):
        assert offset >= 0, "offset must be non-negative"
        assert limit > 0, "limit must be positive"

        base_q = (
            cls._query()
            .exclude(is_deleted=True)
            .annotate(
                annotated_distance=Distance(
                    "kitchen__pickup_address__point", reference_point
                )
            )
        )

        # Annotated distance will be in meters
        delivery_q = base_q.filter(can_deliver=True).filter(
            annotated_distance__lte=F("kitchen__deliver_range")
        )

        pickup_q = (
            base_q.filter(can_pickup=True)
            .exclude(kitchen__pickup_address__isnull=True)
            .filter(
                kitchen__pickup_address__point__distance_lte=(
                    reference_point,
                    D(mi=pickup_within),
                )
            )
        )

        intermediate_q = delivery_q | pickup_q

        non_recurring_q = intermediate_q.filter(is_recurring=False, date=date)

        daily_recurring_q = intermediate_q.filter(
            is_recurring=True, repeat_every="day", date__lte=date, end_date__gte=date
        )

        weekly_recurring_q = intermediate_q.filter(
            is_recurring=True,
            repeat_every="week",
            date__lte=date,
            end_date__gte=date,
            date__week_day=((date.weekday() + 1) % 7) + 1,
        )

        q = (
            (non_recurring_q | daily_recurring_q | weekly_recurring_q)
            .order_by("annotated_distance")
            .distinct()
            .all()
        )

        if flag == "page":
            q = q[offset : offset + limit]

        return [cls(_model=model) for model in q]


class Single(Offer):
    def __init__(self, dishes=None, **kwargs):
        if dishes is not None:
            dishes = set(dishes)
            if len(dishes) > 1:
                raise ValidationError(_("Too many dishes to create a single."))
        super().__init__(dishes=dishes, **kwargs)

    class DoesNotExist(Offer.DoesNotExist):
        pass

    @classmethod
    def _query(cls):
        return (
            super()
            ._query()
            .annotate(dishes_count=Count("dishes"))
            .filter(dishes_count=1)
        )


class Bundle(Offer):
    def __init__(self, dishes=None, **kwargs):
        if dishes is not None:
            dishes = set(dishes)
            if len(dishes) <= 1:
                raise ValidationError(_("Too few dishes to create a bundle."))
        super().__init__(dishes=dishes, **kwargs)

    class DoesNotExist(Offer.DoesNotExist):
        pass

    @classmethod
    def _query(cls):
        return (
            super()
            ._query()
            .annotate(dishes_count=Count("dishes"))
            .filter(dishes_count__gt=1)
        )
