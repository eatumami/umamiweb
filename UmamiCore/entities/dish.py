from collections import namedtuple
from datetime import date
from django.db import transaction
from django.db.models import F
from UmamiCore import models
from .DoesNotExist import DoesNotExist

Option = namedtuple("Option", "name price")
Extra = namedtuple("Extra", "name price")
Ingredient = namedtuple("Ingredient", "name")
Tag = namedtuple("Tag", "name")


class Dish(object):
    """ Manipulate Dish objects. """

    def __init__(self, kitchen=None, _identity=None):
        """ """
        if _identity is not None and kitchen is not None:
            raise ValueError("Cannot modify the kitchen of an existing dish.")

        if _identity is None and kitchen is None:
            raise ValueError("Must specify a kitchen for a new dish.")

        if _identity is not None:
            self._identity = _identity
            self._version = _identity.latest_version
            self._old_extras = self._load_extras(self._version)
            self._new_extras = None
            self._old_options = self._load_options(self._version)
            self._new_options = None
            self._old_tags = self._load_tags(self._version)
            self._new_tags = None
            self._old_ingredients = self._load_ingredients(self._version)
            self._new_ingredients = None
            self._dirty = False
        else:
            self._identity = models.DishIdentity(kitchen=kitchen)
            self._version = models.Dish()
            self._old_extras = dict()
            self._new_extras = dict()
            self._old_options = dict()
            self._new_options = dict()
            self._old_tags = set()
            self._new_tags = set()
            self._old_ingredients = set()
            self._new_ingredients = set()
            self._dirty = True

    @property
    def pk(self):
        return self._identity.pk

    @property
    def version(self):
        return self._version.pk

    @property
    def kitchen(self):
        return self._identity.kitchen

    def _get_name(self):
        return self._version.name

    def _set_name(self, name):
        self._version.name = name
        self._dirty = True

    name = property(_get_name, _set_name)

    def _get_description(self):
        return self._version.description

    def _set_description(self, description):
        self._version.description = description
        self._dirty = True

    description = property(_get_description, _set_description)

    def _get_image(self):
        return self._version.image

    def _set_image(self, image):
        self._version.image = image
        self._dirty = True

    image = property(_get_image, _set_image)

    def _get_price(self):
        return self._version.price.amount

    def _set_price(self, price):
        self._version.price = price
        self._dirty = True

    price = property(_get_price, _set_price)

    @property
    def options(self):
        options = self._new_options or self._old_options
        return [Option(n, p) for n, p in options.items()]

    def add_option(self, name, price):
        if self._new_options is None:
            self._new_options = dict()
        self._new_options[str(name).lower()] = price
        self._dirty = True

    def carry_over_options(self):
        self._new_options = self._old_options.copy()
        self._dirty = True

    @property
    def extras(self):
        extras = self._new_extras or self._old_extras
        return [Extra(n, p) for n, p in extras.items()]

    def add_extra(self, name, price):
        if self._new_extras is None:
            self._new_extras = dict()
        self._new_extras[str(name).lower()] = price
        self._dirty = True

    def carry_over_extras(self):
        self._new_extras = self._old_extras.copy()
        self._dirty = True

    @property
    def ingredients(self):
        ingredients = self._new_ingredients or self._old_ingredients
        return [Ingredient(name=i) for i in ingredients]

    def add_ingredient(self, name):
        if self._new_ingredients is None:
            self._new_ingredients = set()
        self._new_ingredients.add(str(name).lower())
        self._dirty = True

    def carry_over_ingredients(self):
        self._new_ingredients = self._old_ingredients.copy()
        self._dirty = True

    @property
    def tags(self):
        tags = self._new_tags or self._old_tags
        return [Tag(name=t) for t in tags]

    def add_tag(self, name):
        if self._new_tags is None:
            self._new_tags = set()
        self._new_tags.add(str(name).lower())
        self._dirty = True

    def carry_over_tags(self):
        self._new_tags = self._old_tags.copy()
        self._dirty = True

    @property
    def rating(self):
        return self._identity.rating

    def save(self):
        if not self._dirty:
            return

        with transaction.atomic():
            self._save_identity()
            self._save_version()
            self._save_options()
            self._save_extras()
            self._save_ingredients()
            self._save_tags()

        self._old_options = self._new_options or dict()
        self._new_options = None
        self._old_extras = self._new_extras or dict()
        self._new_extras = None
        self._old_ingredients = self._new_ingredients or set()
        self._new_ingredients = None
        self._old_tags = self._new_tags or set()
        self._new_tags = None
        self._dirty = False

    def _save_identity(self):
        # If the identity is brand new we need to expicitly
        # add it to the db.
        if self._identity.pk is None:
            self._identity.full_clean()
            self._identity.save()

    def _save_version(self):
        # The version itself
        self._version.pk = None  # None pk will create a new row.
        self._version.identity = self._identity
        self._version.full_clean()
        self._version.save()
        # Make sure the new version is the "latest"
        self._identity.latest_version = self._version
        self._identity.save()

    def _save_options(self):
        if self._new_options is None:
            return

        for name, price in self._new_options.items():
            option = models.DishOption(dish=self._version, name=name, price=price)
            option.full_clean()
            option.save()

    def _save_extras(self):
        if self._new_extras is None:
            return

        for name, price in self._new_extras.items():
            extra = models.DishExtra(dish=self._version, name=name, price=price)
            extra.full_clean()
            extra.save()

    def _save_ingredients(self):
        ingredients = self._new_ingredients or set()

        for name in ingredients:
            ingredient = models.DishIngredient(dish=self._version, name=name)
            ingredient.full_clean()
            ingredient.save()

        added = ingredients - self._old_ingredients
        for name in added:
            counter, _ = models.DishIngredientCount.objects.get_or_create(name=name)
            counter.count = F("count") + 1
            counter.save()

        removed = self._old_ingredients - ingredients
        for name in removed:
            counter, _ = models.DishIngredientCount.objects.get_or_create(name=name)
            counter.count = F("count") - 1
            counter.save()

    def _save_tags(self):
        tags = self._new_tags or set()

        for name in tags:
            tag = models.DishTag(dish=self._version, name=name)
            tag.full_clean()
            tag.save()

        added = tags - self._old_tags
        for name in added:
            counter, _ = models.DishTagCount.objects.get_or_create(name=name)
            counter.count = F("count") + 1
            counter.save()

        removed = self._old_tags - tags
        for name in removed:
            counter, _ = models.DishTagCount.objects.get_or_create(name=name)
            counter.count = F("count") - 1
            counter.save()

    @staticmethod
    def _query():
        return models.DishIdentity.objects.select_related(
            "latest_version"
        ).prefetch_related(
            "latest_version__options",
            "latest_version__extras",
            "latest_version__ingredients",
            "latest_version__tags",
        )

    class DoesNotExist(DoesNotExist):
        pass

    @classmethod
    def fetch_with_pk(cls, pk):
        try:
            return cls(_identity=cls._query().get(pk=pk))
        except models.DishIdentity.DoesNotExist as e:
            raise cls.DoesNotExist() from e

    @classmethod
    def fetch_with_pks(cls, pks):
        try:
            ids = {i.pk: i for i in cls._query().filter(pk__in=pks).all()}
            return [cls(_identity=ids[pk]) for pk in pks]
        except KeyError as e:
            raise cls.DoesNotExist() from e

    @classmethod
    def fetch_with_chef_and_pks(cls, chef, pks):
        try:
            ids = {
                i.pk: i
                for i in cls._query().filter(kitchen__chef=chef, pk__in=pks).all()
            }
            return [cls(_identity=ids[pk]) for pk in pks]
        except KeyError as e:
            raise cls.DoesNotExist() from e

    @classmethod
    def fetch_with_chef(cls, chef):
        try:
            dishes = {i.pk: i for i in cls._query().filter(kitchen__chef=chef).all()}
            return [cls(_identity=dish) for dish in dishes.values()]
        except KeyError as e:
            raise cls.DoesNotExist() from e

    @staticmethod
    def fetch_top_ingredients(offset=0, limit=250):
        assert offset >= 0, "offset must be non negative"
        assert limit > 0, "limit must be positive"
        ingredients = (
            models.DishIngredientCount.objects.order_by("count")
            .values_list("name", flat=True)
            .all()[offset : offset + limit]
        )
        return ingredients

    @staticmethod
    def fetch_top_tags(offset=0, limit=250):
        assert offset >= 0, "offset must be non negative"
        assert limit > 0, "limit must be positive"
        tags = (
            models.DishTagCount.objects.order_by("count")
            .values_list("name", flat=True)
            .all()[offset : offset + limit]
        )
        return tags

    def _load_ingredients(self, dish):
        return set(i.name for i in dish.ingredients.all())

    def _load_tags(self, dish):
        return set(t.name for t in dish.tags.all())

    def _load_options(self, dish):
        return {o.name: o.price.amount for o in dish.options.all()}

    def _load_extras(self, dish):
        return {e.name: e.price.amount for e in dish.extras.all()}


class Review(object):
    def __init__(self, _model=None, _dish=None):
        assert _model is not None, "Expected _model"
        self._model = _model
        self._old_rating = self._model.rating
        self._dirty = False
        self._lazy_dish = _dish
        self._lazy_user = None

    @property
    def model(self):
        return self._model

    @property
    def pk(self):
        return self._model.pk

    @property
    def dish(self):
        if self._lazy_dish is None:
            self._lazy_dish = Dish(_identity=self._model.dish)
        return self._lazy_dish

    @property
    def user(self):
        if self._lazy_user is None:
            self._lazy_user = self._model.user
        return self._lazy_user

    @property
    def date(self):
        return self._model.date

    @property
    def blank(self):
        return self._model.blank

    def _get_text(self):
        return self._model.text

    def _set_text(self, text):
        if self._model.text == text:
            return
        self._model.text = text
        self._dirty = True

    text = property(_get_text, _set_text)

    def _get_rating(self):
        return self._model.rating

    def _set_rating(self, rating):
        if self._model.rating == rating:
            return
        self._model.rating = rating
        self._dirty = True

    rating = property(_get_rating, _set_rating)

    def save(self):
        if not self._dirty:
            return
        self._save()
        self._dirty = False

    @transaction.atomic()
    def _save(self):
        # Figure out changes to scores
        delta_total = self._model.rating - self._old_rating
        delta_votes = 1 if self._model.blank else 0
        # Validate and save changes to the rating object
        self._model.blank = False
        self._model.date = date.today()
        self._model.full_clean()
        self._model.save()
        # Update scores of the rated objects
        self._model.dish._change_rating(delta_total, delta_votes)
        self._model.dish.kitchen._change_rating(delta_total, delta_votes)

    class DoesNotExist(DoesNotExist):
        pass

    @classmethod
    def _query(cls):
        return models.DishReview.objects

    @classmethod
    def fetch_for_dish(cls, dish, offset=0, limit=250):
        assert offset >= 0, "offset must be non negative"
        assert limit > 0, "limit must be positive"
        if isinstance(dish, Dish):
            q = (
                cls._query()
                .filter(dish=dish._identity, blank=False)
                .all()[offset : offset + limit]
            )
            return [cls(_model=m, _dish=dish) for m in q]
        else:
            q = (
                cls._query()
                .filter(dish=dish, blank=False)
                .all()[offset : offset + limit]
            )
            return [cls(_model=m) for m in q]

    @classmethod
    def fetch_for_dishes(cls, dishes, offset=0, limit=250):
        assert offset >= 0, "offset must be non negative"
        assert limit > 0, "limit must be positive"
        q = (
            cls._query()
            .filter(dish__in=dishes, blank=False)
            .all()[offset : offset + limit]
        )
        return [cls(_model=m) for m in q]

    @classmethod
    def fetch_for_user(cls, user, offset=0, limit=250):
        assert offset >= 0, "offset must be non negative"
        assert limit > 0, "limit must be positive"
        q = cls._query().filter(user=user).all()[offset : offset + limit]
        return [cls(_model=m) for m in q]

    @classmethod
    def fetch_for_dishes_and_user(cls, dishes, user):
        try:
            q = _model = cls._query().filter(dish__in=dishes, user=user).all()
            v = {dr.dish.pk: dr for dr in q}
            return [cls(_model=v[d]) for d in dishes]
        except KeyError as e:
            raise cls.DoesNotExist() from e

    @classmethod
    def fetch_for_dish_and_user(cls, dish, user):
        r, _ = cls._query().get_or_create(dish=dish, user=user)
        return cls(_model=r)
