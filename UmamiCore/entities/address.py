from collections import namedtuple
from UmamiCore import models

_AddressBase = namedtuple(
    "_AddressBase",
    ["line1", "line2", "city", "prov", "postal_code", "country", "lon", "lat"],
)


class Address(_AddressBase):
    """ A domain representation of an address. """

    __slots__ = ()

    @property
    def latlon(self):
        return (self.lat, self.lon)

    def persist(self):
        return models.Address.objects.create_address(**self._asdict())
