from UmamiCore import models
from decimal import Decimal


class FeeForDelivery(object):
    @staticmethod
    def calculate(distance_in_miles):
        return models.FeeForDelivery.calculate(distance_in_miles)


class FeeForStripe(object):

    _THIRTY_CENTS = Decimal("0.30")
    _STRIPE_FACTOR = Decimal("0.029")
    _ADJUST_FACTOR = 1 / (1 - _STRIPE_FACTOR)

    @classmethod
    def calculate_effective_fee(cls, price):
        """ Calculate the amount of money that we need to allocate for stripe. """
        return round(
            cls._ADJUST_FACTOR * (cls._STRIPE_FACTOR * price + cls._THIRTY_CENTS), 4
        )


class FeeForPlatform(object):

    _FACTOR = Decimal("0.1")

    @classmethod
    def calculate_fee(cls, price):
        return round(cls._FACTOR * price, 4)
