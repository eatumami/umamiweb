from collections import defaultdict, namedtuple
from decimal import Decimal
from django.conf import settings
from django.core.exceptions import ValidationError
from django.db import transaction
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from UmamiSite.helper.OfferHelper import get_current_client_adjusted_datetime

from UmamiCore import models
from UmamiCore.helpers import combine_date_and_time
from UmamiCore.errors import UmamiError

from .dish import Dish, Review
from .address import Address
from .fees import FeeForDelivery, FeeForPlatform, FeeForStripe

from .offer import Offer

OrderRequestBase = namedtuple(
    "OrderRequestBase", ["stripe_token", "delivery_address", "notes", "items"]
)


class OrderRequest(OrderRequestBase):
    __slots__ = ()

    @classmethod
    def parse_data(cls, data):
        items = [OrderRequestItem.parse_data(datum) for datum in data.pop("items")]
        delivery_address = data.pop("delivery_address", None)
        return cls(**data, delivery_address=delivery_address, items=items)


OrderRequestItemBase = namedtuple(
    "OrderRequestItemBase",
    ["uuid", "offer", "quantity", "choices", "delivery", "kitchen", "date"],
)


class OrderRequestItem(OrderRequestItemBase):
    __slots__ = ()

    @classmethod
    def parse_data(cls, data):
        choices = [
            OrderRequestChoice.parse_data(datum) for datum in data.pop("choices")
        ]
        return cls(**data, choices=choices)


OrderRequestChoiceBase = namedtuple(
    "OrderRequestChoiceBase", ["dish", "version", "option", "extras"]
)


class OrderRequestChoice(OrderRequestChoiceBase):
    __slots__ = ()

    @classmethod
    def parse_data(cls, data):
        option = OrderRequestOption.parse_data(data.pop("option"))
        extras = [OrderRequestExtra.parse_data(datum) for datum in data.pop("extras")]
        return cls(**data, option=option, extras=extras)


OrderRequestOptionBase = namedtuple("OrderRequestOptionBase", ["name"])


class OrderRequestOption(OrderRequestOptionBase):
    __slots__ = ()

    @classmethod
    def parse_data(cls, data):
        if data is None:
            return None
        return cls(**data)


OrderRequestExtraBase = namedtuple("OrderRequestExtraBase", ["name"])


class OrderRequestExtra(OrderRequestExtraBase):
    __slots__ = ()

    @classmethod
    def parse_data(cls, data):
        return cls(**data)


class OrderRequestData(object):
    def __init__(self, request):
        offer_pks, quantities, deliveries, delivery_items, pickup_items = (
            set(),
            defaultdict(lambda: defaultdict(int)),
            {},
            [],
            [],
        )
        for item in request.items:
            offer_pks.add(item.offer)
            quantities[item.offer][item.date.strftime("%Y-%m-%d")] += item.quantity
            deliveries[str(item.offer)] = item.delivery
            if item.delivery:
                delivery_items.append(item)
            else:
                pickup_items.append(item)

        offer_query = (
            models.Offer.objects.filter(pk__in=offer_pks)
                .select_related("kitchen", "kitchen__chef")
                .prefetch_related(
                "dishes",
                "dishes__latest_version",
                "dishes__latest_version__extras",
                "dishes__latest_version__options",
            )
                .all()
        )

        offers, dishes, kitchens, versions = {}, {}, {}, {}
        for offer in offer_query:
            offers[str(offer.pk)] = offer
            kitchens[str(offer.pk)] = offer.kitchen
            for dish in offer.dishes.all():
                dishes[str(dish.pk)] = dish
                versions[str(dish.latest_version.pk)] = dish

        self.delivery_items = delivery_items
        self.pickup_items = pickup_items
        self.request = request
        self.offers = offers
        self.dishes = dishes
        self.kitchens = kitchens
        self.quantities = quantities
        self.deliveries = deliveries
        self.versions = versions
        self.timestamp = timezone.now()


class OrderError(UmamiError):
    def __init__(self, detail):
        super().__init__()
        self.detail = detail


class OrderReceiptBuilder(object):
    def __init__(self, data, user):
        self.request = OrderRequest.parse_data(data)
        self.data = OrderRequestData(self.request)
        self.user = user
        self._validate()

    def _validate(self):
        errors = defaultdict(list)
        self._validate_items(errors)
        self._validate_quantities(errors)
        self._validate_notes(errors)
        self._validate_delivery(errors)
        if errors:
            raise OrderError(errors)

    def _validate_items(self, errors):
        for item in self.request.items:
            self._validate_item(item, errors)

    def _validate_item(self, item, errors):
        selected_dishes, required_dishes = (
            set(),
            set(str(x.pk) for x in self.data.offers[item.offer].dishes.all()),
        )

        for choice in item.choices:
            self._validate_choice(item, choice, errors)
            if choice.dish in selected_dishes:
                errors["multiple_choices"].append({"dish": choice.dish})
            selected_dishes.add(choice.dish)

        if required_dishes != selected_dishes:
            for dish in required_dishes - selected_dishes:
                errors["missing_choices"].append({"dish": dish})
            for dish in selected_dishes - required_dishes:
                errors["extra_choices"].append({"dish": dish})

        offer = self.data.offers.get(str(item.offer))
        offer_entity = Offer(_model=offer)

        if offer_entity.is_expired:
            errors["expired_offers"].append({"offer": item.offer})

        if offer_entity.is_deleted:
            errors["deleted_offers"].append({"offer": item.offer})

        if not offer_entity.is_available_for_order(item.date):
            errors["offer_not_available_for_date"].append({"offer": item.offer})

    def _validate_choice(self, item, choice, errors):
        if choice.version != str(self.data.dishes[choice.dish].latest_version.pk):
            errors["stale_dish"].append({"dish": choice.dish})
        self._validate_option(item, choice, errors)
        self._validate_extras(item, choice, errors)

    def _validate_option(self, item, choice, errors):
        if choice.option is not None:
            # XXX <hackery for bundles>
            if self.data.offers[item.offer].dishes.count() > 1:
                errors["bundle_expects_default_option"].append({"offer": item.offer})
            # XXX </hackery for bundles>
            avaliable_options = set(
                str(x.name)
                for x in self.data.dishes[choice.dish].latest_version.options.all()
            )
            if choice.option.name not in avaliable_options:
                errors["option_not_available"].append(
                    {
                        "offer": item.offer,
                        "dish": choice.dish,
                        "option": choice.option.name,
                    }
                )

    def _validate_extras(self, item, choice, errors):
        if len(choice.extras) > 0:
            # XXX <hackery for bundles>
            if self.data.offers[item.offer].dishes.count() > 1:
                errors["bundle_expects_empty_extras"].append({"offer": item.offer})
            # XXX </hackery for bundles>
            avaliable_extras = set(
                str(x.name)
                for x in self.data.dishes[choice.dish].latest_version.extras.all()
            )
            for extra in choice.extras:
                if extra.name not in avaliable_extras:
                    errors["extra_not_available"].append(
                        {"offer": item.offer, "dish": choice.dish, "extra": extra.name}
                    )

    def _validate_quantities(self, errors):
        for offer_id, quantity in self.data.quantities.items():
            date, q = zip(*quantity.items())
            try:
                offer_available = models.OfferAvailable.objects.get(
                    offer=offer_id, date=date[0]
                )
                available = offer_available.available
            except models.OfferAvailable.DoesNotExist:
                available = self.data.offers[offer_id].total_available

            if available < q[0]:
                errors["out_of_stock"].append(
                    {"offer": offer_id, "available": available}
                )

    def _validate_notes(self, errors):
        if len(self.request.notes) > 255:
            errors["notes"].append(_("Notes too long."))

    def _validate_delivery(self, errors):
        if self.data.delivery_items and not self.request.delivery_address:
            errors["delivery_address"].append(
                _("Need a delivery address to do delivery")
            )
        for pickup_item in self.data.pickup_items:
            if not self.data.offers[pickup_item.offer].can_pickup:
                errors["cannot_pickup"].append(
                    _("This offer is not available for pickup")
                )
        for delivery_item in self.data.delivery_items:
            if not self.data.offers[delivery_item.offer].can_deliver:
                errors["cannot_deliver"].append(
                    _("This offer is not available for delivery")
                )

    def build(self):
        pickup_orders = self.build_pickup_orders()
        delivery_orders = self.build_delivery_orders()
        total = sum([order.total for order in pickup_orders]) + sum(
            [order.total for order in delivery_orders]
        )
        subtotal = sum([order.subtotal for order in pickup_orders]) + sum(
            [order.subtotal for order in delivery_orders]
        )
        stripe_fee = sum([order.stripe_fee for order in pickup_orders]) + sum(
            [order.stripe_fee for order in delivery_orders]
        )
        platform_fee = sum([order.platform_fee for order in pickup_orders]) + sum(
            [order.platform_fee for order in delivery_orders]
        )

        delivery_fee = sum([order.delivery_fee for order in delivery_orders])

        return OrderReceipt(
            pickup_orders=pickup_orders,
            delivery_orders=delivery_orders,
            total=total,
            stripe_fee=stripe_fee,
            subtotal=subtotal,
            platform_fee=platform_fee,
            delivery_fee=delivery_fee,
            data=self.data,
        )

    def insert_new_order(self, new_order, orders):
        grouped = False
        for i, order in enumerate(orders):
            if (
                    order.seller == new_order.seller
                    and order.handoff_date == new_order.handoff_date
                    and order.handoff_time_start == new_order.handoff_time_start
                    and order.handoff_time_end == new_order.handoff_time_end
            ):
                orders[i] = IndividualOrderReceipt(
                    pk=None,
                    stripe_charge_token=None,
                    buyer=self.user,
                    seller=order.seller,
                    handoff_type="",
                    handoff_address=order.handoff_address,
                    handoff_date=order.handoff_date,
                    handoff_time_start=order.handoff_time_start,
                    handoff_time_end=order.handoff_time_end,
                    delivery_fee=order.delivery_fee,
                    platform_fee=order.platform_fee + new_order.platform_fee,
                    subtotal=order.subtotal + new_order.subtotal,
                    stripe_fee=order.stripe_fee + new_order.stripe_fee,
                    total=order.total + new_order.total,
                    notes=order.notes,
                    items=order.items + new_order.items,
                    refunded_amount=0,
                    cancelled=False,
                    is_refundable=True,
                    is_cancellable=True,
                    data=self.data,
                )
                grouped = True
        if not grouped:
            orders.append(new_order)
            orders.sort(
                key=lambda x: (x.handoff_date, x.handoff_time_start), reverse=True
            )

    def build_delivery_orders(self):
        delivery_orders = []
        for item in self.data.delivery_items:
            delivery_item = self._build_item(item)
            handoff = self._build_handoff(item)

            delivery_fee = 0

            if item.delivery:
                # Validate that the food is in delivery range
                loc = handoff.handoff_address.latlon

                kitchen = self.data.kitchens.get(str(item.offer))
                if not kitchen.delivers_to(loc):
                    raise OrderError(
                        {
                            "delivery_out_of_range": [
                                _(
                                    "This kitchen does not deliver to the requested location."
                                )
                            ]
                        }
                    )

                delivery_fee = kitchen.delivery_price(loc).amount

            platform_fee = FeeForPlatform.calculate_fee(
                delivery_item.subtotal + delivery_fee
            )
            stripe_fee = FeeForStripe.calculate_effective_fee(
                delivery_item.subtotal + delivery_fee
            )

            new_order = IndividualOrderReceipt(
                pk=None,
                stripe_charge_token=None,
                buyer=self.user,
                seller=self.data.kitchens.get(str(item.offer)),
                handoff_type="delivery",
                handoff_address=handoff.handoff_address,
                handoff_date=handoff.handoff_date,
                handoff_time_start=handoff.handoff_time_start,
                handoff_time_end=handoff.handoff_time_end,
                delivery_fee=delivery_fee,
                platform_fee=platform_fee,
                subtotal=delivery_item.subtotal,
                stripe_fee=stripe_fee,
                total=delivery_fee + stripe_fee + delivery_item.subtotal,
                notes=self.request.notes,
                items=[delivery_item],
                refunded_amount=0,
                cancelled=False,
                is_refundable=True,
                is_cancellable=True,
                data=self.data,
            )
            self.insert_new_order(new_order, delivery_orders)
        return delivery_orders

    def build_pickup_orders(self):
        pickup_orders = []
        for item in self.data.pickup_items:
            pickup_item = self._build_item(item)

            handoff = self._build_handoff(item)

            platform_fee = FeeForPlatform.calculate_fee(pickup_item.subtotal)
            stripe_fee = FeeForStripe.calculate_effective_fee(pickup_item.subtotal)

            new_order = IndividualOrderReceipt(
                pk=None,
                notes="",
                stripe_charge_token=None,
                buyer=self.user,
                seller=self.data.kitchens.get(item.offer),
                handoff_type="pickup",
                handoff_address=handoff.handoff_address,
                handoff_date=handoff.handoff_date,
                handoff_time_start=handoff.handoff_time_start,
                handoff_time_end=handoff.handoff_time_end,
                delivery_fee=Decimal(0),
                platform_fee=platform_fee,
                subtotal=pickup_item.subtotal,
                stripe_fee=stripe_fee,
                total=stripe_fee + pickup_item.subtotal,
                items=[pickup_item],
                refunded_amount=0,
                cancelled=False,
                is_cancellable=True,
                is_refundable=True,
                data=self.data,
            )
            self.insert_new_order(new_order, pickup_orders)
        return pickup_orders

    def _build_item(self, item):
        # TODO The subtotal uses the price of the dish, instead of the optional offer discounted price.
        choices = []
        subtotal = 0

        offer = Offer.fetch_with_pk(item.offer)

        available_obj = models.OfferAvailable.objects.filter(
            offer=item.offer, date=item.date
        )

        for choice in item.choices:
            x = self._build_choice(item, choice)
            subtotal += x.subtotal
            choices.append(x)
        # If the offer is a bundle, then we just use the price since we don't support extras or options for bundles currently
        if offer.is_bundle:
            subtotal = offer.price
        return OrderReceiptItem(
            uuid=item.uuid,
            offer=item.offer,
            available=offer.total_available
            if len(available_obj) == 0
            else available_obj[0].available,
            name=offer.name,
            quantity=item.quantity,
            choices=choices,
            subtotal=subtotal * item.quantity,
        )

    def _build_choice(self, item, choice):
        # TODO The subtotal is the price of the dish. Instead of giving the full breakdown, if
        # if there is a discount or something just give that price for the item.
        dish, dishp = self._build_dish(choice.dish)
        option, optionp = self._build_option(choice, choice.option)
        extras, extrasp = self._build_extras(choice, choice.extras)
        subtotal = dishp + optionp + extrasp
        return OrderReceiptChoice(
            dish=dish, option=option, extras=extras, subtotal=subtotal, review=None
        )

    def _build_dish(self, dish):
        d = Dish(_identity=self.data.dishes[dish])
        return d, d.price

    def _build_option(self, choice, option):
        if option is None:
            return None, Decimal(0)
        opt = self.data.dishes[choice.dish].latest_version.options.get(name=option.name)
        name = opt.name
        subtotal = opt.price.amount
        return OrderReceiptOption(name=name, subtotal=subtotal), subtotal

    def _build_extras(self, choice, extras):
        es = []
        subtotal = 0
        for extra in extras:
            e, s = self._build_extra(choice, extra)
            es.append(e)
            subtotal += s
        return es, subtotal

    def _build_extra(self, choice, extra):
        ext = self.data.dishes[choice.dish].latest_version.extras.get(name=extra.name)
        name = ext.name
        subtotal = ext.price.amount
        return OrderReceiptExtra(name=name, subtotal=subtotal), subtotal

    def _build_handoff(self, item):
        offer = self.data.offers.get(item.offer)
        handoff_date = item.date
        handoff_time_start = (
            offer.deliver_time.start if item.delivery else offer.pickup_time.start
        )
        handoff_time_end = (
            offer.deliver_time.end if item.delivery else offer.pickup_time.end
        )

        return OrderReceiptHandoff(
            handoff_address=Address(**self.request.delivery_address)
            if item.delivery
            else self.data.kitchens.get(str(offer.id)).pickup_address,
            handoff_date=handoff_date,
            handoff_time_start=handoff_time_start,
            handoff_time_end=handoff_time_end,
        )


class OrderReceiptLoader(object):
    def __init__(self, order):
        self.order = order

    def load(self):
        items = self._load_items()

        return IndividualOrderReceipt(
            pk=self.order.pk,
            stripe_charge_token=self.order.stripe_charge_token,
            buyer=self.order.buyer,
            seller=self.order.seller,
            handoff_type=self.order.handoff_type,
            handoff_address=self.order.handoff_address,
            handoff_date=self.order.handoff_date,
            handoff_time_start=self.order.handoff_time_start,
            handoff_time_end=self.order.handoff_time_end,
            subtotal=self.order.subtotal.amount,
            total=self.order.total.amount,
            delivery_fee=self.order.delivery_fee.amount,
            platform_fee=self.order.platform_fee.amount,
            stripe_fee=self.order.stripe_fee.amount,
            refunded_amount=self.order.refunded_amount.amount,
            cancelled=self.order.cancelled,
            is_cancellable=self._is_cancellable(),
            is_refundable=self._is_refundable(),
            notes=self.order.notes,
            items=items,
            data=None,
        )

    def _is_refundable(self):
        time_since_handoff = get_current_client_adjusted_datetime().date() - self.order.handoff_date
        return self.order.refunded_amount.amount == 0 and time_since_handoff.days < 30 and not self.order.cancelled

    def _is_cancellable(self):
        handoff_end_datetime = combine_date_and_time(self.order.handoff_date, self.order.handoff_time_end, 0)
        return get_current_client_adjusted_datetime() < handoff_end_datetime and not self.order.cancelled

    def _load_items(self):
        items = []
        for item in self.order.items.all():
            x = self._load_item(item)
            items.append(x)
        return items

    def _load_item(self, item):
        dishes = {}
        subtotals_by_dish = defaultdict(Decimal)
        extras_by_dish = defaultdict(list)
        options_by_dish = {}

        for d in item.offer.dishes.all():
            dk = str(d.pk)
            dishes[dk] = d
            subtotals_by_dish[dk] += d.latest_version.price.amount

        for extra in item.extras.all():
            dvk = str(extra.extra.dish.pk)
            dk = str(extra.extra.dish.identity.pk)
            ex = OrderReceiptExtra(
                name=extra.extra.name, subtotal=extra.extra.price.amount
            )
            extras_by_dish[dk].append(ex)
            subtotals_by_dish[dk] += ex.subtotal

        for option in item.option.all():
            dvk = str(option.option.dish.pk)
            dk = str(option.option.dish.identity.pk)
            op = OrderReceiptOption(
                name=option.option.name, subtotal=option.option.price.amount
            )
            options_by_dish[dk] = op
            subtotals_by_dish[dk] += op.subtotal

        subtotal = Decimal()
        choices = []
        for dk, dish in dishes.items():
            choice = OrderReceiptChoice(
                dish=Dish(_identity=dish),
                option=options_by_dish.get(dk),
                extras=extras_by_dish[dk],
                subtotal=subtotals_by_dish[dk],
                review=Review.fetch_for_dish_and_user(dish, self.order.buyer),
            )
            subtotal += choice.subtotal
            choices.append(choice)

        item_offer = Offer(_model=item.offer)
        return OrderReceiptItem(
            offer=str(item.offer.pk),
            uuid="",
            available=0,
            name=item_offer.name,
            quantity=item.quantity,
            choices=choices,
            subtotal=subtotal * item.quantity,
        )


IndividualOrderReceiptBase = namedtuple(
    "IndividualOrderReceiptBase",
    [
        "pk",
        "notes",
        "buyer",
        "seller",
        "handoff_type",
        "stripe_charge_token",
        "handoff_address",
        "handoff_date",
        "handoff_time_start",
        "handoff_time_end",
        "items",
        "total",
        "subtotal",
        "delivery_fee",
        "stripe_fee",
        "platform_fee",
        "refunded_amount",
        "cancelled",
        "is_cancellable",
        "is_refundable",
        "data",
    ],
)

OrderReceiptBase = namedtuple(
    "OrderReceiptBase",
    [
        "delivery_orders",
        "pickup_orders",
        "subtotal",
        "stripe_fee",
        "delivery_fee",
        "platform_fee",
        "total",
        "data",
    ],
)

OrderReceiptItem = namedtuple(
    "OrderReceiptItem",
    ["uuid", "offer", "name", "quantity", "choices", "available", "subtotal"],
)

OrderReceiptChoice = namedtuple(
    "OrderReceiptChoice", ["dish", "option", "extras", "subtotal", "review"]
)

OrderReceiptOption = namedtuple("OrderReceiptOption", ["name", "subtotal"])

OrderReceiptExtra = namedtuple("OrderReceiptExtra", ["name", "subtotal"])

OrderReceiptHandoff = namedtuple(
    "OrderReceiptHandoff",
    ["handoff_address", "handoff_date", "handoff_time_start", "handoff_time_end"],
)


class _OrderReceipt(object):
    def description(self):
        return "Order of {} items from {} at {}".format(
            sum(i.quantity for i in self.items), self.seller.title, timezone.now()
        )

    def total_in_cents(self):
        return round(100 * self.total)

    def total_minus_fees_in_cents(self):
        return round(100 * (self.total - self.stripe_fee - self.platform_fee))

    def currency(self):
        return settings.DEFAULT_CURRENCY

    @staticmethod
    def build(data, user):
        return OrderReceiptBuilder(data, user).build()

    def accquire(self):
        """ Try to accquire the resources described in this order. """
        pks, qs = zip(*self.data.quantities.items())
        models.Offer.dec_available(pks, qs)

    def release(self, *args):
        """ Replace resources accquired by this order. """
        pks, qs = zip(*self.data.quantities.items())
        models.Offer.inc_available(pks, qs)

    def persist(self, token, delivery):
        pk = self.save(token, delivery)
        return self._replace(pk=pk, stripe_charge_token=token)

    def abandon(self, data):
        pass

    class DoesNotExist(Exception):
        pass

    @transaction.atomic()
    def restore_quantities(self):
        quantities = defaultdict(lambda: defaultdict(int))
        for item in self.items:
            quantities[item.offer][self.handoff_date] = item.quantity
        pks, qs = zip(*quantities.items())
        models.Offer.inc_available(pks, qs)
        order_model = self.fetch_model_with_pk(self.pk)
        order_model.mark_cancelled()
        order_model.set_refunded_amount(self.total_in_cents())

    @classmethod
    def _query(cls):
        return models.Order.objects.prefetch_related(
            "items",
            "items__offer",
            "items__extras",
            "items__extras__extra",
            "items__option",
            "items__option__option",
        )

    @classmethod
    def fetch_with_pk(cls, pk):
        try:
            o = cls._query().get(pk=pk)
            return OrderReceiptLoader(o).load()
        except models.Order.DoesNotExist:
            raise DoesNotExist()

    @classmethod
    def fetch_with_pks(cls, pks):
        try:
            os = cls._query().filter(pk__in=pks)
            return [OrderReceiptLoader(o).load() for o in os]
        except models.Order.DoesNotExist:
            raise DoesNotExist()

    @classmethod
    def fetch_model_with_pk(cls, pk):
        try:
            return cls._query().get(pk=pk)
        except models.Order.DoesNotExist:
            raise DoesNotExist()

    @classmethod
    def fetch_sold(cls, chef, offset=0, limit=250):
        os = (
            cls._query()
                .filter(seller=chef.kitchen)
                .order_by("-timestamp")
                .all()[offset: offset + limit]
        )
        return [OrderReceiptLoader(o).load() for o in os]

    @classmethod
    def fetch_purchased(cls, buyer, offset=0, limit=250):
        os = (
            cls._query()
                .filter(buyer=buyer)
                .order_by("-timestamp")
                .all()[offset: offset + limit]
        )
        return [OrderReceiptLoader(o).load() for o in os]

    @classmethod
    def fetch_upcoming(cls, chef):
        os = (
            cls._query()
                .filter(
                seller=chef.kitchen,
                handoff_date__gte=get_current_client_adjusted_datetime().date(),
            )
                .exclude(cancelled=True)
                .order_by("handoff_date", "handoff_time_start")
                .all()
        )
        return [OrderReceiptLoader(o).load() for o in os]

    def save(self, stripe_charge_token, delivery):
        if self.stripe_charge_token is not None:
            # TODO This is an internal error, not a user error right?
            # shouldn't this be some other kind of error?
            raise ValidationError(_("Can't save, already saved."))
        try:
            return self._save(stripe_charge_token, delivery)
        except ValidationError as err:
            raise OrderError(err.error_dict)

    @transaction.atomic()
    def _save(self, stripe_charge_token, delivery):

        if delivery:
            addr = self.handoff_address.persist()
        else:
            addr = self.handoff_address

        # Order itself
        order = models.Order(
            notes=self.notes,
            buyer=self.buyer,
            seller=self.seller,
            handoff_type="delivery" if delivery else "pickup",
            handoff_address=addr,
            handoff_date=self.handoff_date,
            handoff_time_start=self.handoff_time_start,
            handoff_time_end=self.handoff_time_end,
            delivery_fee=self.delivery_fee,
            platform_fee=self.platform_fee,
            stripe_fee=self.stripe_fee,
            subtotal=self.subtotal,
            total=self.total,
            stripe_charge_token=stripe_charge_token,
        )
        order.full_clean()
        order.save()

        # Each item
        for item in self.items:
            i = models.OrderItem(
                order=order, offer=self.data.offers[item.offer], quantity=item.quantity
            )
            i.full_clean()
            i.save()
            # Particular choices
            for choice in item.choices:
                # Choice of option
                if choice.option is not None:
                    d = self.data.dishes[
                        str(choice.dish.pk)
                    ].latest_version.options.get(name=choice.option.name)
                    o = models.OrderItemOption(order_item=i, option=d)
                    o.full_clean()
                    o.save()
                # Choice of extras
                for extra in choice.extras:
                    d = self.data.dishes[str(choice.dish.pk)].latest_version.extras.get(
                        name=extra.name
                    )
                    e = models.OrderItemExtra(order_item=i, extra=d)
                    e.full_clean()
                    e.save()

        # Update user's address
        if delivery:
            self.buyer.last_used_address = addr
            self.buyer.save()

        for dish in self.data.dishes.values():
            r, _ = models.DishReview.objects.get_or_create(dish=dish, user=self.buyer)

        return order.pk


class OrderReceipt(OrderReceiptBase, _OrderReceipt):
    pass


class IndividualOrderReceipt(_OrderReceipt, IndividualOrderReceiptBase):
    pass
