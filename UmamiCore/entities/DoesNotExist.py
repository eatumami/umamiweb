from UmamiCore.errors import UmamiError


class DoesNotExist(UmamiError):
    pass
