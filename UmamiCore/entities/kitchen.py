import UmamiCore.models as models
from django.utils import timezone
from datetime import datetime
from django.utils.translation import gettext_lazy as _
from collections import defaultdict
from .offer import Offer, Single, Bundle
from django.contrib.gis.measure import D
from .DoesNotExist import DoesNotExist
from decimal import Decimal
from django.core.exceptions import ValidationError


class Kitchen(object):
    def __init__(self, _chef=None, _model=None):
        if _model is not None:
            self._init_with_model(_model)
        else:
            self._init_with_chef(_chef)

    def _init_with_chef(self, _chef):
        self._model = models.Kitchen(chef=_chef)
        self._dirty = True

    def _init_with_model(self, _model):
        self._model = _model
        self._dirty = False

    @property
    def model(self):
        return self._model

    @property
    def pk(self):
        return self._model.pk

    @property
    def chef(self):
        return self._model.chef

    @property
    def model(self):
        return self._model

    @property
    def date_joined(self):
        return self._model.date_joined

    def _get_title(self):
        return self._model.title

    def _set_title(self, title):
        self._model.title = title
        self._dirty = True

    title = property(_get_title, _set_title)

    def _get_bio(self):
        return self._model.bio

    def _set_bio(self, bio):
        self._model.bio = bio
        self._dirty = True

    bio = property(_get_bio, _set_bio)

    def _get_style(self):
        return self._model.style

    def _set_style(self, style):
        self._model.style = style
        self._dirty = True

    style = property(_get_style, _set_style)

    def _get_logo_image(self):
        return self._model.logo_image

    def _set_logo_image(self, logo_image):
        self._model.logo_image = logo_image
        self._dirty = True

    logo_image = property(_get_logo_image, _set_logo_image)

    def _get_banner_image0(self):
        return self._model.banner_image0

    def _set_banner_image0(self, banner_image0):
        self._model.banner_image0 = banner_image0
        self._dirty = True

    banner_image0 = property(_get_banner_image0, _set_banner_image0)

    def _get_banner_image1(self):
        return self._model.banner_image1

    def _set_banner_image1(self, banner_image1):
        self._model.banner_image1 = banner_image1
        self._dirty = True

    banner_image1 = property(_get_banner_image1, _set_banner_image1)

    def _get_banner_image2(self):
        return self._model.banner_image2

    def _set_banner_image2(self, banner_image2):
        self._model.banner_image2 = banner_image2
        self._dirty = True

    banner_image2 = property(_get_banner_image2, _set_banner_image2)

    def _get_banner_image3(self):
        return self._model.banner_image3

    def _set_banner_image3(self, banner_image3):
        self._model.banner_image3 = banner_image3
        self._dirty = True

    banner_image3 = property(_get_banner_image3, _set_banner_image3)

    def _get_banner_image4(self):
        return self._model.banner_image4

    def _set_banner_image4(self, banner_image4):
        self._model.banner_image4 = banner_image4
        self._dirty = True

    banner_image4 = property(_get_banner_image4, _set_banner_image4)

    @property
    def banner_images(self):
        images = []
        for i in range(5):
            try:
                image = getattr(self, "banner_image{}".format(i))
                image.url  # raises an exception if the field is null
                images.append(image)
            except:
                pass
        return images

    def _get_deliver_range(self):
        """ Return deliver range in miles."""
        return D(m=self._model.deliver_range).mi

    def _set_deliver_range(self, deliver_range):
        """ Set deliver range in miles. """
        self._model.deliver_range = round(Decimal.from_float(D(mi=deliver_range).m), 2)
        self._dirty = True

    deliver_range = property(_get_deliver_range, _set_deliver_range)

    def _get_pickup_address(self):
        return self._model.pickup_address

    def _set_pickup_address(self, pickup_address):
        self._model.pickup_address = pickup_address
        self._dirty = True

    pickup_address = property(_get_pickup_address, _set_pickup_address)

    def _get_stripe_account_token(self):
        return self._model.stripe_account_token

    def _set_stripe_account_token(self, sac):
        self._model.stripe_account_token = sac
        self._dirty = True

    stripe_account_token = property(
        _get_stripe_account_token, _set_stripe_account_token
    )

    def _get_stripe_access_token(self):
        return self._model.stripe_access_token

    def _stripe_access_token(self, token):
        self._model.stripe_access_token = token
        self._dirty = True

    stripe_access_token = property(_get_stripe_access_token, _stripe_access_token)

    def _get_stripe_refresh_token(self):
        return self._model.stripe_refresh_token

    def _set_stripe_refresh_token(self, token):
        self._model.stripe_refresh_token = token
        self._dirty = True

    stripe_refresh_token = property(
        _get_stripe_refresh_token, _set_stripe_refresh_token
    )

    def _get_latest_verification(self):
        return self._model.latest_verification

    def _set_latest_verification(self, verification):
        self._model.latest_verification = verification
        self._dirty = True

    latest_verification = property(_get_latest_verification, _set_latest_verification)

    def _get_photo_id(self):
        return self._model.photo_id

    def _set_photo_id(self, photo_id):
        self._model.photo_id = photo_id
        self._dirty = True

    photo_id = property(_get_photo_id, _set_photo_id)

    @property
    def has_identification(self):
        return self._model.has_identification

    @property
    def rating(self):
        return self._model.rating

    @property
    def has_verification(self):
        return self._model.has_verification

    @property
    def has_stripe(self):
        return self._model.has_stripe

    @property
    def current_offers_calendar(self):
        return Offer.fetch_upcoming_offers_mapped_to_date(self._model.chef)

    @property
    def current_offers(self):
        return Offer.fetch_upcoming_offers(self._model.chef)

    @property
    def past_offers(self):
        return Offer.fetch_past(self._model.chef)

    @property
    def current_singles(self):
        return Single.fetch_upcoming_offers_mapped_to_date(self._model.chef)

    @property
    def past_singles(self):
        return Single.fetch_past(self._model.chef)

    @property
    def current_bundles(self):
        return Bundle.fetch_upcoming_offers_mapped_to_date(self._model.chef)

    @property
    def past_bundles(self):
        return Bundle.fetch_past(self._model.chef)

    def delivers_to(self, latlon):
        return self._model.delivers_to(latlon)

    def distance_to(self, latlon):
        return self._model.distance_to(latlon)

    @property
    def postal_code(self):
        if self._model.pickup_address is not None:
            return self._model.pickup_address.postal_code
        else:
            return None

    @property
    def lon(self):
        if self._model.pickup_address is not None:
            return self._model.pickup_address.lon
        else:
            return None

    @property
    def lat(self):
        if self._model.pickup_address is not None:
            return self._model.pickup_address.lat
        else:
            return None

    @property
    def permit_number(self):
        v = self._model.latest_verification
        if v is None:
            return None
        return v.permit_number

    @property
    def permit_county(self):
        v = self._model.latest_verification
        if v is None:
            return None
        return v.permit_county

    def save(self):
        if not self._dirty:
            return
        self._model.full_clean()
        self._model.save()

    class DoesNotExist(DoesNotExist):
        pass

    @classmethod
    def _query(cls):
        return models.Kitchen.objects.select_related("latest_verification")

    @classmethod
    def fetch_with_pk(cls, pk):
        try:
            return cls(_model=cls._query().get(pk=pk))
        except models.Kitchen.DoesNotExist:
            raise cls.DoesNotExist()

    @classmethod
    def fetch_with_chef(cls, chef):
        try:
            return cls(_model=cls._query().get(chef=chef))
        except models.Kitchen.DoesNotExist:
            raise cls.DoesNotExist()


class KitchenVerification(object):
    def __init__(self, kitchen, data):
        self._model = models.KitchenVerification(
            kitchen=kitchen, date_verified=timezone.now().date(), **data
        )
        self._data = data
        self._kitchen = kitchen
        self._dirty = True
        self._validate()

    def save(self):
        if not self._dirty:
            return
        self._model.full_clean()
        self._model.save()
        self._kitchen.latest_verification = self._model
        self._kitchen.save()

    def _validate(self):
        if not self._data.get("permit_number"):
            raise ValidationError(_("Need permit number"))
        if not self._data.get("permit_county"):
            raise ValidationError(_("Need permit issuing county"))

        if not self._data.get("date_expires"):
            raise ValidationError(_("Need permit expiry date"))
        else:
            date_expires = datetime.strptime(
                self._data.get("date_expires"), "%Y-%m-%d"
            ).date()
            if date_expires < timezone.now().date():
                raise ValidationError(_("Expiry date needs to be in the future"))

    @property
    def permit_number(self):
        return self._model.permit_number

    @property
    def permit_county(self):
        return self._model.permit_county
