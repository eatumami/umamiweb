from django import forms
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import Group

from UmamiCore.models import *


class UserChangeForm(forms.ModelForm):
    """A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """

    password = ReadOnlyPasswordHashField()

    class Meta:
        model = User
        fields = (
            "email",
            "phone",
            "password",
            "first_name",
            "last_name",
            "is_active",
            "is_admin",
        )

    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial["password"]


class UserAdmin(BaseUserAdmin):
    # The forms to add and change user instances
    form = UserChangeForm
    add_form = UserCreationForm

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = ("email", "phone", "first_name", "last_name", "is_admin")
    list_filter = ("is_admin",)
    fieldsets = (
        (
            None,
            {"fields": ("email", "phone", "password", "last_used_address", "is_admin")},
        ),
        ("Personal info", {"fields": ("first_name", "last_name")}),
    )
    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    add_fieldsets = (
        (
            None,
            {
                "classes": ("wide",),
                "fields": (
                    "email",
                    "phone",
                    "first_name",
                    "last_name",
                    "password1",
                    "password2",
                ),
            },
        ),
    )
    search_fields = ("email",)
    ordering = ("email",)
    filter_horizontal = ()


# - - - - -

admin.site.register(User, UserAdmin)
admin.site.unregister(Group)

admin.site.register(Address)
admin.site.register(Customer)
admin.site.register(FeeForDelivery)

# - - - - -


@admin.register(Kitchen)
class KitchenAdmin(admin.ModelAdmin):
    list_display = ("id", "chef")
    search_fields = ("id", "chef__email")


admin.site.register(KitchenFollower)
admin.site.register(KitchenVerification)
admin.site.register(FeaturedKitchen)

# - - - - -


@admin.register(Dish)
class DishAdmin(admin.ModelAdmin):
    list_display = ("id", "identity")
    search_fields = ("id", "identity")


@admin.register(DishIdentity)
class DishIdentityAdmin(admin.ModelAdmin):
    list_display = ("id", "latest_version", "kitchen")
    search_fields = ("id", "latest_version", "kitchen__id")


admin.site.register(DishIngredient)
admin.site.register(DishExtra)
admin.site.register(DishOption)
admin.site.register(DishReview)
admin.site.register(DishTag)

# - - - - -


@admin.register(Offer)
class OfferAdmin(admin.ModelAdmin):
    list_display = ("id", "name", "kitchen")
    search_fields = ("id", "name", "kitchen__id", "kitchen__chef__email")


@admin.register(OfferAvailable)
class OfferAvailableAdmin(admin.ModelAdmin):
    list_display = ("date", "available", "offer")
    search_fields = ("offer__id",)


admin.site.register(OfferDish)
admin.site.register(OfferTime)


# - - - - -


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = ("id", "seller", "buyer")
    search_fields = (
        "id",
        "seller__email",
        "seller__phone",
        "buyer__email",
        "buyer__phone",
    )


admin.site.register(OrderItem)
admin.site.register(OrderItemExtra)
admin.site.register(OrderItemOption)
