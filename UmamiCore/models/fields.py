from django.conf import settings
from django.core.validators import MinValueValidator
from django.db.models.fields import CharField, IntegerField, DecimalField
from django.db.models import FileField
from djmoney.models.fields import MoneyField
from djmoney.models.validators import MinMoneyValidator
from UmamiCore.helpers import RandomFileName
import hashid_field
import logging

logger = logging.getLogger(__name__)


class NonNegativeIntegerField(IntegerField):
    """ An integer field that must hold a non-negative value. Yes,
        PositiveIntegerField historically fulfils this purpose but that is
        under debate and misnamed so here this object is. """

    def __init__(self, *args, **kwargs):
        kwargs["default"] = kwargs.get("default", 0)
        kwargs["validators"] = kwargs.get("validators", [MinValueValidator(0)])
        super().__init__(*args, **kwargs)


class NonNegativeDecimalField(DecimalField):
    """ A decimal field that must hold a non-negative value. Yes,
        PositiveIntegerField historically fulfils this purpose but that is
        under debate and misnamed so here this object is. """

    def __init__(self, *args, **kwargs):
        kwargs["default"] = kwargs.get("default", 0.0)
        kwargs["max_digits"] = kwargs.get("max_digits", 12)
        kwargs["decimal_places"] = kwargs.get("decimal_places", 1)
        kwargs["validators"] = kwargs.get("validators", [MinValueValidator(0.0)])
        super().__init__(*args, **kwargs)


class PriceField(MoneyField):
    """ A money field with default arguments chosen based on the discussion
        found here:
            https://stackoverflow.com/questions/224462/storing-money-in-a-decimal-column-what-precision-and-scale/224866#224866

        Whenever a quantity of money is represented in our DB it will be using
        this field. """

    def __init__(self, *args, **kwargs):
        kwargs["max_digits"] = kwargs.get("max_digits", 19)
        kwargs["decimal_places"] = kwargs.get("decimal_places", 4)
        kwargs["default_currency"] = kwargs.get(
            "default_currency", self.get_default_currency()
        )
        kwargs["validators"] = kwargs.get("validators", [MinMoneyValidator(0)])
        super().__init__(*args, **kwargs)

    @classmethod
    def get_default_currency(cls):
        """ Try to look up the default currency, else use USD. """
        try:
            return settings.DEFAULT_CURRENCY
        except:
            logger.warn("DEFAULT_CURRENCY not set in settings, using 'USD'.")
            return "USD"


class SlugifiedImageField(FileField):
    """ An ImageField configured to upload correctly. """

    def __init__(self, *args, **kwargs):
        kwargs["upload_to"] = kwargs.get("upload_to", RandomFileName("images"))
        kwargs["max_length"] = kwargs.get("max_length", 255)
        kwargs["blank"] = kwargs.get("blank", True)
        kwargs["null"] = kwargs.get("null", True)
        super().__init__(*args, **kwargs)


class StripeTokenField(CharField):
    """ A field configured to hold stripe tokens. """

    def __init__(self, *args, **kwargs):
        # stripe apparently does not share a spec for its tokens so we will reserve
        # more than we need (ids are more like 30 chars in length...)
        kwargs["max_length"] = kwargs.get("max_length", 255)
        kwargs["blank"] = kwargs.get("blank", True)
        super().__init__(*args, **kwargs)


class HashidAutoField(hashid_field.HashidAutoField):
    def __init__(self, *args, **kwargs):
        kwargs["alphabet"] = kwargs.get("alphabet", settings.HASHID_FIELD_ALPHABET)
        super().__init__(*args, **kwargs)
