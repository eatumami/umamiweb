""" Delivery fees model. """

from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext as _
from djmoney.money import Money
from .fields import PriceField
import logging

logger = logging.getLogger(__name__)


class FeeForDelivery(models.Model):
    """ Keep track of delivery fees """

    base_price = PriceField(
        help_text=_("The delivery fee will always be at least this amount.")
    )

    per_mile_price = PriceField(
        help_text=_(
            "The distance beyond `per_mile_grace` will be multiplied by "
            "this amount to caluclate the final price."
        )
    )

    per_mile_grace = models.FloatField(
        help_text=_(
            "Any delivery performed within this distance will cost the "
            "`base_price`. The `per_mile_price` will be applied if the "
            "delivery distance is greater than this amount."
        )
    )

    created_on = models.DateTimeField(
        default=timezone.now,
        editable=False,
        help_text=_("Keeps track of the moment when this entry was added to the DB."),
    )

    effective_on = models.DateTimeField(
        help_text=_(
            "This field is used to determine which instance of this "
            "model applies. The entry with the most recent `effective_on` "
            "that is less than the current datetime will apply."
        )
    )

    class Meta:
        app_label = "UmamiCore"

    @staticmethod
    def calculate(distance_in_miles):
        """ Calculate the cost to deliver with the given distance. """
        now = timezone.now()
        try:
            fee = FeeForDelivery.objects.filter(effective_on__lt=now).latest(
                "effective_on"
            )
        except FeeForDelivery.DoesNotExist:
            logger.warn("No FeeForDelivery has been set. Adding a default fee.")
            # Keep an eye on the hardcoded USD.
            fee = FeeForDelivery.objects.create(
                base_price=Money("2.99", "USD"),
                per_mile_price=Money("0.50", "USD"),
                per_mile_grace=2.0,
                effective_on=now,
            )

        cost = (
            fee.base_price
            + max(0, distance_in_miles - fee.per_mile_grace) * fee.per_mile_price
        )
        cost.amount = round(cost.amount, 2)
        return cost
