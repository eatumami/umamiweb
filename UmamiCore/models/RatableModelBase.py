from django.db.models import Model, F
from .fields import NonNegativeDecimalField, NonNegativeIntegerField


class RatableModelBase(Model):
    """ Common fields and behavior for models that store ratings. """

    rating_total = NonNegativeDecimalField()
    rating_votes = NonNegativeIntegerField()

    class Meta:
        abstract = True

    @property
    def rating(self):
        """ Get the rating. """
        total, votes = self.rating_total, self.rating_votes
        return {"score": total / votes if votes != 0 else 0.0, "votes": votes}

    def _change_rating(self, delta_total, delta_votes):
        self.rating_total = F("rating_total") + delta_total
        self.rating_votes = F("rating_votes") + delta_votes
        self.save(update_fields=["rating_total", "rating_votes"])
