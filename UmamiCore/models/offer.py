import datetime
from django.contrib.gis.db import models
from django.db import transaction
from django.db.models import Case, Count, When
from UmamiSite.helper.OfferHelper import (
    get_current_client_adjusted_datetime,
    get_all_offering_dates,
)

from .fields import HashidAutoField, PriceField


class OfferTime(models.Model):
    """ Time Ranges for an offer """

    kitchen = models.ForeignKey(
        "UmamiCore.Kitchen",
        related_name="offer_time",
        on_delete=models.CASCADE,
        db_index=True,
    )
    start = models.TimeField()
    end = models.TimeField()


class OfferAvailable(models.Model):
    date = models.DateField()
    available = models.IntegerField(null=True)
    offer = models.ForeignKey(
        "UmamiCore.Offer",
        related_name="availables",
        on_delete=models.CASCADE,
        db_index=True,
    )


class Offer(models.Model):
    """ An offer to sell one or more dishes. """

    id = HashidAutoField(min_length=4, primary_key=True)

    kitchen = models.ForeignKey(
        "UmamiCore.Kitchen",
        related_name="offers",
        on_delete=models.CASCADE,
        db_index=True,
    )
    dishes = models.ManyToManyField(
        "UmamiCore.DishIdentity",
        related_name="offers",
        through="UmamiCore.OfferDish",
        db_index=True,
    )
    name = models.CharField(max_length=40, blank=True)
    price = PriceField(null=True, blank=True)
    date = models.DateField()
    lead_time = models.DurationField()
    total_available = models.PositiveSmallIntegerField()
    is_recurring = models.BooleanField(default=False)
    repeat_every = models.CharField(max_length=25, blank=True)
    end_date = models.DateField(null=True, blank=True)
    is_deleted = models.BooleanField(default=False)
    pickup_time = models.ForeignKey(
        "UmamiCore.OfferTime",
        related_name="pickup_offers",
        on_delete=models.PROTECT,
        blank=True,
        null=True,
    )
    deliver_time = models.ForeignKey(
        "UmamiCore.OfferTime",
        related_name="delivery_offers",
        on_delete=models.PROTECT,
        blank=True,
        null=True,
    )
    can_deliver = models.BooleanField()
    can_pickup = models.BooleanField()
    order_by = models.TimeField(db_index=True)

    @classmethod
    def _same_order(cls, ids):
        return Case(*[When(id=id, then=pos) for pos, id in enumerate(ids)])

    @classmethod
    def inc_available(cls, ids, qs):
        """ Increase the available amount for each pair from ids and qs. """
        with transaction.atomic():
            offers = cls.objects.filter(id__in=ids).order_by(cls._same_order(ids))
            for offer, quantity in zip(offers, qs):
                date, q = zip(*quantity.items())
                available = OfferAvailable.objects.select_for_update().get(
                    offer=offer, date=date[0]
                )
                available.available += q[0]
                available.save()
        return offers

    @classmethod
    def dec_available(cls, ids, qs):
        """ Decrease the available amount for each pair from ids and qs. """
        with transaction.atomic():
            offers = cls.objects.filter(id__in=ids).order_by(cls._same_order(ids))

            availables = []
            for offer, quantity in zip(offers, qs):
                date, q = zip(*quantity.items())
                available_obj, created = OfferAvailable.objects.select_for_update().get_or_create(
                    offer=offer, date=date[0]
                )
                available_obj.available = (
                    offer.total_available - q[0]
                    if created
                    else available_obj.available - q[0]
                )
                if available_obj.available < 0:
                    raise ValueError("Too few available")
                availables.append(available_obj)
            for available in availables:
                available.save()
        return offers

    @classmethod
    def drain_available(cls, id, date, multi_delete):
        """ Consume all of the remaining available amount. """

        with transaction.atomic():
            offer = cls.objects.get(id=id)
            offering_dates = get_all_offering_dates(vars(offer))
            date_index = offering_dates.index(date)
            is_multi_delete = eval(multi_delete) if isinstance(multi_delete, str) else multi_delete

            if is_multi_delete:
                if date_index == 0:
                    offer.is_deleted = True
                else:
                    date_index = date_index - 1
                    previous_date = offering_dates[date_index]
                    date_obj = datetime.datetime.strptime(previous_date, "%Y-%m-%d")
                    offer.end_date = date_obj
                offer.save()
            else:
                available_obj, _ = OfferAvailable.objects.select_for_update().get_or_create(
                    offer=offer, date=date
                )
                if len(offering_dates) == 1:
                    offer.is_deleted = True
                    offer.save()
                elif (
                        not offer.is_recurring
                        and (available_obj.available == offer.total_available or not available_obj.available)
                ):
                    offer.is_deleted = True
                    offer.save()
                else:
                    if offer.is_recurring and date_index == 0:
                        if len(offering_dates) >= 2:
                            next_date = offering_dates[1]
                            date_obj = datetime.datetime.strptime(next_date, "%Y-%m-%d")
                            offer.date = date_obj
                            offer.save()
                            return
                    available_obj.available = -1
                    available_obj.save()

    def get_available_for_date(self, date):
        try:
            available_obj = OfferAvailable.objects.get(offer=self, date=date)
            return available_obj.available
        except OfferAvailable.DoesNotExist:
            return self.total_available


class OfferDish(models.Model):
    offer = models.ForeignKey(
        "UmamiCore.Offer", related_name="offerdishs", on_delete=models.CASCADE
    )
    dish = models.ForeignKey(
        "UmamiCore.DishIdentity", related_name="offerdishs", on_delete=models.CASCADE
    )
