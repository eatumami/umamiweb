""" Customer model. """

from django.db import models
from django.utils import timezone
from .fields import StripeTokenField


class Customer(models.Model):
    """ Keep track of customer info. """

    user = models.OneToOneField(
        "UmamiCore.User",
        related_name="customer",
        on_delete=models.CASCADE,
        db_index=True,
    )
    date_joined = models.DateTimeField(default=timezone.now)
    stripe_customer_token = StripeTokenField()

    class Meta:
        app_label = "UmamiCore"
        indexes = [models.Index(fields=["user"])]
