""" Models related to Orders. These are records of transactions. """

from django.db import models
from django.utils import timezone
from phonenumber_field.modelfields import PhoneNumberField
from UmamiCore.helpers import generate_random_order_id

from .fields import HashidAutoField, PriceField, StripeTokenField


class Order(models.Model):
    id = HashidAutoField(min_length=8, primary_key=True)

    timestamp = models.DateTimeField(default=timezone.now, db_index=True)
    buyer = models.ForeignKey("UmamiCore.User", on_delete=models.PROTECT, db_index=True)
    seller = models.ForeignKey(
        "UmamiCore.Kitchen", on_delete=models.PROTECT, db_index=True
    )

    handoff_type = models.CharField(
        max_length=8, choices=[("delivery", "delivery"), ("pickup", "pickup")]
    )
    handoff_address = models.ForeignKey(
        "UmamiCore.Address", related_name="handoff_for", on_delete=models.PROTECT
    )
    handoff_date = models.DateField()
    handoff_time_start = models.TimeField()
    handoff_time_end = models.TimeField()

    notes = models.CharField(max_length=255, blank=True)

    subtotal = PriceField()
    total = PriceField()
    delivery_fee = PriceField()
    platform_fee = PriceField()
    stripe_fee = PriceField()
    refunded_amount = PriceField()
    cancelled = models.BooleanField(default=False)
    stripe_charge_token = StripeTokenField()

    def mark_cancelled(self):
        """ Mark this order as cancelled """
        self.cancelled = True
        self.save()

    def set_refunded_amount(self, refund_amount):
        self.refunded_amount = refund_amount / 100
        self.save()


class OrderItem(models.Model):
    """ A single record of an individual offer item """

    order = models.ForeignKey(
        "UmamiCore.Order", related_name="items", on_delete=models.PROTECT
    )
    offer = models.ForeignKey(
        "UmamiCore.Offer", related_name="order_items", on_delete=models.PROTECT
    )
    quantity = models.PositiveSmallIntegerField()


class OrderItemExtra(models.Model):
    """ Keep track of selected extras in OrderItems. """

    order_item = models.ForeignKey(
        "UmamiCore.OrderItem", on_delete=models.CASCADE, related_name="extras"
    )
    extra = models.ForeignKey(
        "UmamiCore.DishExtra",
        related_name="selected_extras",
        on_delete=models.PROTECT,
        null=True,
    )


class OrderItemOption(models.Model):
    """ Keep track of selected options in OrderItems. """

    order_item = models.ForeignKey(
        "UmamiCore.OrderItem", on_delete=models.PROTECT, related_name="option"
    )
    option = models.ForeignKey(
        "UmamiCore.DishOption",
        related_name="selected_options",
        on_delete=models.PROTECT,
        null=True,
    )
