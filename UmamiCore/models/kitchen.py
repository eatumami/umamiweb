""" Kitchen model. """

import geopy.distance

from .fields import HashidAutoField, SlugifiedImageField, StripeTokenField
from .fees import FeeForDelivery
from .RatableModelBase import RatableModelBase

from django.contrib.gis.measure import D
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from django.utils import timezone


class Kitchen(RatableModelBase):
    """ A kitchen is where food is made and sold. It is not a literal
        physical location, it is a person or organization's identity as
        a seller on our platform. """

    id = HashidAutoField(min_length=4, primary_key=True)

    chef = models.OneToOneField(
        "UmamiCore.User", related_name="kitchen", on_delete=models.CASCADE
    )
    date_joined = models.DateTimeField(default=timezone.now)

    # - - - - - - - - - - - - - - - - - - - -
    # url and public id stuff

    name = models.CharField(max_length=128, blank=True)

    # - - - - - - - - - - - - - - - - - - - -
    # description stuff

    title = models.CharField(max_length=128, blank=True)
    bio = models.TextField(max_length=800, blank=True)
    style = models.CharField(max_length=30, blank=True)

    # - - - - - - - - - - - - - - - - - - - -
    # image stuff

    logo_image = SlugifiedImageField()
    banner_image0 = SlugifiedImageField()
    banner_image1 = SlugifiedImageField()
    banner_image2 = SlugifiedImageField()
    banner_image3 = SlugifiedImageField()
    banner_image4 = SlugifiedImageField()

    # - - - - - - - - - - - - - - - - - - - -
    # Photo ID for verification
    photo_id = SlugifiedImageField()

    # - - - - - - - - - - - - - - - - - - - -
    # settings stuff

    # Store deliver range internally using meters.
    # Need many more digits to represent distances in meters
    deliver_range = models.DecimalField(decimal_places=2, max_digits=10, default=0)
    pickup_address = models.ForeignKey(
        "UmamiCore.Address",
        null=True,
        blank=True,
        on_delete=models.PROTECT,
        db_index=True,
    )

    # - - - - - - - - - - - - - - - - - - - -
    # verification stuff

    latest_verification = models.ForeignKey(
        "UmamiCore.KitchenVerification",
        related_name="latest_verification_of",
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )

    # - - - - - - - - - - - - - - - - - - - -
    # Admin override to skip permits
    umami_override = models.BooleanField(default=False)

    # - - - - - - - - - - - - - - - - - - - -
    # Stripe tokens
    stripe_account_token = StripeTokenField()
    stripe_refresh_token = StripeTokenField()
    stripe_access_token = StripeTokenField()

    # - - - - - - - - - - - - - - - - - - - -

    class Meta:
        app_label = "UmamiCore"
        indexes = [models.Index(fields=["chef"]), models.Index(fields=["name"])]

    @property
    def banner_images(self):
        """ Get the banner images in a list format. """
        images = []
        for i in range(5):
            try:
                image = getattr(self, "banner_image{}".format(i))
                image.url  # raises an exception if the field is null
                images.append(image)
            except:
                pass
        return images

    @property
    def active_verification(self):
        """ Get the active verification. """
        return self.latest_verification

    @property
    def has_verification(self):
        if self.latest_verification:
            return (
                self.latest_verification.permit_number is not None
                and self.latest_verification.permit_number != ""
                and self.has_identification
            )
        return self.umami_override

    @property
    def has_identification(self):
        return bool(self.photo_id)

    @property
    def has_stripe(self):
        return self.stripe_account_token is not None and self.stripe_account_token != ""

    def delivers_to(self, latlon):
        """ Does this kitchen deliver to the given address. """
        return self.distance_to(latlon) <= D(m=self.deliver_range).mi

    def delivery_price(self, latlon):
        """ What will it cost for this kitchen to deliver to the given
            address? """
        return FeeForDelivery.calculate(self.distance_to(latlon))

    def distance_to(self, latlon):
        """ Return the distance from this kitchen's current address to the
            given address. """
        try:
            self_address = self.active_verification.address
        except:
            self_address = self.pickup_address
        self_latlon = (self_address.lat, self_address.lon)
        return geopy.distance.distance(self_latlon, latlon).miles

    def __str__(self):
        return "Kitchen(id={}, chef={})".format(self.id, self.chef)


class KitchenFollower(models.Model):
    """ Keeps track of the follower relationship between a user and a
        kitchen. """

    kitchen = models.ForeignKey("UmamiCore.Kitchen", on_delete=models.CASCADE)
    user = models.ForeignKey("UmamiCore.User", on_delete=models.CASCADE)

    class Meta:
        app_label = "UmamiCore"
        unique_together = ("kitchen", "user")


class KitchenVerification(models.Model):
    """ Provides verification info for a Kitchen. Kitchens that don't have
        an up to date verification should not be able to sell anything. """

    kitchen = models.ForeignKey(
        "UmamiCore.Kitchen", on_delete=models.CASCADE, related_name="verifications"
    )
    address = models.ForeignKey("UmamiCore.Address", on_delete=models.PROTECT)
    date_verified = models.DateField()
    date_expires = models.DateField()
    permit_number = models.CharField(max_length=50)
    permit_county = models.CharField(max_length=100)

    class Meta:
        app_label = "UmamiCore"


class FeaturedKitchen(models.Model):
    """A table to keep track of featured kitchens."""

    kitchen = models.OneToOneField(
        "UmamiCore.Kitchen",
        primary_key=True,
        on_delete=models.CASCADE,
        related_name="featured",
    )
