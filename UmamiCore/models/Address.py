import geopy.distance
from django.contrib.gis.db import models
from django.contrib.gis.db.models.functions import Distance
from django.contrib.gis.geos import Point
from django.contrib.gis.measure import D
from django.core.validators import MaxValueValidator, MinValueValidator
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _


class AddressManager(models.Manager):
    def create(self, *args, **kwargs):
        """ Makes creating Address objects easier by filling in the point field automatically. """
        if "point" not in kwargs:
            lon = kwargs.get("lon", 0)
            lat = kwargs.get("lat", 0)
            kwargs.update(point=Point(lon, lat))
        return super(AddressManager, self).create(*args, **kwargs)

    def get_or_create(self, *args, **kwargs):
        """ Makes creating Address objects easier by filling in the point field automatically. """
        if "point" not in kwargs:
            lon = kwargs.get("lon", 0)
            lat = kwargs.get("lat", 0)
            kwargs.update(point=Point(lon, lat))
        return super(AddressManager, self).get_or_create(*args, **kwargs)

    def near(self, reference_point, within=250):
        """ Lookup addresses near the reference point, sorted in order of distance """
        return (
            self.filter(point__distance_lte=(reference_point, D(km=within)))
            .annotate(distance=Distance("point", reference_point))
            .order_by("distance")
        )

    def create_address(self, *args, **kwargs):
        addr, _ = self.get_or_create(
            line1__iexact=kwargs.get("line1"),
            line2__iexact=kwargs.get("line2"),
            city__iexact=kwargs.get("city"),
            prov__iexact=kwargs.get("prov"),
            postal_code__iexact=kwargs.get("postal_code"),
            country__iexact=kwargs.get("country"),
            lon=kwargs.get("lon"),
            lat=kwargs.get("lat"),
            defaults=kwargs,  # Don't forget defaults
        )
        return addr


class Address(models.Model):
    objects = AddressManager()

    line1 = models.CharField(max_length=255)
    line2 = models.CharField(max_length=255, blank=True)
    city = models.CharField(max_length=255)
    prov = models.CharField(max_length=255)
    postal_code = models.CharField(max_length=6)
    country = models.CharField(max_length=75)  # longest country name is 74 chars
    lon = models.FloatField(
        validators=[MinValueValidator(-180), MaxValueValidator(180)]
    )
    lat = models.FloatField(validators=[MinValueValidator(-90), MaxValueValidator(90)])
    point = models.PointField(srid=4326, spatial_index=True)

    def distance_to(self, latlon):
        self_latlon = (self.lat, self.lon)
        return geopy.distance.distance(self_latlon, latlon).miles

    def clean(self):
        # make sure that (self.point.x, self.point.y) == (self.lon, self.lat)
        if self.lon != self.point.x:
            raise ValidationError(_("missmatch (lon != point.x)"))
        if self.lat != self.point.y:
            raise ValidationError(_("missmatch (lat != point.y)"))
        return super(Address, self).clean()

    def get_readable_string(self):
        if self.line2:
            return "{}".format(
                ", ".join(
                    [
                        str(self.line1),
                        str(self.line2),
                        str(self.city),
                        str(self.prov),
                        str(self.postal_code),
                        str(self.country),
                    ]
                )
            )
        else:
            return "{}".format(
                ", ".join(
                    [
                        str(self.line1),
                        str(self.city),
                        str(self.prov),
                        str(self.postal_code),
                        str(self.country),
                    ]
                )
            )

    def __str__(self):
        return self.get_readable_string()
