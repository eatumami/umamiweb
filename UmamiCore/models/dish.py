""" Dish related models. """

from datetime import date
from decimal import Decimal
from django.conf import settings
from django.db import models
from django.utils import timezone

from .RatableModelBase import RatableModelBase
from .fields import (
    HashidAutoField,
    NonNegativeDecimalField,
    PriceField,
    SlugifiedImageField,
)


class Dish(models.Model):
    """ All the relevant info about an item of food. There may be multiple
        versions of a `Dish`. Those are represented by different entries in
        this table. """

    id = HashidAutoField(min_length=2, primary_key=True)

    identity = models.ForeignKey(
        "UmamiCore.DishIdentity",
        related_name="versions",
        on_delete=models.CASCADE,
        db_index=True,
    )

    name = models.CharField(max_length=40)
    image = SlugifiedImageField()
    description = models.TextField(blank=True, max_length=525)
    price = PriceField()

    def __str__(self):
        return f"Dish(id={self.id}, name={self.name})"

    @property
    def kitchen(self):
        return self.identity.kitchen


class DishExtra(models.Model):
    """ Extras for Dishes. """

    dish = models.ForeignKey(
        "UmamiCore.Dish", related_name="extras", on_delete=models.CASCADE, db_index=True
    )
    name = models.CharField(max_length=25)
    price = PriceField()

    def __str__(self):
        return f"DishExtra(name={self.name}, price={self.price.amount})"


class DishIdentity(RatableModelBase):
    """ Represents the identity of a dish. """

    id = HashidAutoField(min_length=4, primary_key=True)

    kitchen = models.ForeignKey(
        "UmamiCore.Kitchen",
        related_name="dishes",
        on_delete=models.CASCADE,
        db_index=True,
    )
    latest_version = models.ForeignKey(
        "UmamiCore.Dish",
        related_name="latest_version_of",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
    )

    def __str__(self):
        return f"DishIdentity(id={self.id}, kitchen={self.kitchen}, latest_version={self.latest_version})"


class DishIngredient(models.Model):
    """ Use to make dishes more easily searched by ingredient. """

    dish = models.ForeignKey(
        "UmamiCore.Dish",
        related_name="ingredients",
        on_delete=models.CASCADE,
        db_index=True,
    )
    name = models.CharField(max_length=25)

    def __str__(self):
        return f"DishIngredient(name={self.name})"


class DishIngredientCount(models.Model):
    name = models.CharField(max_length=25, primary_key=True)
    count = models.PositiveIntegerField(default=0)


class DishOption(models.Model):
    """ Options for Offers. """

    dish = models.ForeignKey(
        "UmamiCore.Dish",
        related_name="options",
        on_delete=models.CASCADE,
        db_index=True,
    )
    name = models.CharField(max_length=25)
    price = PriceField()

    def __str__(self):
        return f"DishOption(name={self.name}, price={self.price.amount})"


class DishReview(models.Model):
    """ Rating and review of Dish """

    dish = models.ForeignKey(
        "UmamiCore.DishIdentity",
        related_name="reviews",
        on_delete=models.CASCADE,
        db_index=True,
    )
    user = models.ForeignKey(
        "UmamiCore.User",
        related_name="reviews",
        on_delete=models.CASCADE,
        db_index=True,
    )

    blank = models.BooleanField(default=True)
    text = models.TextField(max_length=255, blank=True)
    rating = NonNegativeDecimalField()
    date = models.DateField(default=date.today)

    def __str__(self):
        return f"DishReview(dish={self.dish}, user={self.user})"


class DishTag(models.Model):
    """ Enables quick filtering of dishes """

    dish = models.ForeignKey(
        "UmamiCore.Dish", related_name="tags", on_delete=models.CASCADE, db_index=True
    )
    name = models.CharField(max_length=25)

    def __str__(self):
        return f"DishTag(name={self.name})"


class DishTagCount(models.Model):
    name = models.CharField(max_length=25, primary_key=True)
    count = models.PositiveIntegerField(default=0)
