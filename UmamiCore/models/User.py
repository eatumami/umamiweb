from allauth.account.models import EmailAddress
from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from phonenumber_field.modelfields import PhoneNumberField

from .kitchen import Kitchen


class UserManager(BaseUserManager):
    def create_user(self, email, first_name, last_name, password, phone):
        """ Creates and saves a user with the given email, name, phone number,
            and password. """

        if not email:
            raise ValueError("Users must have an email address")

        user = self.model(
            email=self.normalize_email(email),
            phone=phone,
            first_name=first_name,
            last_name=last_name,
        )

        user.set_password(password)
        user.save(using=self._db)
        email = EmailAddress.objects.add_email(None, user, user.email)
        return user

    def create_superuser(self, email, phone, first_name, last_name, password):
        """ Creates and saves a superuser with the given email, name,
            phone number, and password. """

        user = self.create_user(
            email=email,
            phone=phone,
            password=password,
            first_name=first_name,
            last_name=last_name,
        )

        user.is_admin = True
        user.save(using=self._db)
        user.verify_email()
        return user


class User(AbstractBaseUser):
    """ User model for Umami. """

    email = models.EmailField(
        verbose_name="email address", max_length=255, unique=True, primary_key=True
    )
    first_name = models.CharField(_("first name"), max_length=30, blank=False)
    last_name = models.CharField(_("last name"), max_length=30, blank=False)
    phone = PhoneNumberField()

    date_joined = models.DateTimeField(_("date joined"), default=timezone.now)
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)
    last_used_address = models.ForeignKey(
        "UmamiCore.Address", null=True, blank=True, on_delete=models.CASCADE
    )
    chef_signup = models.BooleanField(default=False)

    objects = UserManager()

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = ["first_name", "last_name", "phone"]

    @property
    def full_name(self):
        """ Get the user's full name. """
        return "{} {}".format(self.first_name, self.last_name).strip()

    @property
    def short_name(self):
        """ Get a shortened version of the user's name. """
        return self.first_name

    @property
    def is_chef(self):
        """ Determine if the user has a kitchen. """
        return Kitchen.objects.filter(chef=self).exists()

    @property
    def is_staff(self):
        """ Is the user a member of staff? """
        # Simplest possible answer: All admins are staff
        return self.is_admin

    @property
    def is_email_confirmed(self):
        """ Does the user have a verified email address? """
        return EmailAddress.objects.filter(user=self, verified=True).exists()

    def __str__(self):  # __unicode__ on Python 2
        return self.email

    def has_perm(self, perm, obj=None):
        """ Does the user have a specific permission? """
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        """ Does the user have permissions to view the app `app_label`? """
        # Simplest possible answer: Yes, always
        return True

    def verify_email(self):
        """ Mark the primary email as verified. """
        try:
            email = EmailAddress.objects.get_for_user(self, self.email)
            if not email.verified:
                email.verified = True
                email.save()
        except ObjectDoesNotExist:
            email = EmailAddress.objects.add_email(None, self, self.email)
            email.verified = True
            email.save()
