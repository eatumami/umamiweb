import base64
import os
import uuid
import datetime
import pytz

from django.utils.crypto import get_random_string

from django.utils.deconstruct import deconstructible


def uuid2slug(value):
    return base64.b64encode(value.bytes, b"_+")


def generate_random_order_id():
    return get_random_string(length=10, allowed_chars="1234567890").lower()


def combine_date_and_time(date, time, tz_offset=420):
    if isinstance(date, str):
        date = datetime.datetime.strptime(date, "%Y-%m-%d").date()
    if isinstance(time, str):
        time = datetime.datetime.strptime(time, "%H:%M:%S").time()

    combined_date = datetime.datetime.combine(date, time).replace(tzinfo=pytz.utc)

    # We calculate how much time our user's local machine is ahead or behind UTC by using the + or - offset integer
    tz_offset_delta = datetime.timedelta(minutes=tz_offset)

    # We calculate the ready_by time by first adding their earliest_ready_time to the tz_offset_delta

    return combined_date + tz_offset_delta


@deconstructible
class RandomFileName(object):
    """
    Produce a random unique filename using a UUID in the given directory, which has the same file
    extension as the given file.
    E.g.,
        rfn = RandomFileName('folder')
        rfn('some_file.jpg') == 'folder/<random-stuff>.jpg
    Taken directly from SO:
        http://stackoverflow.com/questions/2673647/enforce-unique-upload-file-names-using-django
    """

    def __init__(self, path):
        self.path = os.path.join(path, "%s%s")

    def generate_name(self, extension):
        return self.path % (uuid2slug(uuid.uuid4()), extension)

    def __call__(self, _, filename):
        # @note It's up to the validators to check if it's the correct file type in name or if one even exist.
        extension = os.path.splitext(filename)[1]

        name = self.generate_name(extension)
        while os.path.exists(name):
            name = self.generate_name(extension)

        return name
