from rest_framework import serializers

from UmamiCore.models.User import *


class UserEditSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ("email", "phone", "first_name", "last_name", "chef_signup")

    def validate(self, data):
        return data

    def update(self, instance, validated_data):
        instance.phone = validated_data.get("phone", instance.phone)
        instance.first_name = validated_data.get("first_name", instance.first_name)
        instance.last_name = validated_data.get("last_name", instance.first_name)
        instance.email = validated_data.get("email", instance.email).lower()
        instance.chef_signup = validated_data.get("chef_signup", False)
        instance.save()
        return instance
