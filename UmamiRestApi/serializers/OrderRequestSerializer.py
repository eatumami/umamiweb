from rest_framework import serializers
from .AddressSerializer import AddressSerializer


class _OrderRequestOptionSerializer(serializers.Serializer):
    name = serializers.CharField()


class _OrderRequestExtraSerializer(serializers.Serializer):
    name = serializers.CharField()


class _OrderRequestChoiceSerializer(serializers.Serializer):
    dish = serializers.CharField()
    version = serializers.CharField()
    option = _OrderRequestOptionSerializer(allow_null=True)
    extras = _OrderRequestExtraSerializer(many=True)


class _OrderRequestItemSerialzier(serializers.Serializer):
    uuid = serializers.CharField()
    offer = serializers.CharField()
    quantity = serializers.IntegerField()
    choices = _OrderRequestChoiceSerializer(many=True)
    kitchen = serializers.CharField()
    delivery = serializers.BooleanField()
    date = serializers.DateField(required=True)


class OrderRequestSerializer(serializers.Serializer):
    stripe_token = serializers.CharField(allow_blank=True, allow_null=True)
    delivery_address = AddressSerializer(required=False, allow_null=True)
    items = _OrderRequestItemSerialzier(many=True)
    notes = serializers.CharField(allow_blank=True, allow_null=True)
