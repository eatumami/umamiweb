from rest_framework import serializers
from .fields import Base64ImageField
from . import DishPreviewSerializer


class OfferPreviewSerializer(serializers.Serializer):
    id = serializers.ReadOnlyField(source="pk.hashid", read_only=True)
    url = serializers.HyperlinkedIdentityField(view_name="offer_by_id", read_only=True)
    kitchen = serializers.ReadOnlyField(source="kitchen.pk.hashid")
    available = serializers.IntegerField(read_only=True)
    total_available = serializers.IntegerField(read_only=True)
    date = serializers.DateField(read_only=True)
    order_by = serializers.DateTimeField(read_only=True)
    name = serializers.CharField()

    # TODO What should this look like if there is more than one dish?
    dishes = DishPreviewSerializer(read_only=True, many=True)
