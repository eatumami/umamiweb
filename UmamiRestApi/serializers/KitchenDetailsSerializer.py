from django.urls import reverse
from django.utils.text import slugify
from rest_framework import serializers
from .fields import Base64ImageField
from .DishPreviewSerializer import DishPreviewSerializer
from .OfferPreviewSerializer import OfferPreviewSerializer
from .OfferSerializer import OfferDisplaySerializer


class KitchenDetailsSerializer(serializers.Serializer):
    """ A detailed view of a Kitchen. """

    id = serializers.ReadOnlyField(source="pk.hashid")
    url = serializers.HyperlinkedIdentityField(
        view_name="kitchen_by_id", read_only=True
    )
    social_url = serializers.SerializerMethodField()
    title = serializers.CharField()
    bio = serializers.CharField()
    style = serializers.CharField()
    rating = serializers.ReadOnlyField()
    logo_image = Base64ImageField()
    banner_images = serializers.SerializerMethodField()

    # current_offers = OfferDisplaySerializer(many=True, read_only=True)
    # past_offers = DishPreviewSerializer(many=True, read_only=True)
    permit_number = serializers.CharField(read_only=True)
    permit_county = serializers.CharField(read_only=True)
    location = serializers.SerializerMethodField()
    has_identification = serializers.BooleanField(read_only=True)

    def get_social_url(self, instance):
        request = self.context.get("request")
        local_uri = reverse(
            "kitchen_social_url",
            kwargs={"title": slugify(instance.title), "pk": instance.pk.hashid},
        )
        return request.build_absolute_uri(local_uri) if request else local_uri

    def get_banner_images(self, instance):
        return [
            self.context["request"].build_absolute_uri(image.url)
            for image in instance.banner_images
        ]

    def get_location(self, instance):
        return f"{instance.pickup_address.city}, {instance.pickup_address.prov}"

    def create(self, validated_data):
        raise NotImplementedError()

    def update(self, instance, validated_data):
        raise NotImplementedError()
