from rest_framework import serializers
from UmamiCore.models import Address


class AddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = Address
        fields = (
            "line1",
            "line2",
            "city",
            "prov",
            "country",
            "postal_code",
            "lon",
            "lat",
        )

    def create(self, validated_data):
        return Address.objects.create_address(**validated_data)
