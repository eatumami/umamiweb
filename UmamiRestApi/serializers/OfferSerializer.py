import datetime
from django.urls import reverse
from django.utils.text import slugify
from UmamiCore.entities import Offer
from rest_framework import serializers
from .DishSerializerBase import DishSerializerBase
from .KitchenPreviewSerializer import KitchenPreviewSerializer
from UmamiRestApi.helpers import get_google_static_map
from UmamiSite.helper.OfferHelper import *


class _ShallowDishSerializer(DishSerializerBase):
    """ Serializes dishes without including the kitchen. Kitchen will be
        found in the parent offer. """

    pass


class OfferTimeSerializer(serializers.Serializer):
    id = serializers.ReadOnlyField()
    start = serializers.TimeField()
    end = serializers.TimeField()


class OfferAvailableSerializer(serializers.Serializer):
    date = serializers.DateField()
    available = serializers.IntegerField()


class OfferSerializer(serializers.Serializer):
    id = serializers.ReadOnlyField(source="pk.hashid")
    name = serializers.CharField(required=False)
    price = serializers.DecimalField(max_digits=12, decimal_places=2, required=False)

    url = serializers.HyperlinkedIdentityField(view_name="offer_by_id", read_only=True)
    social_url = serializers.SerializerMethodField()
    kitchen = KitchenPreviewSerializer(read_only=True)
    dishes = _ShallowDishSerializer(read_only=True, many=True)

    is_bundle = serializers.BooleanField(read_only=True)
    is_expired = serializers.BooleanField(read_only=True)
    order_by = serializers.TimeField(read_only=True)
    postal_code = serializers.CharField(source="kitchen.postal_code", read_only=True)
    lon = serializers.FloatField(source="kitchen.lon", read_only=True)
    lat = serializers.FloatField(source="kitchen.lat", read_only=True)
    distance = serializers.SerializerMethodField(read_only=True)

    end_date = serializers.DateField(required=False)
    date = serializers.DateField()
    availables = OfferAvailableSerializer(read_only=True, many=True)
    total_available = serializers.IntegerField()
    lead_time = serializers.DurationField()
    is_recurring = serializers.BooleanField()
    is_deleted = serializers.BooleanField(read_only=True)
    repeat_every = serializers.CharField(required=False)
    deliver_time = OfferTimeSerializer(required=False, allow_null=True)
    pickup_time = OfferTimeSerializer(required=False, allow_null=True)

    def get_social_url(self, instance):
        request = self.context.get("request")
        base_uri_name = (
            "offer_bundle_social_url"
            if instance.is_bundle
            else "offer_single_social_url"
        )
        local_uri = reverse(
            base_uri_name,
            kwargs={"title": slugify(instance.name), "pk": instance.pk.hashid},
        )
        return request.build_absolute_uri(local_uri) if request else local_uri

    def get_distance(self, instance):
        try:
            ref = self.context["reference_point"]
            return "{} mi".format(
                round(instance.kitchen.distance_to((ref.y, ref.x)), 2)
            )
        except KeyError:  # no reference_point
            return None

    def create(self, validated_data):
        offer = Offer(
            kitchen=self.context["kitchen"],
            dishes=self.context["dishes"],
            pickup_time=self.context.get("pickup_time", None),
            deliver_time=self.context.get("deliver_time", None),
        )
        self.update(offer, validated_data)
        return offer

    def update(self, instance, validated_data):
        for key, value in validated_data.items():
            setattr(instance, key, value)
        instance.set_order_by()
        instance.save()
        return instance


class OfferDisplaySerializer(OfferSerializer):
    can_deliver = serializers.SerializerMethodField()
    can_pickup = serializers.SerializerMethodField()
    is_expired = serializers.SerializerMethodField()
    map_image = serializers.SerializerMethodField()
    ready_dates = serializers.SerializerMethodField()

    def get_ready_dates(self, instance):
        return get_future_available_date_with_display(vars(instance.model))

    def get_is_expired(self, instance):
        try:
            date = self.context["date"]
            if isinstance(date, str):
                date = datetime.datetime.strptime(date, "%Y-%m-%d")
            return not instance.is_available_for_order(date)
        except KeyError:
            return instance.is_expired

    def get_can_deliver(self, instance):
        try:
            ref = self.context["reference_point"]
            if not instance.can_deliver:
                return False
            return instance.kitchen.delivers_to((ref.y, ref.x))
        except KeyError:
            return instance.can_deliver

    def get_can_pickup(self, instance):
        try:
            ref = self.context["reference_point"]
            if not instance.can_pickup:
                return False
            pa = instance.kitchen.pickup_address
            if pa is None:
                return False
            return pa.distance_to((ref.y, ref.x)) <= 20.0
        except KeyError:
            return instance.can_pickup

    def get_map_image(self, instance):
        try:
            if (
                self.context["user_address"]
                or instance.kitchen.lat
                and instance.kitchen.lon
            ):
                return get_google_static_map(
                    self.context["user_address"],
                    instance.kitchen.lat,
                    instance.kitchen.lon,
                    700,
                    250,
                )
        except KeyError:
            return get_google_static_map(
                None, instance.kitchen.lat, instance.kitchen.lon, 700, 250
            )


class OfferCreateSerializer(OfferSerializer):
    can_deliver = serializers.BooleanField(write_only=True)
    can_pickup = serializers.BooleanField(write_only=True)


class MultiOffersDisplaySerializer(OfferDisplaySerializer):
    available = serializers.SerializerMethodField()

    def get_available(self, instance):
        date_string = self.context["date"]
        return instance.get_available_for_date(date_string)
