from rest_framework import serializers


class DishReviewSerializer(serializers.Serializer):
    """ Serializer for `DishReview`. """

    # Is user.full_name a good repr for users?
    user = serializers.CharField(source="user.full_name", read_only=True)
    date = serializers.DateField(read_only=True)
    blank = serializers.BooleanField(read_only=True)

    text = serializers.CharField(allow_blank=True)
    rating = serializers.DecimalField(max_digits=2, decimal_places=1)

    def create(self, validated_data):
        raise NotImplementedError()

    def update(self, instance, validated_data):
        for k, v in validated_data.items():
            setattr(instance, k, v)
        instance.save()
        return instance
