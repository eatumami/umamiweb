from rest_framework import serializers


class KitchenVerificationSerializer(serializers.Serializer):
    date_expires = serializers.DateField(required=False, allow_null=True)
    permit_number = serializers.CharField(required=False, allow_blank=True)
    permit_county = serializers.CharField(required=False, allow_blank=True)

    def create(self, validated_data):
        raise NotImplementedError()

    def update(self, instance, validated_data):
        instance.save()
        return instance
