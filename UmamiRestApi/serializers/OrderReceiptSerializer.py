from django.utils import timezone
from rest_framework import serializers
from .AddressSerializer import AddressSerializer
from .KitchenPreviewSerializer import KitchenPreviewSerializer
from .DishPreviewSerializer import DishPreviewSerializer
from .DishReviewSerializer import DishReviewSerializer
from UmamiCore.helpers import combine_date_and_time


class _OrderReceiptOptionSerializer(serializers.Serializer):
    name = serializers.CharField()
    subtotal = serializers.DecimalField(max_digits=12, decimal_places=2)


class _OrderReceiptExtraSerializer(serializers.Serializer):
    name = serializers.CharField()
    subtotal = serializers.DecimalField(max_digits=12, decimal_places=2)


class _OrderReceiptChoiceSerializer(serializers.Serializer):
    dish = DishPreviewSerializer()
    option = _OrderReceiptOptionSerializer(allow_null=True)
    extras = _OrderReceiptExtraSerializer(many=True)
    subtotal = serializers.DecimalField(max_digits=12, decimal_places=2)
    review = DishReviewSerializer(read_only=True, allow_null=True)


class _OrderReceiptItemSerialzier(serializers.Serializer):
    uuid = serializers.CharField()
    offer = serializers.CharField()
    name = serializers.CharField()
    available = serializers.IntegerField()
    quantity = serializers.IntegerField()
    choices = _OrderReceiptChoiceSerializer(many=True)
    subtotal = serializers.DecimalField(max_digits=12, decimal_places=2)


class IndividualOrderReceiptSerializer(serializers.Serializer):
    id = serializers.ReadOnlyField(source="pk.hashid")
    notes = serializers.CharField(allow_blank=True, allow_null=True)
    buyer_fullname = serializers.CharField(source="buyer.full_name")
    buyer_phone = serializers.CharField(source="buyer.phone")
    buyer_email = serializers.CharField(source="buyer.email")

    kitchen = KitchenPreviewSerializer(source="seller.chef.kitchen", read_only=True)
    seller_fullname = serializers.CharField(source="seller.chef.full_name")
    seller_phone = serializers.CharField(source="seller.chef.phone")
    seller_email = serializers.CharField(source="seller.chef.email")

    handoff_type = serializers.CharField()
    handoff_address = AddressSerializer()
    handoff_date = serializers.DateField()
    handoff_time_start = serializers.TimeField()
    handoff_time_end = serializers.TimeField()
    ended = serializers.SerializerMethodField(read_only=True)

    items = _OrderReceiptItemSerialzier(many=True)

    total = serializers.DecimalField(max_digits=12, decimal_places=2)
    subtotal = serializers.DecimalField(max_digits=12, decimal_places=2)
    stripe_fee = serializers.DecimalField(max_digits=12, decimal_places=2)
    platform_fee = serializers.DecimalField(max_digits=12, decimal_places=2)
    delivery_fee = serializers.DecimalField(max_digits=12, decimal_places=2)
    refunded_amount = serializers.DecimalField(max_digits=12, decimal_places=2)
    cancelled = serializers.BooleanField()
    is_refundable = serializers.BooleanField()
    is_cancellable = serializers.BooleanField()

    def get_ended(self, instance):
        return (
                combine_date_and_time(
                instance.handoff_date, instance.handoff_time_end
            )
                < timezone.now()
        )


class OrderReceiptSerializer(serializers.Serializer):
    pickup_orders = IndividualOrderReceiptSerializer(many=True)
    delivery_orders = IndividualOrderReceiptSerializer(many=True)
    subtotal = serializers.DecimalField(max_digits=12, decimal_places=2)
    platform_fee = serializers.DecimalField(max_digits=12, decimal_places=2)
    delivery_fee = serializers.DecimalField(max_digits=12, decimal_places=2)
    stripe_fee = serializers.DecimalField(max_digits=12, decimal_places=2)
    total = serializers.DecimalField(max_digits=12, decimal_places=2)
