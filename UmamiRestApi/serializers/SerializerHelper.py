from django.utils import timezone


def get_food_image(request, dish):
    return request.build_absolute_uri(dish["image"])


def format_phone(phone):
    try:
        if len(phone) == 10:
            formatted = (
                "(" + str(phone[0:3]) + ") " + str(phone[3:6]) + "-" + str(phone[6:10])
            )
            return formatted
        return phone
    except IndexError:
        return phone


def determine_offer_availability(offer_serialized):
    ready_time = offer_serialized.ready_by
    if timezone.now() >= ready_time:
        return "Available now"
    return "Order before " + ready_time.strftime("%-I:%M%p")
