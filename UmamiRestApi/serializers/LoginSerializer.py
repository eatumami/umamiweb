from django.contrib.auth import authenticate
from rest_framework import serializers
from allauth.account.models import EmailAddress
import rest_auth.serializers


class LoginSerializer(rest_auth.serializers.LoginSerializer):
    # Overriding the rest_auth's LoginSerializer due to lack of email confirmation resend
    # when logging in without a verified email account
    email = serializers.EmailField(required=True)
    password = serializers.CharField(write_only=True)

    def validate(self, attrs):
        email = attrs.get("email")
        password = attrs.get("password")
        user = authenticate(email=email, password=password)
        if user:
            try:
                email_address = user.emailaddress_set.get(email=user.email)
                if not email_address.verified:
                    email_address.send_confirmation(request=self.context.get("request"))
            except EmailAddress.DoesNotExist:
                email = EmailAddress.objects.add_email(
                    self.context.get("request"), user=user, email=email, confirm=True
                )
                email.save()
        return super(LoginSerializer, self).validate(attrs)
