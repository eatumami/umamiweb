from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers
from .AddressSerializer import AddressSerializer

from UmamiCore.models.User import *


class UserSerializer(serializers.ModelSerializer):
    password1 = serializers.CharField(label="Password", write_only=True)
    password2 = serializers.CharField(label="Password confirmation", write_only=True)
    last_used_address = AddressSerializer(allow_null=True)

    class Meta:
        model = User
        fields = (
            "email",
            "phone",
            "first_name",
            "last_name",
            "password1",
            "password2",
            "last_used_address",
            "chef_signup",
        )

    def validate(self, data):
        # Password validation
        password1 = data.pop("password1")
        password2 = data.pop("password2")
        if password1 and password2 and password1 != password2:
            raise serializers.ValidationError(_("Passwords don't match"))
        data["password"] = password2
        # Existing Email validation
        email = data.get("email", None)
        user = User.objects.filter(email__iexact=email)
        if user.exists():
            raise serializers.ValidationError(
                _("Account with that email already exists")
            )
        return data

    def create(self, validated_data):
        validated_data["email"] = validated_data["email"].lower()
        user = User.objects.create_user(**validated_data)
        user.save()
        return user
