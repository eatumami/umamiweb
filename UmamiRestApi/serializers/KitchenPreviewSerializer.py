from django.urls import reverse
from django.utils.text import slugify
from rest_framework import serializers
from .fields import Base64ImageField


class KitchenPreviewSerializer(serializers.Serializer):
    id = serializers.ReadOnlyField(source="pk.hashid")
    url = serializers.HyperlinkedIdentityField(
        view_name="kitchen_by_id", read_only=True
    )
    social_url = serializers.SerializerMethodField()
    title = serializers.ReadOnlyField()
    logo_image = Base64ImageField(read_only=True)
    banner_images = serializers.SerializerMethodField()
    rating = serializers.ReadOnlyField()

    def get_social_url(self, instance):
        request = self.context.get("request")
        local_uri = reverse(
            "kitchen_social_url",
            kwargs={"title": slugify(instance.title), "pk": instance.pk.hashid},
        )
        return request.build_absolute_uri(local_uri) if request else local_uri

    def get_banner_images(self, instance):
        return [
            self.context["request"].build_absolute_uri(image.url)
            for image in instance.banner_images
        ]
