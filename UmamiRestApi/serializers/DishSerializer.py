from rest_framework import serializers
from UmamiCore import entities
from .DishSerializerBase import DishSerializerBase
from .KitchenPreviewSerializer import KitchenPreviewSerializer


class DishSerializer(DishSerializerBase):

    kitchen = KitchenPreviewSerializer(read_only=True)

    def create(self, validated_data):
        dish = entities.Dish(kitchen=self.context["kitchen"])
        return self.update(dish, validated_data)

    def update(self, dish, validated_data):

        options = validated_data.pop("options", None)
        extras = validated_data.pop("extras", None)
        ingredients = validated_data.pop("ingredients", None)
        tags = validated_data.pop("tags", None)

        for key, value in validated_data.items():
            setattr(dish, key, value)

        if options is not None:
            for option in options:
                dish.add_option(option["name"], option["price"])
        else:
            dish.carry_over_options()

        if extras is not None:
            for extra in extras:
                dish.add_extra(extra["name"], extra["price"])
        else:
            dish.carry_over_extras()

        if ingredients is not None:
            for ingredient in ingredients:
                dish.add_ingredient(ingredient["name"])
        else:
            dish.carry_over_ingredients()

        if tags is not None:
            for tag in tags:
                dish.add_tag(tag["name"])
        else:
            dish.carry_over_tags()

        dish.save()
        return dish
