from rest_framework import serializers
from UmamiCore.models import Address
from .AddressSerializer import AddressSerializer
from .fields import Base64ImageField


class KitchenEditSerializer(serializers.Serializer):
    """ Read/write settings and profile for a Kitchen. """

    id = serializers.ReadOnlyField(source="pk.hashid")
    url = serializers.HyperlinkedIdentityField(
        view_name="kitchen_by_id", read_only=True
    )
    title = serializers.CharField()
    bio = serializers.CharField()
    style = serializers.CharField()
    logo_image = Base64ImageField(allow_null=True)
    banner_image0 = Base64ImageField(allow_null=True)
    banner_image1 = Base64ImageField(allow_null=True)
    banner_image2 = Base64ImageField(allow_null=True)
    banner_image3 = Base64ImageField(allow_null=True)
    banner_image4 = Base64ImageField(allow_null=True)
    pickup_address = AddressSerializer(allow_null=True)
    deliver_range = serializers.DecimalField(max_digits=12, decimal_places=2)
    stripe_account_token = serializers.CharField(read_only=True)

    def create(self, validated_data):
        raise NotImplementedError()

    def update(self, instance, validated_data):
        if "pickup_address" in validated_data:
            address = validated_data.pop("pickup_address")
            if address is not None:
                # TODO use an entity. Use a transaction.
                address = Address.objects.create_address(**address)
            instance.pickup_address = address

        for key, value in validated_data.items():
            setattr(instance, key, value)

        instance.save()
        return instance
