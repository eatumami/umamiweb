from rest_framework import serializers
from .fields import Base64ImageField


class DishPreviewSerializer(serializers.Serializer):
    id = serializers.ReadOnlyField(source="pk.hashid")
    url = serializers.HyperlinkedIdentityField(view_name="dish_by_id", read_only=True)
    kitchen = serializers.ReadOnlyField(source="kitchen.pk.hashid")
    name = serializers.CharField()
    image = Base64ImageField(read_only=True)
    rating = serializers.ReadOnlyField()
