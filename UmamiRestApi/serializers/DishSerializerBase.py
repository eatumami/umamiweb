from rest_framework import serializers
from .fields import Base64ImageField


class DishExtraSerializer(serializers.Serializer):
    name = serializers.CharField()
    price = serializers.DecimalField(max_digits=12, decimal_places=2)


class DishIngredientSerializer(serializers.Serializer):
    name = serializers.CharField()


class DishOptionSerializer(serializers.Serializer):
    name = serializers.CharField()
    price = serializers.DecimalField(max_digits=12, decimal_places=2)


class DishTagSerializer(serializers.Serializer):
    name = serializers.CharField()


class DishSerializerBase(serializers.Serializer):
    id = serializers.ReadOnlyField(source="pk.hashid")
    url = serializers.HyperlinkedIdentityField(view_name="dish_by_id", read_only=True)
    version = serializers.ReadOnlyField(source="version.hashid")
    rating = serializers.ReadOnlyField()

    name = serializers.CharField()
    description = serializers.CharField()
    image = Base64ImageField()
    price = serializers.DecimalField(max_digits=12, decimal_places=2)

    options = DishOptionSerializer(many=True)
    extras = DishExtraSerializer(many=True)
    ingredients = DishIngredientSerializer(many=True)
    tags = DishTagSerializer(many=True)
    # reviews = DishReviewSerializer(read_only=True, many=True)
