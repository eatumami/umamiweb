import rest_auth.registration.serializers
from rest_framework import serializers
from phonenumber_field.serializerfields import PhoneNumberField


class RegisterSerializer(rest_auth.registration.serializers.RegisterSerializer):

    # the save method on this object talks to allauth, and uses
    # UmamiCore.adapters.AccountAdapter to create the user model.
    # The superclass also has validation for passwords and email.

    email = serializers.EmailField(required=True)
    first_name = serializers.CharField(write_only=True)
    last_name = serializers.CharField(write_only=True)
    phone = PhoneNumberField()
    password1 = serializers.CharField(write_only=True)
    password2 = serializers.CharField(write_only=True)
    chef_signup = serializers.BooleanField()

    def validate_first_name(self, first_name):
        """ Is the given first_name valid? """
        return first_name  # TODO What is a valid first_name?

    def validate_last_name(self, last_name):
        """ Is the given first_name valid? """
        return last_name  # TODO What is a valid last_name?

    def get_cleaned_data(self):
        return {
            "email": self.validated_data.get("email", ""),
            "first_name": self.validated_data.get("first_name", ""),
            "last_name": self.validated_data.get("last_name", ""),
            "phone": self.validated_data.get("phone", ""),
            "password1": self.validated_data.get("password1", ""),
            "password2": self.validated_data.get("password2", ""),
            "chef_signup": self.validated_data.get("chef_signup", False),
        }
