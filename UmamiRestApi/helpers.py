import logging
from django.conf import settings
from django.core.mail import EmailMessage, send_mail

from django.template.loader import render_to_string
from twilio.rest import Client
from twilio.base.exceptions import TwilioRestException

BUYER_CONFIRMATION_MSG = "Thank you, we have received your order!"
SELLER_CONFIRMATION_MSG = "You have received a new order!"

logger = logging.getLogger(__name__)


def get_google_static_map(home, lat, lon, width, height):
    food_marker = "https://i.imgur.com/emN3N67.png"
    if home:
        base_url = (
            f"https://maps.googleapis.com/maps/api/staticmap?size="
            + f"{str(width)}x{str(height)}&maptype=roadmap&markers={home}"
            + f"&markers=icon:{food_marker}|{lat},{lon}&key={settings.GOOGLE_API_KEY}"
        )
    else:
        base_url = (
            f"https://maps.googleapis.com/maps/api/staticmap?size="
            + f"{str(width)}x{str(height)}&maptype=roadmap&markers="
            + f"icon:{food_marker}|{lat},{lon}&key={settings.GOOGLE_API_KEY}"
        )

    return base_url


def send_order_confirmation_sms(user, order_receipt):
    try:
        logger.info("Sending order confirmation sms")
        client = Client(settings.TWILIO_ACCOUNT_SID, settings.TWILIO_AUTH_TOKEN)
        for order in order_receipt.data:
            offer_names_with_qty = [
                f"{item['quantity']} x {item['name']}" for item in order["items"]
            ]
            offer_qty_string = "\n".join(offer_names_with_qty)
            if order["buyer_phone"]:
                client.messages.create(
                    body=f"Your Umami Order from {order['kitchen']['title']} is confirmed for {order['handoff_type']} on "
                    + f"{order['handoff_date']}. \nSummary: \n{offer_qty_string} \nTotal: ${order['total']} \nSee eatumami.co/purchased for more details.",
                    from_=settings.TWILIO_NUMBER,
                    to=order["buyer_phone"],
                )
            if order["seller_phone"]:
                client.messages.create(
                    body=f"You have received an order from {user.full_name} for {order['handoff_type']} on "
                    + f"{order['handoff_date']}. \nSummary: \n{offer_qty_string} \nTotal: ${order['total']} \nSee eatumami.co/kitchen/upcoming for more details.",
                    from_=settings.TWILIO_NUMBER,
                    to=order["seller_phone"],
                )
        logger.info("Sending order confirmation sms")
    except TwilioRestException:
        pass


def send_order_confirmation_email(user, order_receipt):
    logger.info("Sending order confirmation emails")
    for order in order_receipt.data:
        subject_line_buyer = (
            f"Order Confirmation for {user.full_name} from {order['kitchen']['title']}"
        )
        buyer_msg_plain = render_to_string(
            "account/email/html/order_confirmation.txt",
            {"order": order, "msg": BUYER_CONFIRMATION_MSG, "role": "buyer"},
        )
        buyer_msg_html = render_to_string(
            "account/email/html/order_confirmation.html",
            {"order": order, "msg": BUYER_CONFIRMATION_MSG, "role": "buyer"},
        )

        subject_line_seller = f"You have received a new order from {user.full_name}"
        seller_msg_plain = render_to_string(
            "account/email/html/order_confirmation.txt",
            {"order": order, "msg": SELLER_CONFIRMATION_MSG, "role": "seller"},
        )
        seller_msg_html = render_to_string(
            "account/email/html/order_confirmation.html",
            {"order": order, "msg": SELLER_CONFIRMATION_MSG, "role": "seller"},
        )

        send_mail(
            subject=subject_line_seller,
            message=seller_msg_plain,
            from_email=f"Umami Order <{settings.DEFAULT_FROM_EMAIL}>",
            recipient_list=[order["seller_email"]],
            html_message=seller_msg_html,
            fail_silently=True,
        )
        send_mail(
            subject=subject_line_buyer,
            message=buyer_msg_plain,
            from_email=f"Umami Order <{settings.DEFAULT_FROM_EMAIL}>",
            recipient_list=[user.email],
            html_message=buyer_msg_html,
            fail_silently=True,
        )
    logger.info("Order confirmation emails sent")


def send_refund_confirmation_email(user, order, pubk):
    logger.info("Sending refund emails")
    subject_line_buyer = f"Refund for {order.buyer.full_name} from {order.seller.title}"
    buyer_msg_plain = render_to_string(
        "account/email/html/refund_confirmation_buyer.txt",
        {
            "order": order,
            "msg": "You have received a refund!",
            "role": "buyer",
            "pubk": pubk,
        },
    )
    buyer_msg_html = render_to_string(
        "account/email/umami-html-email.html",
        {
            "order": order,
            "msg": "You have received a refund!",
            "role": "buyer",
            "pubk": pubk,
            "email_purpose": "refund_confirm_buyer",
        },
    )

    subject_line_seller = f"You have issue a refund for {user.full_name}"
    seller_msg_plain = render_to_string(
        "account/email/html/refund_confirmation_seller.txt",
        {
            "order": order,
            "msg": "You have issued a refund",
            "role": "seller",
            "pubk": pubk,
        },
    )
    seller_msg_html = render_to_string(
        "account/email/umami-html-email.html",
        {
            "order": order,
            "msg": "You have issued a refund",
            "role": "seller",
            "pubk": pubk,
            "email_purpose": "refund_confirm_seller",
        },
    )

    send_mail(
        subject=subject_line_seller,
        message=seller_msg_plain,
        from_email=f"Umami Refund <{settings.DEFAULT_FROM_EMAIL}>",
        recipient_list=[order.seller.chef.email],
        html_message=seller_msg_html,
        fail_silently=True,
    )
    send_mail(
        subject=subject_line_buyer,
        message=buyer_msg_plain,
        from_email=f"Umami Refund <{settings.DEFAULT_FROM_EMAIL}>",
        recipient_list=[order.buyer.email],
        html_message=buyer_msg_html,
        fail_silently=True,
    )
    logger.info("Refund emails sent")


def send_refund_confirmation_sms(user, order):
    logger.info("Sending refund sms")
    client = Client(settings.TWILIO_ACCOUNT_SID, settings.TWILIO_AUTH_TOKEN)
    if order.buyer.phone:
        client.messages.create(
            body=f"[Umami Refund] You have received a refund from {order.seller.title} for {order.refunded_amount}.",
            from_=settings.TWILIO_NUMBER,
            to=str(order.buyer.phone),
        )
    if order.seller.chef.phone:
        client.messages.create(
            body=f"[Umami Refund] You have issued a refund to {user.full_name} for {order.refunded_amount}.",
            from_=settings.TWILIO_NUMBER,
            to=str(order.seller.chef.phone),
        )
    logger.info("Refund sms sent")


def send_cancel_confirmation_email(user, order, pubk):
    logger.info("Sending cancellation emails")
    subject_line_buyer = f"Refund for {order.buyer.full_name} from {order.seller.title}"
    buyer_msg_plain = render_to_string(
        "account/email/html/refund_confirmation_buyer.txt",
        {
            "order": order,
            "msg": "Your order was cancelled",
            "role": "buyer",
            "pubk": pubk,
        },
    )
    buyer_msg_html = render_to_string(
        "account/email/umami-html-email.html",
        {
            "order": order,
            "msg": "Your order was cancelled",
            "role": "buyer",
            "pubk": pubk,
            "email_purpose": "refund_confirm_buyer",
        },
    )

    subject_line_seller = f"You have issue a refund for {user.full_name}"
    seller_msg_plain = render_to_string(
        "account/email/html/refund_confirmation_seller.txt",
        {
            "order": order,
            "msg": "You have issued a cancellation",
            "role": "seller",
            "pubk": pubk,
        },
    )
    seller_msg_html = render_to_string(
        "account/email/umami-html-email.html",
        {
            "order": order,
            "msg": "You have issued a cancellation",
            "role": "seller",
            "pubk": pubk,
            "email_purpose": "refund_confirm_seller",
        },
    )

    send_mail(
        subject=subject_line_seller,
        message=seller_msg_plain,
        from_email=f"Umami Cancellation <{settings.DEFAULT_FROM_EMAIL}>",
        recipient_list=[order.seller.chef.email],
        html_message=seller_msg_html,
        fail_silently=True,
    )
    send_mail(
        subject=subject_line_buyer,
        message=buyer_msg_plain,
        from_email=f"Umami Cancellation <{settings.DEFAULT_FROM_EMAIL}>",
        recipient_list=[order.buyer.email],
        html_message=buyer_msg_html,
        fail_silently=True,
    )
    logger.info("Refund emails sent")


def send_cancel_confirmation_sms(user, order):
    logger.info("Sending cancellation sms")
    client = Client(settings.TWILIO_ACCOUNT_SID, settings.TWILIO_AUTH_TOKEN)
    if order.buyer.phone:
        client.messages.create(
            body=f"[Umami Cancellation] Your order was cancelled from {order.seller.title} for {order.refunded_amount}. Refund will take a few days to process.",
            from_=settings.TWILIO_NUMBER,
            to=str(order.buyer.phone),
        )
    if order.seller.chef.phone:
        client.messages.create(
            body=f"[Umami Cancellation] You have cancelled an order to {user.full_name} for {order.refunded_amount}. Refund will take a few days to process.",
            from_=settings.TWILIO_NUMBER,
            to=str(order.seller.chef.phone),
        )
    logger.info("Refund sms sent")


def send_email_to_admins_for_kitchen_verification(user, photo_id, has_permit):
    logger.info("Sending kitchen verification email")
    msg = render_to_string(
        "account/email/verify_kitchen.txt",
        {"photo_id": photo_id, "user": user, "has_permit": has_permit},
    )

    verification_email = EmailMessage(
        subject="Verify New Umami Kitchen From Photo ID",
        body=msg,
        from_email=settings.DEFAULT_FROM_EMAIL,
        to=settings.UMAMI_SUPPORT_MAILING_LIST,
    )
    verification_email.send(fail_silently=True)
    logger.info("Kitchen verification email sent")
