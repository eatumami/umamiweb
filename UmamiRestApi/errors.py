import logging
import stripe

from django.conf import settings
from django.core.exceptions import ValidationError as DjValidationError
from functools import wraps
from rest_framework import status
from rest_framework.response import Response
from rest_framework.exceptions import APIException, ValidationError
from rest_framework.views import exception_handler as default_exception_handler
from UmamiCore.entities import DoesNotExist

logger = logging.getLogger(__name__)


def exception_handler(exc, context):
    """ Format error responses. """
    # Q: Why can't this stuff happen in exception_wrapper?
    # A: If we don't propagate exceptions out of exception_wrapper
    #    then transactions won't be handled correctly.
    #    We need to call default_exception_handler or else keep that
    #    logic up to date ourselves.

    try:
        # re-raise exc so that the logger can format the stack trace
        # correctly
        raise exc
    except:
        logger.exception("Exception caught in handler.")

    # make use of the OG exception handler which does transaction logic.
    response = default_exception_handler(exc, context)

    # # reformat messages
    # if response is not None:
    #     try:
    #         detail = response.data.pop("detail")
    #         response.data["message"] = str(detail)
    #         response.data["code"] = detail.code
    #     except KeyError:
    #         detail = response.data.pop('non_field_errors')
    #         response.data["message"] = str(detail[0])
    return response


def exception_wrapper(fn):
    """ Translate and log exceptions. """

    @wraps(fn)
    def wrapper(*args, **kwargs):
        try:
            return fn(*args, **kwargs)

        except ValidationError as err:
            # This means the FE didn't send the right kind of data.
            logger.exception("Exception caught in wrapper.")
            code = status.HTTP_400_BAD_REQUEST
            detail = err.detail

        except DjValidationError as err:
            # This means the user entered something invalid. Log?
            if settings.DEBUG:
                logger.exception("Exception caught in wrapper.")
            code = status.HTTP_400_BAD_REQUEST
            detail = str(err)

        except DoesNotExist as err:
            # This will happen often, don't log
            if settings.DEBUG:
                logger.exception("Exception caught in wrapper.")
            code = status.HTTP_404_NOT_FOUND
            detail = str(err)

        return Response({"detail": detail, "code": code}, status=code)

    return wrapper


def response_for_stripe_error(err):
    try:
        raise err

    except stripe.error.CardError as e:
        logger.exception("Caught a stripe error")
        code = "card_declined"
        stat = status.HTTP_400_BAD_REQUEST
        message = e.json_body.get("message")  # stripe docs say this is user facing
        detail = {}

    except stripe.error.RateLimitError as e:
        logger.exception("Caught a stripe error")
        code = "rate_limited"
        stat = status.HTTP_429_TOO_MANY_REQUESTS
        message = "The service is overloaded right now, please try again in 5 minutes."
        detail = {}

    except stripe.error.InvalidRequestError as e:
        logger.exception("Caught a stripe error")
        code = "internal_err_stripe"
        stat = status.HTTP_500_INTERNAL_SERVER_ERROR
        message = "Something went wrong on our end!"
        detail = {}

    except stripe.error.AuthenticationError as e:
        logger.exception("Caught a stripe error")
        code = "internal_err_stripe"
        stat = status.HTTP_500_INTERNAL_SERVER_ERROR
        message = "Something went wrong on our end!"
        detail = {}

    except stripe.error.APIConnectionError as e:
        logger.exception("Caught a stripe error")
        code = "internal_err_stripe"
        stat = status.HTTP_500_INTERNAL_SERVER_ERROR
        message = "Something went wrong on our end!"
        detail = {}

    except stripe.error.StripeError as e:
        logger.exception("Caught a stripe error")
        code = "internal_err_stripe"
        stat = status.HTTP_500_INTERNAL_SERVER_ERROR
        message = "Something went wrong on our end!"
        detail = {}

    return Response(
        {"code": code, "status": stat, "message": message, "detail": detail},
        status=stat,
    )
