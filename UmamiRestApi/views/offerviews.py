""" Views for manipulating offers. """
import dateutil.parser
import copy

from collections import defaultdict
from django.utils import timezone

from django.contrib.gis.geos import Point
from django.core.exceptions import ValidationError
from django.db import transaction
from django.utils.translation import ugettext_lazy as _

from rest_framework import serializers
from rest_framework import status
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework.views import APIView

from UmamiCore.models import OfferTime
from UmamiCore.entities import Dish, Kitchen, Offer, Single, Bundle
from UmamiSite.helper.OfferHelper import *
from UmamiCore.helpers import combine_date_and_time

from UmamiRestApi.serializers import (
    MultiOffersDisplaySerializer,
    OfferSerializer,
    OfferDisplaySerializer,
    OfferCreateSerializer,
    OfferPreviewSerializer,
    OfferTimeSerializer,
)
from ..errors import exception_wrapper


class OfferView(APIView):
    """ Manipulate a particular offer. """

    permission_classes = (AllowAny,)
    renderer_classes = (JSONRenderer,)
    _Entity = Offer

    @exception_wrapper
    def get(self, request, pk):
        """ Just render the requested offer. """
        try:
            offer = self._Entity.fetch_with_pk(pk=pk)
            user_lat = request.GET.get("lat")
            user_lon = request.GET.get("lon")
            user_address = request.GET.get("address")

            context = {"request": request, "user_address": user_address}

            if user_lat and user_lon:
                context["reference_point"] = Point(
                    float(user_lon), float(user_lat), srid=4326
                )
            offer_s = OfferDisplaySerializer(offer, context=context)

            return Response(offer_s.data, status=status.HTTP_200_OK)
        except self._Entity.DoesNotExist:
            return Response({}, status=status.HTTP_404_NOT_FOUND)

    @exception_wrapper
    def post(self, request, pk):
        """ Modify the given offer. """
        # for now the mutable api will be disabled
        return Response({}, status=status.HTTP_405_METHOD_NOT_ALLOWED)

    @exception_wrapper
    def delete(self, request, pk):
        """ Remove the given offer. """
        try:
            offer = self._Entity.fetch_with_pk(pk=pk)
            date = request.GET.get("d")
            multi_delete = request.GET.get("multiDelete", 'False')
            if offer.kitchen.chef != request.user:
                return Response(
                    _("User is not the chef."), status=status.HTTP_401_UNAUTHORIZED
                )
            # consume all remaining available
            self._Entity.drain_available(pk, date, multi_delete)
            return Response({}, status=status.HTTP_200_OK)
        except self._Entity.DoesNotExist:
            return Response({}, status=status.HTTP_404_NOT_FOUND)


class SingleView(OfferView):
    _Entity = Single


class BundleView(OfferView):
    _Entity = Bundle


class OffersNearView(APIView):
    """ Get offers near the given point. """

    permission_classes = (AllowAny,)
    renderer_classes = (JSONRenderer,)
    _Entity = Offer

    @exception_wrapper
    def get(self, request):
        """ Get offers near the given point. """
        count = 8  # return 8 singles per request
        page = int(request.GET.get("page", 1)) - 1
        user_lat = float(request.GET.get("lat"))
        user_lon = float(request.GET.get("lon"))
        user_address = request.GET.get("address")
        date_iso = request.GET.get("d")
        flag = request.GET.get("flag", "page")
        reference_point = Point(user_lon, user_lat, srid=4326)

        try:
            date = dateutil.parser.parse(date_iso).date()
        except (TypeError, ValueError):
            date = timezone.now().date()

        offers = self._Entity.fetch_near(
            reference_point, date, offset=page * count, limit=count, flag=flag
        )
        if len(offers) == 0:
            return Response(data=[], status=status.HTTP_200_OK)
        # render offers
        offers_s = MultiOffersDisplaySerializer(
            offers,
            many=True,
            context={
                "request": request,
                "reference_point": reference_point,
                "user_address": user_address,
                "date": date,
            },
        )

        return Response(data=offers_s.data, status=status.HTTP_200_OK)


class SinglesNearView(OffersNearView):
    _Entity = Single


class BundlesNearView(OffersNearView):
    _Entity = Bundle


class ActiveOffersView(APIView):
    """ Get all upcoming offers for a kitchen """

    permission_classes = (IsAuthenticated,)
    renderer_classes = (JSONRenderer,)
    _Entity = Offer

    @exception_wrapper
    def get(self, request):
        # Can these limits ever be reached?
        offset, limit = 0, 1000
        # Fetch and render data
        active_offers = self._Entity.fetch_upcoming_offers_mapped_to_date(
            chef=request.user
        )

        result = defaultdict(list)
        for date in active_offers.keys():
            result[date] = [
                OfferDisplaySerializer(offer, many=True, context={"request": request})
                for offer in active_offers[date]
            ]
        # done.
        return Response(data=result, status=status.HTTP_200_OK)


class ActiveSinglesView(ActiveOffersView):
    _Entity = Single


class ActiveBundlesView(ActiveOffersView):
    _Entity = Bundle


class PastOffersView(APIView):
    """ Get all upcoming offers for a kitchen """

    permission_classes = (IsAuthenticated,)
    renderer_classes = (JSONRenderer,)
    _Entity = Offer

    @exception_wrapper
    def get(self, request):
        # Can these limits ever be reached?
        offset, limit = 0, 1000
        # Fetch and render data
        past_offers = self._Entity.fetch_past(request.user, offset=offset, limit=limit)
        past_s = OfferPreviewSerializer(
            past_offers, many=True, context={"request": request}
        )
        # Group data by date
        data = defaultdict(list)
        for offer in past_s.data:
            data[offer["date"]].append(offer)
        # done.
        return Response(data=data, status=status.HTTP_200_OK)


class PastSinglesView(PastOffersView):
    _Entity = Single


class PastBundlesView(PastOffersView):
    _Entity = Bundle


class AllOffersView(APIView):
    """ Get all offers for a kitchen """

    permission_classes = (IsAuthenticated,)
    renderer_classes = (JSONRenderer,)
    _Entity = Offer

    @exception_wrapper
    def get(self, request):
        # Can these limits ever be reached?
        offset, limit = 0, 1000
        # Fetch and render data
        all_offers = self._Entity.fetch_for_kitchen(
            request.user.kitchen, offset=offset, limit=limit
        )
        all_s = OfferDisplaySerializer(
            all_offers, many=True, context={"request": request}
        )
        # Group data by date
        data = defaultdict(list)
        for offer in all_s.data:
            dates_for_offer = get_all_offering_dates(offer)
            for date in dates_for_offer:
                offer_copy = copy.deepcopy(offer)
                available_for_date = next(
                    (
                        offer_for_date["available"]
                        for offer_for_date in offer_copy["availables"]
                        if offer_for_date["date"] == date
                    ),
                    offer_copy["total_available"],
                )
                offer_copy["is_expired"] = (
                        combine_date_and_time(date, offer_copy["order_by"])
                        < timezone.now()
                )
                if available_for_date > -1:
                    offer_copy["available"] = available_for_date
                    data[date].append(offer_copy)
        # done.
        return Response(data=data, status=status.HTTP_200_OK)


class AllSinglesView(AllOffersView):
    _Entity = Single


class AllBundlesView(AllOffersView):
    _Entity = Bundle


class CreateOfferView(APIView):
    """ Post new offers. """

    permission_classes = (IsAuthenticated,)
    renderer_classes = (JSONRenderer,)
    _Entity = Offer

    @exception_wrapper
    def post(self, request):
        """ If a user has a valid kitchen then they can create new offers. """
        try:
            # Does the user have permission to create a dish?
            kitchen = Kitchen.fetch_with_chef(request.user)

            if not kitchen.has_verification:
                return Response(
                    {"message": "Kitchen is not validated."},
                    status=status.HTTP_401_UNAUTHORIZED,
                )
            dishes = request.data.pop("dishes").split(",")
            tz_offset = request.data.pop("tz_offset")

            context = {
                "request": request,
                "dishes": dishes,
                "kitchen": kitchen,
                "tz_offset": tz_offset,
            }
            if "pickup_time_id" in request.data:
                context["pickup_time"] = OfferTime.objects.get(
                    pk=request.data.pop("pickup_time_id")
                )
            if "delivery_time_id" in request.data:
                context["deliver_time"] = OfferTime.objects.get(
                    pk=request.data.pop("delivery_time_id")
                )

            offer = self._Entity(
                kitchen=kitchen,
                dishes=dishes,
                pickup_time=context.get("pickup_time", None),
                deliver_time=context.get("deliver_time", None),
            )

            offer_s = OfferCreateSerializer(offer, data=request.data, context=context)
            offer_s.is_valid(raise_exception=True)
            offer_s.save()

            # done.
            return Response(
                {
                    "id": offer_s.data["id"],
                    "social_url": offer_s.data["social_url"],
                    "message": "Success!",
                },
                status=status.HTTP_200_OK,
            )
        except Dish.DoesNotExist:
            return Response(
                {"message": _("No such dish")}, status=status.HTTP_400_BAD_REQUEST
            )
        except Kitchen.DoesNotExist:
            return Response(
                {"message": "No kitchen"}, status=status.HTTP_401_UNAUTHORIZED
            )

    @exception_wrapper
    def put(self, request):
        """ Update an existing offer. """
        try:
            offer = self._Entity.fetch_with_pk(request.data.get("offer"))
            tz_offset = request.data.pop("tz_offset")
            offer_s = OfferCreateSerializer(
                offer,
                data=request.data,
                context={"request": request, "tz_offset": tz_offset},
            )
            offer_s.is_valid(raise_exception=True)
            offer_s.update(instance=offer, validated_data=offer_s.validated_data)

            # done.
            return Response(
                {
                    "id": offer_s.data["id"],
                    "url": offer_s.data["url"],
                    "message": "Success!",
                },
                status=status.HTTP_200_OK,
            )
        except Dish.DoesNotExist:
            return Return(
                {"message": _("No such dish")}, status=status.HTTP_400_BAD_REQUEST
            )
        except Kitchen.DoesNotExist:
            return Response(
                {"message": "No kitchen"}, status=status.HTTP_401_UNAUTHORIZED
            )


class CreateSingleView(CreateOfferView):
    _Entity = Single


class CreateBundleView(CreateOfferView):
    _Entity = Bundle


class OfferByIdForEdit(APIView):
    """ Post new offers. """

    permission_classes = (IsAuthenticated,)
    renderer_classes = (JSONRenderer,)
    _Entity = Offer

    @exception_wrapper
    def get(self, request, pk):
        """ Fetch offer for edit. """
        try:
            offer = self._Entity.fetch_with_pk_and_kitchen(pk=pk, chef=request.user)
            offer_s = OfferDisplaySerializer(offer, context={"request": request})
            return Response(offer_s.data, status=status.HTTP_200_OK)
        except self._Entity.DoesNotExist:
            return Response({}, status=status.HTTP_404_NOT_FOUND)


def _insert_times_lots(offer, delivery_time_slots, pickup_time_slots):
    available_dates = get_all_offering_dates(offer)
    for date in available_dates:
        if offer["deliver_time"]:
            time_slot = OfferTimeSerializer(offer["deliver_time"]).data
            if time_slot not in delivery_time_slots[date]:
                delivery_time_slots[date].append(time_slot)
        if offer["pickup_time"]:
            time_slot = OfferTimeSerializer(offer["pickup_time"]).data
            if time_slot not in pickup_time_slots[date]:
                pickup_time_slots[date].append(time_slot)


class AvailableTimesForDate(APIView):
    permission_classes = (IsAuthenticated,)
    renderer_classes = (JSONRenderer,)
    _Entity = Offer

    @exception_wrapper
    def get(self, request):
        """ Fetch offer for edit. """
        try:
            upcoming_offers = self._Entity.fetch_upcoming_offers(chef=request.user)

            delivery_time_slots = defaultdict(list)
            pickup_time_slots = defaultdict(list)

            for offer in OfferSerializer(
                upcoming_offers["non_recurring_offers"],
                context={"request": request},
                many=True,
            ).data:
                if offer["deliver_time"]:
                    delivery_time_slots[offer["date"]].append(
                        OfferTimeSerializer(offer["deliver_time"]).data
                    )
                if offer["pickup_time"]:
                    pickup_time_slots[offer["date"]].append(
                        OfferTimeSerializer(offer["pickup_time"]).data
                    )

            for offer in OfferSerializer(
                upcoming_offers["weekly_offers"],
                context={"request": request},
                many=True,
            ).data:
                _insert_times_lots(offer, delivery_time_slots, pickup_time_slots)
            for offer in OfferSerializer(
                upcoming_offers["daily_offers"], context={"request": request}, many=True
            ).data:
                _insert_times_lots(offer, delivery_time_slots, pickup_time_slots)

            return Response(
                {
                    "deliver_times": delivery_time_slots,
                    "pickup_times": pickup_time_slots,
                },
                status=status.HTTP_200_OK,
            )
        except self._Entity.DoesNotExist:
            return Response({}, status=status.HTTP_404_NOT_FOUND)


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Bulk manipulation of Offers


class BulkManipulateTargetSerializer(serializers.Serializer):
    id = serializers.CharField()
    date = serializers.DateField()
    label = serializers.CharField(required=False)


class BulkManipulateOfferViewRequestSerializer(serializers.Serializer):
    move = BulkManipulateTargetSerializer(many=True)
    copy = BulkManipulateTargetSerializer(many=True)


class LabelMap(dict):
    def __setitem__(self, key, value):
        if key not in self:
            super().__setitem__(key, value)
        else:
            raise ValidationError(_("Duplicate label."))


class BulkManipulateOfferView(APIView):
    """ Copy and move offers using a single call. """

    permission_classes = (IsAuthenticated,)
    renderer_classes = (JSONRenderer,)

    @exception_wrapper
    def post(self, request):
        # Parse request
        commands_s = BulkManipulateOfferViewRequestSerializer(
            data=request.data, context={"request": request}
        )
        commands_s.is_valid(raise_exception=True)
        commands = commands_s.validated_data

        # Fetch and arrange required data.
        pks = set(
            command["id"]
            for command in commands.get("move", []) + commands.get("copy", [])
        )
        offers = Offer.fetch_with_chef_and_pks(request.user, pks)
        offers = {str(offer.pk): offer for offer in offers}

        label_to_pk = LabelMap()
        # Try to perform the requested actions
        with transaction.atomic():
            for command in commands.get("copy", []):
                offer = offers[command["id"]]
                o = offer.copy_to(command["date"])
                label = command.get("label")
                if label is not None:
                    label_to_pk[label] = str(o.pk)
            for command in commands.get("move", []):
                offer = offers[command["id"]]
                o = offer.move_to(command["date"])
                label = command.get("label")
                if label is not None:
                    label_to_pk[label] = str(o.pk)

        return Response(label_to_pk, status=status.HTTP_200_OK)
