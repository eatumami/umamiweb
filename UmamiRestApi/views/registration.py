from rest_auth.registration import views
from rest_framework import status
from rest_framework.response import Response
from rest_framework.serializers import ValidationError
from UmamiRestApi.errors import exception_wrapper


class RegisterView(views.RegisterView):
    @exception_wrapper
    def post(self, *args, **kwargs):
        try:
            return super().post(*args, **kwargs)
        except ValidationError as err:
            return Response(
                {
                    "code": "invalid",  # Placeholder ... what should this be?
                    "status": status.HTTP_400_BAD_REQUEST,
                    "message": "There are issues with your profile!",
                    "detail": err.detail,
                },
                status=status.HTTP_400_BAD_REQUEST,
            )
