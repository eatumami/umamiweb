from rest_framework import status
from rest_framework.decorators import api_view, renderer_classes, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework.views import APIView

from UmamiCore.models import KitchenFollower
from UmamiRestApi.serializers import *
from ..errors import exception_wrapper


class UserView(APIView):

    permission_classes = (IsAuthenticated,)
    renderer_classes = (JSONRenderer,)

    @exception_wrapper
    def post(self, request):
        serializer = UserEditSerializer(request.user, data=request.data, partial=True)
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return Response(data=serializer.data, status=status.HTTP_200_OK)
        return Response({}, status=status.HTTP_400_BAD_REQUEST)

    @exception_wrapper
    def get(self, request):
        user_s = UserSerializer(request.user, partial=True)
        return Response(data=user_s.data, status=status.HTTP_200_OK)


@api_view(["POST"])
@permission_classes((IsAuthenticated,))
@renderer_classes((JSONRenderer,))
@exception_wrapper
def user_follows(request):
    try:
        user = request.user
        kitchen = Kitchen.objects.get(pk=request.data["id"])
        follow, created = KitchenFollower.objects.get_or_create(
            kitchen=kitchen, user=user
        )
        if not created:
            # duplicated follow action, should log a warning?
            pass
        return Response({"success": "true"}, status=status.HTTP_200_OK)
    except Kitchen.DoesNotExist:
        return Response(
            {"success": "false", "message": "No kitchen with the given ID."},
            status=status.HTTP_400_BAD_REQUEST,
        )


@api_view(["POST"])
@permission_classes((IsAuthenticated,))
@renderer_classes((JSONRenderer,))
@exception_wrapper
def user_unfollows(request):
    user = request.user
    kpk = request.data.get("id")
    n, _ = KitchenFollower.objects.filter(kitchen=kpk, user=user).delete()
    if n > 0:
        return Response({"success": "true"}, status=status.HTTP_200_OK)
    else:
        return Response({"success": "false"}, status=status.HTTP_400_BAD_REQUEST)
