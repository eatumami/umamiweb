from django.utils.translation import ugettext_lazy as _

from collections import defaultdict
from rest_framework import status
from rest_framework.decorators import api_view, renderer_classes, permission_classes
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response

from UmamiWeb import settings
from UmamiCore.models import Customer
from UmamiCore.entities import OrderReceipt, OrderError
from UmamiRestApi.serializers import (
    OrderRequestSerializer,
    OrderReceiptSerializer,
    IndividualOrderReceiptSerializer,
)
from UmamiRestApi.helpers import (
    send_order_confirmation_email,
    send_refund_confirmation_email,
    send_cancel_confirmation_email,
    send_order_confirmation_sms,
    send_refund_confirmation_sms,
    send_cancel_confirmation_sms,
)
from UmamiRestApi.errors import exception_wrapper, response_for_stripe_error

import UmamiCore.business as business

import stripe

stripe.api_key = settings.STRIPE_SECRET_KEY


@api_view(["POST"])
@permission_classes((IsAuthenticated,))
@renderer_classes((JSONRenderer,))
@exception_wrapper
def create_order(request):
    try:
        context = dict(request=request)

        # Process api request
        order_s = OrderRequestSerializer(data=request.data, context=context)
        order_s.is_valid(raise_exception=True)
        order_request = order_s.validated_data

        # Set up and validate the order object.
        order = OrderReceipt.build(order_request, request.user)
        receipt_s = OrderReceiptSerializer(order, context=context)

        # Perform the transaction, if there is a stripe token. Else this is a dry run.
        tok = order_request.get("stripe_token", None)
        if tok is not None:
            order = business.process_transaction(order, tok, request.user)
            receipt_s = IndividualOrderReceiptSerializer(
                order, context=context, many=True
            )
            send_order_confirmation_sms(request.user, receipt_s)
            send_order_confirmation_email(request.user, receipt_s)

        return Response(receipt_s.data, status=status.HTTP_200_OK)

    except OrderError as err:
        return Response(
            {
                "message": _("There are errors with the requested order!"),
                "code": "invalid",
                "status": status.HTTP_400_BAD_REQUEST,
                "detail": err.detail,
            },
            status=status.HTTP_400_BAD_REQUEST,
        )

    except stripe.error.StripeError as err:
        return response_for_stripe_error(err)


@api_view(["POST"])
@permission_classes((IsAuthenticated,))
@renderer_classes((JSONRenderer,))
@exception_wrapper
def process_kitchen_refund(request):
    try:
        refund_amount = request.data.get("amount", None)
        order = OrderReceipt.fetch_model_with_pk(request.data["order_id"])
        if refund_amount and refund_amount > float(round(order.total.amount, 2)):
            return Response(
                {"message": _("Cannot refund more than order total")},
                status=status.HTTP_400_BAD_REQUEST,
            )
        if not request.user.is_chef:
            return Response(
                {"message": _("Not a chef")}, status=status.HTTP_400_BAD_REQUEST
            )
        if order.seller.chef != request.user:
            return Response(
                {
                    "message": _(
                        "You are not authorized to issue refund for this kitchen"
                    )
                },
                status=status.HTTP_400_BAD_REQUEST,
            )
        refund = business.process_refund(order, refund_amount)
        send_refund_confirmation_sms(request.user, order)
        send_refund_confirmation_email(request.user, order, order.pk.hashid)
        return Response(refund, status=status.HTTP_200_OK)
    except KeyError:
        return Response(
            {"message": _("Order does not exist")}, status=status.HTTP_404_NOT_FOUND
        )
    except stripe.error.StripeError as err:
        return response_for_stripe_error(err)


@api_view(["POST"])
@permission_classes((IsAuthenticated,))
@renderer_classes((JSONRenderer,))
@exception_wrapper
def process_order_cancellation(request):
    try:
        order_entity = OrderReceipt.fetch_with_pk(request.data["order_id"])
        order_model = OrderReceipt.fetch_model_with_pk(request.data["order_id"])
        refund_amount = order_entity.total
        if not request.user.is_chef:
            return Response(
                {"message": _("Not a chef")}, status=status.HTTP_400_BAD_REQUEST
            )
        if order_entity.seller.chef != request.user:
            return Response(
                {
                    "message": _(
                        "You are not authorized to cancel for this kitchen"
                    )
                },
                status=status.HTTP_400_BAD_REQUEST,
            )
        refund = business.process_refund(order_model, refund_amount)
        order_entity.restore_quantities()

        send_cancel_confirmation_sms(request.user, order_model)
        send_cancel_confirmation_email(request.user, order_model, order_model.pk.hashid)
        return Response(refund, status=status.HTTP_200_OK)
    except KeyError:
        return Response(
            {"message": _("Order does not exist")}, status=status.HTTP_404_NOT_FOUND
        )
    except stripe.error.StripeError as err:
        return response_for_stripe_error(err)


@api_view(["GET"])
@permission_classes((IsAuthenticated,))
@renderer_classes((JSONRenderer,))
@exception_wrapper
def get_all_cards(request):
    try:
        # TODO Move this into business.py so that stripe stuff isn't scattered all over
        cards = stripe.Customer.retrieve(
            request.user.customer.stripe_customer_token
        ).sources.list(limit=3, object="card")
        for i in range(0, len(cards.data)):
            cards.data[i][
                "logo"
            ] = f"{settings.STATIC_URL}assets/credit-card-logos/{cards.data[i]['brand'].lower()}.png"
        return Response(cards.data, status=status.HTTP_200_OK)
    except Customer.DoesNotExist:
        return Response([], status=status.HTTP_200_OK)

    except stripe.error.StripeError as err:
        return response_for_stripe_error(err)


@api_view(["GET"])
@permission_classes((IsAuthenticated,))
@renderer_classes((JSONRenderer,))
@exception_wrapper
def get_sold_orders(request):
    orders = OrderReceipt.fetch_sold(request.user)
    receipt_s = IndividualOrderReceiptSerializer(
        orders, many=True, context={"request": request}
    )
    return Response(receipt_s.data, status=status.HTTP_200_OK)


@api_view(["GET"])
@permission_classes((IsAuthenticated,))
@renderer_classes((JSONRenderer,))
@exception_wrapper
def get_purchased_orders(request):
    orders = OrderReceipt.fetch_purchased(request.user)
    receipt_s = IndividualOrderReceiptSerializer(
        orders, many=True, context={"request": request}
    )
    return Response(receipt_s.data, status=status.HTTP_200_OK)


@api_view(["GET"])
@permission_classes((IsAuthenticated,))
@renderer_classes((JSONRenderer,))
@exception_wrapper
def get_orders(request, pks):
    order = OrderReceipt.fetch_with_pks(pks)
    receipt_s = IndividualOrderReceiptSerializer(
        order, context={"request": request}, many=True
    )
    return Response(receipt_s.data, status=status.HTTP_200_OK)


@api_view(["GET"])
@permission_classes((IsAuthenticated,))
@renderer_classes((JSONRenderer,))
@exception_wrapper
def get_upcoming_orders(request):
    try:
        orders = OrderReceipt.fetch_upcoming(request.user)
        orders_s = IndividualOrderReceiptSerializer(
            orders, context={"request": request}, many=True
        )
        return Response(orders_s.data, status=status.HTTP_200_OK)
    except OrderReceipt.DoesNotExist:
        return Response({}, status=status.HTTP_404_NOT_FOUND)
