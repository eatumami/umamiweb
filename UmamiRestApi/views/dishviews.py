""" Views for manipulating dishes. """

from django.core.cache import cache
from rest_framework import status
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework.views import APIView
from UmamiCore.models import *
from UmamiRestApi.serializers import *
from ..errors import exception_wrapper

import UmamiCore.entities as entities


class DishView(APIView):
    """ View to create new dishes. """

    permission_classes = (IsAuthenticated,)
    renderer_classes = (JSONRenderer,)

    @exception_wrapper
    def post(self, request):
        """ Create a new dish. """
        try:
            context = {
                "request": request,
                "kitchen": Kitchen.objects.get(chef=request.user),
            }
            dish_s = DishSerializer(data=request.data, context=context)
            dish_s.is_valid(raise_exception=True)
            dish_s.save()
            return Response(
                {
                    "id": dish_s.data["id"],
                    "url": dish_s.data["url"],
                    "version": dish_s.data["version"],
                    "message": "Success!",
                },
                status=status.HTTP_200_OK,
            )
        except Kitchen.DoesNotExist:
            return Response(
                {"message": "No kitchen."}, status=status.HTTP_401_UNAUTHORIZED
            )

    @exception_wrapper
    def put(self, request):
        """ Update an existing dish. """
        try:
            context = {
                "request": request,
                "kitchen": Kitchen.objects.get(chef=request.user),
            }
            existing_dish = entities.Dish.fetch_with_pk(pk=request.data.get("id"))

            dish_s = DishSerializer(existing_dish, data=request.data, context=context)
            dish_s.is_valid(raise_exception=True)
            updated_dish = dish_s.update(
                dish=existing_dish, validated_data=dish_s.validated_data
            )
            updated_dish_s = DishSerializer(updated_dish, context=context)

            return Response(
                {
                    "id": updated_dish_s.data["id"],
                    "url": updated_dish_s.data["url"],
                    "version": updated_dish_s.data["version"],
                    "message": "Success!",
                },
                status=status.HTTP_200_OK,
            )
        except Kitchen.DoesNotExist:
            return Response(
                {"message": "No kitchen."}, status=status.HTTP_401_UNAUTHORIZED
            )


class DishesByKitchen(APIView):
    permission_classes = (IsAuthenticated,)
    renderer_classes = (JSONRenderer,)

    @exception_wrapper
    def get(self, request):
        """ Get the requested Dish. """
        try:
            dishes = entities.Dish.fetch_with_chef(request.user)
            dishes_s = DishSerializer(dishes, context={"request": request}, many=True)
            return Response(dishes_s.data, status=status.HTTP_200_OK)
        except Dish.DoesNotExist:
            return Response(
                {"message": "No dishes with those IDs."},
                status=status.HTTP_404_NOT_FOUND,
            )


class DishesByIdsForOfferView(APIView):
    permission_classes = (AllowAny,)
    renderer_classes = (JSONRenderer,)

    @exception_wrapper
    def get(self, request, pks):
        """ Get the requested Dish. """
        try:
            # Preserve the order of the list after converting to a Set
            ordered_pk_set = list(dict.fromkeys(pks))

            dishes = entities.Dish.fetch_with_chef_and_pks(request.user, ordered_pk_set)
            dishes_s = DishSerializer(dishes, context={"request": request}, many=True)
            return Response(dishes_s.data, status=status.HTTP_200_OK)
        except Dish.DoesNotExist:
            return Response(
                {"message": "No dishes with those IDs."},
                status=status.HTTP_404_NOT_FOUND,
            )


class DishByIdView(APIView):
    """ Individual dish object manipulation. """

    permission_classes = (AllowAny,)
    renderer_classes = (JSONRenderer,)

    @exception_wrapper
    def get(self, request, pk):
        """ Get the requested Dish. """
        try:
            dish = entities.Dish.fetch_with_pk(pk)
            dish_s = DishSerializer(dish, context={"request": request})
            return Response(dish_s.data, status=status.HTTP_200_OK)
        except DishIdentity.DoesNotExist:
            return Response(
                {"message": "No dish with that ID."}, status=status.HTTP_404_NOT_FOUND
            )

    @exception_wrapper
    def post(self, request, pk):
        """ Modify the requested Dish. """
        try:
            dish = entities.Dish.fetch_with_pk(pk)
            if dish.kitchen.chef != request.user:
                return Response({}, status=status.HTTP_401_UNAUTHORIZED)
            dish_s = DishSerializer(
                dish, data=request.data, context={"request": request}
            )
            dish_s.is_valid(raise_exception=True)
            dish_s.save()
            return Response({"message": "Success!"}, status=status.HTTP_200_OK)
        except DishIdentity.DoesNotExist:
            return Response(
                {"message": "No dish with that ID."}, status=status.HTTP_404_NOT_FOUND
            )


class DishTagsView(APIView):
    permission_classes = (AllowAny,)
    renderer_classes = (JSONRenderer,)

    # 3 minute timeout before fetching new tags
    _CACHE_TIMEOUT = 60 * 3
    _CACHE_KEY = "top_250_tags"

    @exception_wrapper
    def get(self, request):
        """ Get the top 250 tags. """
        if cache.has_key(self._CACHE_KEY):
            tags = cache.get(self._CACHE_KEY)
        else:
            tags = entities.Dish.fetch_top_tags()
            cache.set(self._CACHE_KEY, tags, self._CACHE_TIMEOUT)
        return Response(tags, status=status.HTTP_200_OK)


class DishIngredientsView(APIView):
    permission_classes = (AllowAny,)
    renderer_classes = (JSONRenderer,)

    # 3 minute timeout before fetching new ingredients
    _CACHE_TIMEOUT = 60 * 3
    _CACHE_KEY = "top_250_ingredients"

    @exception_wrapper
    def get(self, request):
        """ Get the top 250 ingredients. """
        if cache.has_key(self._CACHE_KEY):
            ingredients = cache.get(self._CACHE_KEY)
        else:
            ingredients = entities.Dish.fetch_top_ingredients()
            cache.set(self._CACHE_KEY, ingredients, self._CACHE_TIMEOUT)
        return Response(ingredients, status=status.HTTP_200_OK)


class DishExtrasView(APIView):
    permission_classes = (AllowAny,)
    renderer_classes = (JSONRenderer,)

    # 3 minute timeout before fetching new Extras
    _CACHE_TIMEOUT = 60 * 3
    _CACHE_KEY = "top_250_extras"

    @exception_wrapper
    def get(self, request):
        """ Get the top 250 ingredients. """
        if cache.has_key(self._CACHE_KEY):
            ingredients = cache.get(self._CACHE_KEY)
        else:
            ingredients = entities.Dish.fetch_top_ingredients()
            cache.set(self._CACHE_KEY, ingredients, self._CACHE_TIMEOUT)
        return Response(ingredients, status=status.HTTP_200_OK)


class DishReviewsView(APIView):
    """ List view of reviews for the given dish. """

    permission_classes = (AllowAny,)
    renderer_classes = (JSONRenderer,)

    @exception_wrapper
    def get(self, request, pk):
        page = int(request.GET.get("page", 1)) - 1
        limit = int(request.GET.get("limit", 8))
        reviews = entities.Review.fetch_for_dish(pk, offset=page * limit, limit=limit)
        reviews_s = DishReviewSerializer(
            reviews, many=True, context={"requset": request}
        )
        return Response(reviews_s.data, status=status.HTTP_200_OK)


class DishesReviewsView(APIView):
    """ List view of reviews for the given dish. """

    permission_classes = (AllowAny,)
    renderer_classes = (JSONRenderer,)

    @exception_wrapper
    def get(self, request, pks):
        page = int(request.GET.get("page", 1)) - 1
        limit = int(request.GET.get("limit", 8))
        reviews = entities.Review.fetch_for_dishes(
            pks, offset=page * limit, limit=limit
        )
        reviews_s = DishReviewSerializer(
            reviews, many=True, context={"requset": request}
        )
        return Response(reviews_s.data, status=status.HTTP_200_OK)


class DishReviewView(APIView):
    """ Read and write a particular user's review for a particular dish. """

    permission_classes = (IsAuthenticated,)
    renderer_classes = (JSONRenderer,)

    @exception_wrapper
    def get(self, request, pk):
        try:
            review = entities.Review.fetch_for_dish_and_user(pk, request.user)
            review_s = DishReviewSerializer(review, context={"requset": request})
            return Response(review_s.data, status=status.HTTP_200_OK)
        except entities.Review.DoesNotExist:
            return Response({}, status=status.HTTP_404_NOT_FOUND)

    @exception_wrapper
    def post(self, request, pk):
        try:
            review = entities.Review.fetch_for_dish_and_user(pk, request.user)
            review_s = DishReviewSerializer(
                review, data=request.data, context={"requset": request}
            )
            review_s.is_valid(raise_exception=True)
            review_s.save()
            return Response(review_s.data, status=status.HTTP_200_OK)
        except entities.Review.DoesNotExist:
            return Response({}, status=status.HTTP_403_FORBIDDEN)
