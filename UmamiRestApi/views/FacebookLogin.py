from allauth.socialaccount.providers.facebook.views import FacebookOAuth2Adapter
from rest_auth.views import LoginView
from rest_auth.registration.serializers import SocialLoginSerializer

from rest_framework.response import Response
from rest_framework import status
from ..errors import exception_wrapper


class FacebookLogin(LoginView):
    """ View used to validate auth_tokens that come from facebook.
        Code found at:
        https://django-rest-auth.readthedocs.io/en/latest/installation.html#facebook
        """

    adapter_class = FacebookOAuth2Adapter
    serializer = None
    user = None

    @exception_wrapper
    def post(self, request, *args, **kwargs):
        self.serializer = SocialLoginSerializer(
            data=request.data, context={"request": request, "view": self}
        )
        self.serializer.is_valid(raise_exception=True)
        self.user = self.serializer.validated_data["user"]
        if self.user.is_email_confirmed:
            self.login()
            return self.get_response()
        else:
            return Response(
                {"error": "need_email_confirmation"}, status=status.HTTP_400_BAD_REQUEST
            )
