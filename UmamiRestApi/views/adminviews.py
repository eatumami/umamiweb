from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.permissions import AllowAny
from rest_framework.renderers import JSONRenderer
from django.core.mail import EmailMessage
from UmamiWeb import settings

from ..errors import exception_wrapper


class HealthCheckView(APIView):
    """
    Checks to see if the site is healthy.
    """

    permission_classes = (AllowAny,)
    renderer_classes = (JSONRenderer,)

    @exception_wrapper
    def get(self, request):
        return Response(status=status.HTTP_200_OK)


class ContactUsView(APIView):
    """
    Checks to see if the site is healthy.
    """

    permission_classes = (AllowAny,)
    renderer_classes = (JSONRenderer,)

    @exception_wrapper
    def post(self, request):
        subject_line = (
            f"{request.data.get('reason', '')}: {request.data.get('name', '')}"
        )

        to_email = add_email_label(
            settings.DEFAULT_FROM_EMAIL, request.data.get("reason", "")
        )

        user_email = EmailMessage(
            subject=subject_line,
            body=request.data.get("message", ""),
            from_email=settings.DEFAULT_FROM_EMAIL,
            to=[to_email],
            bcc=settings.UMAMI_SUPPORT_MAILING_LIST,
            reply_to=[request.data.get("email", "")],
        )
        user_email.send(fail_silently=True)
        return Response(status=status.HTTP_200_OK)


def add_email_label(email_address, label):
    email_format = email_address.split("@")
    email_format.insert(1, "+" + label)
    email_format.insert(2, "@")
    return "".join(email_format)
