""" Views provied by the Rest API. """

from .dishviews import *
from .FacebookConnect import *
from .FacebookLogin import *
from .kitchenviews import *
from .offerviews import *
from .orderviews import *
from .registration import *
from .UserView import *
from .adminviews import *
