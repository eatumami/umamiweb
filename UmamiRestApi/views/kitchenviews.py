""" Views related to kitchens. """

from rest_framework import status
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.response import Response
from rest_framework.views import APIView

from UmamiRestApi.helpers import send_email_to_admins_for_kitchen_verification

from django.contrib.gis.geos import Point
from UmamiCore.models import FeaturedKitchen
from UmamiCore.entities import Kitchen, KitchenVerification
from UmamiRestApi.serializers import (
    KitchenDetailsSerializer,
    KitchenEditSerializer,
    KitchenVerificationSerializer,
    KitchenPreviewSerializer,
    MultiOffersDisplaySerializer,
    OfferDisplaySerializer,
)
from ..errors import exception_wrapper


class KitchenView(APIView):
    """ Actions that a user can take on their own kitchen. """

    permission_classes = (IsAuthenticated,)
    renderer_classes = (JSONRenderer,)

    @exception_wrapper
    def get(self, request):
        """ View the profile of the kitchen for the user making the request. """
        kitchen = Kitchen.fetch_with_chef(request.user)
        kitchen_s = KitchenEditSerializer(kitchen, context={"request": request})
        return Response(kitchen_s.data, status=status.HTTP_200_OK)

    @exception_wrapper
    def post(self, request):
        """ The user making the request wants a kitchen. """
        try:
            kitchen = Kitchen.fetch_with_chef(request.user)
            created = False
        except Kitchen.DoesNotExist:
            created = True
            kitchen = Kitchen(_chef=request.user)

        kitchen_s = KitchenEditSerializer(
            kitchen, data=request.data, context={"request": request}, partial=True
        )
        kitchen_s.is_valid(raise_exception=True)
        kitchen_s.save()
        return Response({"created": created}, status=status.HTTP_200_OK)


class KitchenDetailView(APIView):
    """ Actions that a user can take on their own kitchen. """

    permission_classes = (IsAuthenticated,)
    renderer_classes = (JSONRenderer,)

    @exception_wrapper
    def get(self, request):
        """ View the profile of the kitchen for the user making the request. """
        kitchen = Kitchen.fetch_with_chef(request.user)
        kitchen_s = KitchenDetailsSerializer(kitchen, context={"request": request})
        return Response(kitchen_s.data, status=status.HTTP_200_OK)


class KitchenVerify(APIView):
    permission_classes = (IsAuthenticated,)
    parser_classes = (MultiPartParser, FormParser)

    @exception_wrapper
    def post(self, request):
        """ The user giving kitchen verification info. """

        kitchen = Kitchen.fetch_with_chef(request.user)
        send_email = True

        if request.FILES.get("photo_id") and not kitchen.has_identification:
            kitchen.photo_id = request.FILES.get("photo_id")
            kitchen.save()
            send_email = True

        has_permit = (
            request.data.get("has_permit") is not None
            and request.data.get("has_permit") == "true"
        )
        if has_permit:
            # TODO: seperate pickup address from verification address
            data = {
                "address": kitchen.pickup_address,
                "permit_number": request.data.get("permit_number", ""),
                "permit_county": request.data.get("permit_county", ""),
                "date_expires": request.data.get("date_expires")
                if request.data.get("date_expires") != ""
                else None,
            }

            kitchen_verification = KitchenVerification(
                kitchen=request.user.kitchen, data=data
            )
            kitchen_verification_s = KitchenVerificationSerializer(
                kitchen_verification, data=request.data, context={"request": request}
            )
            kitchen_verification_s.is_valid(raise_exception=True)
            kitchen_verification_s.save()
            send_email = True

        if send_email:
            send_email_to_admins_for_kitchen_verification(
                request.user, kitchen.photo_id.url, has_permit
            )

        return Response({"has_permit": has_permit}, status=status.HTTP_200_OK)


class KitchenByIdView(APIView):
    """ Actions that a user can take on a kitchen with the given name. """

    permission_classes = (AllowAny,)
    renderer_classes = (JSONRenderer,)

    @exception_wrapper
    def get(self, request, pk):
        """ View the profile of the kitchen with the given name. """
        kitchen = Kitchen.fetch_with_pk(pk)
        user_address = request.GET.get("address")
        user_lat = request.GET.get("lat")
        user_lon = request.GET.get("lon")
        context = {"request": request, "user_address": user_address}

        if user_lon and user_lat:
            context["reference_point"] = Point(
                float(user_lon), float(user_lat), srid=4326
            )

        kitchen_s = KitchenDetailsSerializer(kitchen, context=context)
        result = kitchen_s.data

        current_offers_calendar = kitchen.current_offers_calendar
        current_offers_list = []
        for date in current_offers_calendar.keys():
            offer_s = MultiOffersDisplaySerializer(
                current_offers_calendar[date],
                many=True,
                context={"request": request, "date": date},
            )
            current_offers_list.append({"date": date, "offers": offer_s.data})

        result["daily_offers"] = OfferDisplaySerializer(
            kitchen.current_offers["daily_offers"],
            many=True,
            context={"request": request},
        ).data
        result["weekly_offers"] = OfferDisplaySerializer(
            kitchen.current_offers["weekly_offers"],
            many=True,
            context={"request": request},
        ).data
        result["non_recurring_offers"] = OfferDisplaySerializer(
            kitchen.current_offers["non_recurring_offers"],
            many=True,
            context={"request": request},
        ).data
        result["current_offers"] = current_offers_list

        # TODO: implement past offers, currently empty
        result["past_offers"] = kitchen.past_offers

        return Response(result, status=status.HTTP_200_OK)


class FeaturedKitchenView(APIView):
    """ Actions that a user can take on their own kitchen. """

    permission_classes = (AllowAny,)
    renderer_classes = (JSONRenderer,)

    @exception_wrapper
    def get(self, request):
        """ View the profile of the kitchen for the user making the request. """
        kitchens = [featured.kitchen for featured in FeaturedKitchen.objects.all()]
        kitchens_s = KitchenPreviewSerializer(
            kitchens, context={"request": request}, many=True
        )
        return Response(kitchens_s.data, status=status.HTTP_200_OK)
