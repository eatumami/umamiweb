from django.conf.urls import include, url
from django.urls import path
from django.conf import settings
from rest_framework.urlpatterns import format_suffix_patterns, register_converter

# from rest_auth.registration.views import RegisterView
from UmamiRestApi.views import *

import UmamiCore.converters as converters

converters.register_converters()

# - - - - -
# Here are the URLs

urlpatterns = [
    # auth stuff
    url("api/auth/", include("rest_auth.urls")),
    path("api/auth/facebook/connect/", FacebookConnect.as_view(), name="fb_connect"),
    path("api/auth/facebook/", FacebookLogin.as_view(), name="fb_login"),
    path("api/registration", RegisterView.as_view(), name="rest_register"),
    # dish api
    path("api/dish", DishView.as_view()),
    path("api/dish/<hashid:pk>", DishByIdView.as_view(), name="dish_by_id"),
    path("api/my/dishes", DishesByKitchen.as_view(), name="dishes_by_kitchen"),
    # reviews
    path(
        "api/dish/reviews/<hashid:pk>",
        DishReviewsView.as_view(),
        name="reviews_for_dish",
    ),
    path(
        "api/dish/reviews/<hashid_list:pks>",
        DishesReviewsView.as_view(),
        name="reviews_for_dishes",
    ),
    path("api/review/<hashid:pk>", DishReviewView.as_view(), name="review_dish"),
    # offer dishes api
    path("api/offer", CreateOfferView.as_view()),
    path("api/offer/<hashid:pk>", OfferView.as_view(), name="offer_by_id"),
    path("api/offer/times/", AvailableTimesForDate.as_view()),
    path("api/offer/use/<hashid:pk>", OfferByIdForEdit.as_view(), name="dishes_by_ids"),
    path("api/offers", OffersNearView.as_view()),
    path("api/single", CreateSingleView.as_view()),
    path("api/single/<hashid:pk>", SingleView.as_view(), name="single_by_id"),
    path("api/singles", SinglesNearView.as_view()),
    path("api/bundle", CreateBundleView.as_view()),
    path("api/bundle/<hashid:pk>", BundleView.as_view(), name="bundle_by_id"),
    path("api/bundles", BundlesNearView.as_view()),
    path("api/my/offers/active", ActiveOffersView.as_view()),
    path("api/my/offers/past", PastOffersView.as_view()),
    path("api/my/offers/all", AllOffersView.as_view()),
    path("api/my/singles", ActiveSinglesView.as_view()),
    path("api/my/bundles", ActiveBundlesView.as_view()),
    path(
        "api/dish/use/<hashid_list:pks>",
        DishesByIdsForOfferView.as_view(),
        name="dishes_by_ids",
    ),
    path("api/bulk_manipulate", BulkManipulateOfferView.as_view()),
    # kitchen api
    path("api/kitchen", KitchenView.as_view()),
    path("api/kitchen/<hashid:pk>", KitchenByIdView.as_view(), name="kitchen_by_id"),
    path("api/kitchen/details/", KitchenDetailView.as_view()),
    path("api/kitchen_verify", KitchenVerify.as_view()),
    path("api/featured-kitchens", FeaturedKitchenView.as_view()),
    # order api
    path("api/order", create_order),
    path("api/order/<hashid_list:pks>", get_orders),
    path("api/sold", get_sold_orders),
    path("api/purchased", get_purchased_orders),
    path("api/user/cards", get_all_cards),
    path("api/upcoming", get_upcoming_orders),
    path("api/refund", process_kitchen_refund),
    path("api/cancel", process_order_cancellation),
    # other stuff...
    path("api/user", UserView.as_view()),
    path("api/tags", DishTagsView.as_view()),
    # url(r"^api/tags/(?P<query>[a-zA-Z]+)$", search_tags),
    path("api/ingredients", DishIngredientsView.as_view()),
    # url(r"^api/ingredients/(?P<query>[a-zA-Z]+)$", search_ingredients),
    # url(r"^api/extras$", get_all_extras),
    path("api/follow", user_follows),
    path("api/unfollow", user_unfollows),
    # Server Health Check:
    path("health_check/", HealthCheckView.as_view()),
    path("api/contact", ContactUsView.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns, allowed=["json", "html"])
