""" Tests for registration stuff. """

import json
from .test_base import *
from rest_framework.test import APIClient
from UmamiCore.models import User


class RegistrationTests(TestCase):
    """ Tests for registration stuff. """

    def setUp(self):
        self.request = {
            "email": "someguy@whatever.com",
            "first_name": "Some",
            "last_name": "Guy",
            "phone": "4165706881",
            "password1": "hellopa55word",
            "password2": "hellopa55word",
            "chef_signup": False,
        }

    def post_request(self):
        client = self.make_anon_client()
        return client.post(
            "/api/registration",
            data=json.dumps(self.request),
            content_type="application/json",
        )

    def test_register_valid(self):
        response = self.post_request()
        self.assertEqual(response.status_code, 201)
        user = User.objects.get(email=self.request["email"])
        self.assertEqual(user.first_name, self.request["first_name"])
        self.assertEqual(user.last_name, self.request["last_name"])
        self.assertEqual(user.phone, self.request["phone"])
        self.assertFalse(user.is_email_confirmed)

    def test_register_duplicate_email(self):
        # Use request to create user, expect to succeed
        response = self.post_request()
        self.assertEqual(response.status_code, 201)
        # Send data again, expecting to error out due to email
        response = self.post_request()
        self.assertEqual(response.status_code, 400)
        self.assertTrue("detail" in response.data)
        self.assertTrue("email" in response.data.get("detail", {}))

    def test_register_password_missmatch(self):
        # Missmatch in passwords
        self.request["password2"] = self.request["password1"] + "blah"
        response = self.post_request()
        self.assertEqual(response.status_code, 400)
        self.assertTrue("detail" in response.data)
        self.assertTrue("non_field_errors" in response.data.get("detail", {}))

    # TODO etc...
