""" Tests for registration stuff. """

import json
import datetime
from .test_base import *
from rest_framework.test import APIClient

import UmamiCore.models as models
import UmamiCore.entities as entities


class OrderTests(TestCase):
    """ Tests for order stuff. """

    def setUp(self):

        self.chef = models.User.objects.create_user(
            "chefguy@gmail.com", "Chef", "Guy", "kmalouieionvlmn", "4165706991"
        )

        self.chef_address = entities.Address(
            "2940 Acampo Rd", "", "Acampo", "CA", "USA", "95220", -121.280709, 38.174501
        )
        self.chef_address = self.chef_address.persist()

        self.kitchen = entities.Kitchen(_chef=self.chef)
        self.kitchen.stripe_account_token = "acct_1Dt0ysBYM51iDjQV"
        self.kitchen.pickup_address = self.chef_address
        self.kitchen.save()

        self.dish = entities.Dish(kitchen=self.kitchen._model)
        self.dish.name = "Tasty Food"
        self.dish.description = "It is tasty."
        self.dish.price = 10
        self.dish.save()

        self.offer = entities.Offer(kitchen=self.kitchen, dishes=[self.dish.pk])
        self.offer.is_recurring = False
        self.offer.total_available = 5
        self.offer.can_deliver = True
        self.offer.deliver_time = {"start": "14:00:00", "end": "15:00:00"}
        self.offer.can_pickup = True
        self.offer.pickup_start = {"start": "16:00:00", "end": "17:00:00"}
        self.offer.date = datetime.date.today().isoformat()
        self.offer.lead_time = datetime.timedelta(hours=2)
        self.offer.order_by = datetime.datetime.now() + self.offer.lead_time
        self.offer.save()

        self.customer_email = "customer@gmail.com"
        self.customer_password = "asldkfjsdlkfj"
        self.customer = models.User.objects.create_user(
            self.customer_email, "Custy", "Mer", self.customer_password, "4165706882"
        )
        self.customer.verify_email()

        self.request = {
            "stripe_token": "tok_visa",
            "notes": "hi this is me.",
            "delivery_address": {
                "line1": "2940 Acampo Rd",
                "line2": "",
                "city": "Acampo",
                "prov": "CA",
                "country": "USA",
                "postal_code": "95220",
                "lon": -121.280709,
                "lat": 38.174501,
            },
            "items": [
                {
                    "uuid": "wtfisthis?",
                    "kitchen": self.kitchen.pk,
                    "delivery": False,
                    "offer": self.offer.pk,
                    "quantity": 1,
                    "choices": [
                        {
                            "dish": self.dish.pk,
                            "version": self.dish.version,
                            "option": None,
                            "extras": [],
                        }
                    ],
                }
            ],
        }

    def post_order(self):
        client = self.make_client(self.customer_email, self.customer_password)
        return client.post(
            "/api/order", data=json.dumps(self.request), content_type="application/json"
        )

    def test_valid_order(self):
        response = self.post_order()
        self.assertEqual(response.status_code, 200)

    def test_out_of_stock(self):
        self.request["items"][0]["quantity"] = 10
        response = self.post_order()
        self.assertEqual(response.status_code, 400)
        self.assertTrue("detail" in response.data)
        self.assertTrue("out_of_stock" in response.data.get("detail", {}))
