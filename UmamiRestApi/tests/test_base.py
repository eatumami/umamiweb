""" Base for tests. Has helpers for accessing the rest api. """

import django.test
from rest_framework.test import APIClient
from UmamiCore.models import *
import UmamiCore.entities as entities


class AuthAPI:
    """ A wrapper for the auth portion of the rest api. """

    @staticmethod
    def login(client, email, password):
        """ Send a login request, and set the client's credentials. """
        request = {"email": email, "password": password}
        response = client.post("/api/auth/login/", request)
        status = response.status_code
        token = None
        if status == 200 and "key" in response.data:
            token = response.data["key"]
            client.credentials(HTTP_AUTHORIZATION="Token " + token)
        return token, status

    @staticmethod
    def logout(client, key):
        """ Send a logout request, and clear the client's credentials. """
        request = {"key": key}
        response = client.post("/api/auth/logout/", request)
        status = response.status_code
        if status == 200:
            client.credentials()
        return status


class TestCase(django.test.TestCase):
    """ Base class for tests. Has helpers for accessing the rest api. """

    def make_anon_client(self):
        return APIClient()

    def make_client(self, email, password):
        client = APIClient()
        token, status = AuthAPI.login(client, email, password)
        self.assertEqual(status, 200)
        self.assertIsNotNone(token)
        return client

    def access_as_anon(self):
        return self.make_anon_client()

    def access_as_user(self, email, password):
        user = self.make_user(email, password)
        client = self.make_client(email, password)
        return client, user

    def access_as_chef(self, email, password):
        chef, kitchen, verification = self.make_chef(email, password)
        client = self.make_client(email, password)
        return client, chef, kitchen, verification

    def make_user(self, email, password):
        user = UserFactory.create(email=email, password=password)
        user.verify_email()
        return user

    def make_chef(self, email, password):
        chef = self.make_user(email, password)
        kitchen = KitchenFactory.create(chef=chef)
        verification = KitchenVerificationFactory.create(kitchen=kitchen)
        return chef, kitchen, verification
