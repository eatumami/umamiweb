from django.apps import AppConfig


class UmamiRestApiConfig(AppConfig):
    name = "UmamiRestApi"
