# Umami API and WEB
Front End and REST APIS in Django 

This is an eCommerce platform that enables a local micro-economy by connecting people who are always on the lookout for delicious authentic food and those that are passionate about sharing their secret recipes with their neighbours.

Work currently in progress...

# Redis Cache stuff

1) Need to install redis-python, django-redis-cache, hiredis etc, in requirements.txt (run: pip3 install -r requirements.txt )

2) Need to install Redis server https://redis.io/download

3) Configure the "redis.conf" file (MACOS: /usr/local/etc/redis.conf) find "requirepass" uncomment and set it to 'umamiiscool' (no quotes)

4) Then start the Redis dev server by running "redis-server" command against the "redis.conf" file you just modified (MACOS: redis-server /usr/local/etc/redis.conf)

# Postgres stuff

1) Need to install postgresql-9.5 (brew install postgresql@9.5) and postGIS (brew install postgis)

2) Need to create a user (on the system). I made 'umamiguy'

3) Then enter:
  sudo -u postgres createuser --interactive
4) And enter the user's name as the 'role', they will need to be a superuser to create the postgis extension

5) sudo -u postgres psql
  ALTER USER umamiguy WITH PASSWORD 'umamiiscool';
  \q

6) Then log into umamiguy and create the database:
  su umamiguy
  createdb umamidb

# Running the app

1) Start your Postgres server

2) Start your Redis server (MAC: redis-server /usr/local/etc/redis.conf)

3) Use Python 3.6

4) python3 manage.py migrate

5) python3 manage.py loaddata testdata.json

6) python3 manage.py runserver 8000

then it should work ....
