"""
urls for the umamisite django application!
"""

from allauth.account import views as allauthviews
from django.conf.urls import include, url
from django.views.generic import TemplateView
from django.urls import path
from django.contrib.sitemaps.views import sitemap
from UmamiSite.helper import SitemapHelper
from django.http import HttpResponse
from . import views

import UmamiCore.converters as converters

converters.register_converters()

sitemaps = {
    'static': SitemapHelper.StaticSitemap,
    'offers': SitemapHelper.OfferSitemap,
    'kitchens': SitemapHelper.KitchenSitemap,
}

metasitemaps = {
    'offers': SitemapHelper.OfferSitemap,
    'kitchens': SitemapHelper.KitchenSitemap,
}

urlpatterns = [
    url(r"^$", TemplateView.as_view(template_name="home/index.html"), name='home'),
    url(r"^faq$", TemplateView.as_view(template_name="home/user-faq.html"), name='faq'),
    # 404
    url(
        r"^unavailable",
        TemplateView.as_view(template_name="common/not_available_yet.html"),
    ),
    # Terms
    url(r"^terms", TemplateView.as_view(template_name="common/terms_conditions.html"), name='terms'),
    # Contact Us
    url(r"^contact$", TemplateView.as_view(template_name="home/contact-us.html"), name='contact'),
    url(
        r"^contact/success$",
        TemplateView.as_view(template_name="home/contact_success.html"),
    ),
    # main results page
    url(r"^foodpage$", views.foodpage, name="results"),
    # individual food pages
    url(r"^offer$", views.offer_view),
    url(r"^bundle", views.bundle_view),
    url(r"^dish$", views.dish_view),
    # checkout and payment
    url(r"^checkout$", views.checkout_view),
    url(r"^stripe", views.stripe_redirect),
    # food creation
    url(r"^dish/new", views.new_dish),
    path("dish/edit/<hashid:pk>", views.edit_dish, name="edit_dish"),
    path("offer/new/<hashid_list:pks>", views.new_offer, name="new_offer"),
    path("offer/edit/<hashid:pk>", views.edit_offer, name="edit_offer"),
    url(r"^my_offers/calendar", views.offers_calendar),
    url(r"^my/dishes", views.sell_dish),
    # kitchen pages
    url(r"^partner$", views.partner, name='partner'),
    url(r"^partner/new", views.new_kitchen),
    url(r"^kitchen/not_complete", views.kitchen_not_complete),
    url(r"^kitchen$", views.kitchen),
    url(r"^kitchen/verification$", views.kitchen_verification),
    url(r"^kitchen/upcoming$", views.upcoming, name="kitchen_dashboard"),
    # Kitchen guide
    url(
        r"^kitchen/guide",
        TemplateView.as_view(template_name="kitchen/kitchen_guide.html"), name='kitchen_guide'
    ),
    # orders
    url(r"^order$", views.order),
    url(r"^sold", views.sold_orders, name='sold'),
    url(r"^purchased", views.purchased_orders, name='purchased'),
    # account management
    url(r"^accounts/", include("allauth.urls")),
    url(r"^accounts/edit$", views.edit_profile),
    url(r"^accounts/edit/kitchen", views.edit_kitchen),
    url(r"^accounts/dashboard", views.dashboard, name="user_profile"),
    # Reset key
    url(
        r"^accounts/password/reset/key/(?P<uidb36>[a-zA-Z0-9.+_-]+@[a-zA-Z0-9._-]+\.[a-zA-Z]+)-(?P<key>.+-.+)/$",
        allauthviews.password_reset_from_key,
        name="account_reset_password_from_key",
    ),
    url(r'^robots.txt', lambda x: HttpResponse("User-agent: *\nAllow: /", content_type="text/plain"), name="robots_file"),
    # Kitchen paths
    path("<slug:title>-k<hashid:pk>", views.kitchen, name="kitchen_social_url"),
    path("k<hashid:pk>", views.kitchen, name="kitchen_short_url"),
    # Offer (single) paths
    path("<slug:title>-o<hashid:pk>", views.offer_view, name="offer_single_social_url"),
    path("o<hashid:pk>", views.offer_view, name="offer_single_short_url"),
    # Offer (bundle) paths
    path(
        "<slug:title>-b<hashid:pk>", views.bundle_view, name="offer_bundle_social_url"
    ),
    path("b<hashid:pk>", views.bundle_view, name="offer_bundle_short_url"),
    path('sitemap.xml', sitemap, {'sitemaps': sitemaps}, name='django.contrib.sitemaps.views.sitemap'),
    path('metasitemap.xml', sitemap, {'sitemaps': metasitemaps}, name='django.contrib.sitemaps.views.sitemap'),
]
