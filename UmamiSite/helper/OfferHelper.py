import datetime
from django.utils import timezone


def get_current_client_adjusted_datetime():
    return timezone.now() - datetime.timedelta(minutes=420)


def get_future_available_date_with_display(offer, limit=50):
    client_adjusted_server_time = get_current_client_adjusted_datetime()
    start_date = (
        offer["date"]
        if isinstance(offer["date"], datetime.date)
        else datetime.datetime.strptime(offer["date"], "%Y-%m-%d").date()
    )

    if not offer["is_recurring"]:
        return [
            {
                "name": start_date.strftime("%Y-%m-%d"),
                "value": start_date.strftime("%a %m/%d"),
            }
        ]

    available_dates = []
    end_date = (
        offer["end_date"]
        if isinstance(offer["end_date"], datetime.date)
        else datetime.datetime.strptime(offer["end_date"], "%Y-%m-%d").date()
    )
    order_by_time = (
        offer["order_by"]
        if isinstance(offer["order_by"], datetime.time)
        else datetime.datetime.strptime(offer["order_by"], "%H:%M:%S").time()
    )

    increment_days_by = 1 if offer["repeat_every"] == "day" else 7

    limit_counter = 0

    while start_date <= end_date and limit_counter < limit:
        if (start_date < client_adjusted_server_time.date()) or (
                start_date == client_adjusted_server_time.date()
                and order_by_time <= client_adjusted_server_time.time()
        ):
            start_date += datetime.timedelta(days=increment_days_by)
            continue
        available_dates.append(
            {
                "name": start_date.strftime("%Y-%m-%d"),
                "value": start_date.strftime("%a %m/%d"),
            }
        )
        start_date += datetime.timedelta(days=increment_days_by)
        limit_counter += 1

    return available_dates


def get_all_offering_dates(offer):
    '''
    This return a list of all dates that are
    :param offer: offer
    :param limit: max offering dates we want to return
    :return:
    '''
    start_date = (
        offer["date"]
        if isinstance(offer["date"], datetime.date)
        else datetime.datetime.strptime(offer["date"], "%Y-%m-%d").date()
    )

    if not offer["is_recurring"]:
        return [start_date.strftime("%Y-%m-%d")]

    end_date = (
        offer["end_date"]
        if isinstance(offer["end_date"], datetime.date)
        else datetime.datetime.strptime(offer["end_date"], "%Y-%m-%d").date()
    )

    order_by = (
        offer["order_by"]
        if isinstance(offer["order_by"], datetime.time)
        else datetime.datetime.strptime(offer["order_by"], "%H:%M:%S").time()
    )

    available_dates = []

    increment_days_by = 1 if offer["repeat_every"] == "day" else 7

    while start_date <= end_date:
            available_dates.append(start_date.strftime("%Y-%m-%d"))
            start_date += datetime.timedelta(days=increment_days_by)
    return available_dates
