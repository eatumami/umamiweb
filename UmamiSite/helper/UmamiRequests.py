import requests


def get_with_header(url, request, params={}):
    if request.COOKIES.get("umamiAuthToken"):
        headers = {"Authorization": f"Token {request.COOKIES.get('umamiAuthToken')}"}
        response = requests.get(
            url=url, cookies=request.COOKIES, headers=headers, params=params
        )
    else:
        response = requests.get(url, cookies=request.COOKIES, params=params)

    if response.status_code == 401:
        response = requests.get(url, cookies=request.COOKIES, params=params)
    return response.json() if response.ok else {}
