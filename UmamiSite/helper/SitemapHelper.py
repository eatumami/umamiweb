from django.contrib.sitemaps import Sitemap
from django.urls import reverse
from django.utils.text import slugify
from UmamiCore.models import Offer, Kitchen


class StaticSitemap(Sitemap):
    priority = 0.7
    changefreq = 'weekly'

    def items(self):
        return ['home', 'faq', 'terms', 'contact', 'partner', 'kitchen_guide', 'sold', 'purchased', 'user_profile', 'account_login']

    def location(self, item):
        return reverse(item)


class OfferSitemap(Sitemap):
    priority = 0.7
    changefreq = 'weekly'

    def items(self):
        offers = Offer.objects.all()
        return offers

    def location(self, offer):
        base_uri_name = (
            "offer_bundle_social_url"
            if offer.dishes.count() > 1
            else "offer_single_social_url"
        )

        offer_name = offer.name or "umami-offer"

        return reverse(
            base_uri_name,
            kwargs={"title": slugify(offer_name), "pk": offer.pk.hashid},
        )


class KitchenSitemap(Sitemap):
    priority = 0.8
    changefreq = 'daily'

    def items(self):
        kitchens = Kitchen.objects.all()
        return kitchens

    def location(self, kitchen):
        return reverse(
            "kitchen_social_url",
            kwargs={"title": slugify(kitchen.title), "pk": kitchen.pk.hashid},
        )
