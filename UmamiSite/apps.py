from django.apps import AppConfig


class UmamiSiteConfig(AppConfig):
    name = "UmamiSite"
