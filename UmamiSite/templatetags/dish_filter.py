from datetime import datetime
from django import template
from itertools import chain
from django.template import defaultfilters

register = template.Library()


@register.filter
def joinby(value, arg):
    return arg.join([item["name"] for item in value])


@register.filter
def parse_tags(value):
    str_list = []
    for tag in value:
        str_list.append(f"<span>{tag.get('name')}</span>")
    return "".join(str_list)


@register.filter
def get_avg_bundle_rating(dishes):
    ratings_list = [dish["rating"]["score"] for dish in dishes]
    return sum(ratings_list) / float(len(ratings_list)) if len(ratings_list) else 0


@register.filter
def get_dish_reviews_for_bundle(dishes):
    reviews = [review for review in [dish["reviews"] for dish in dishes]]
    reviews = list(chain(*reviews))
    return reviews


@register.filter
def get_day_of_week(date):
    for fmt in ("%m/%d/%Y", "%Y-%m-%d", "%d.%m.%Y", "%d/%m/%Y", "%d-%m-%Y"):
        try:
            return datetime.strptime(date, fmt).strftime("%a")
        except ValueError:
            pass
    return ""


@register.filter
def get_correct_date_format(date):
    for fmt in ("%m/%d/%Y", "%Y-%m-%d", "%d.%m.%Y", "%d/%m/%Y", "%d-%m-%Y"):
        try:
            return datetime.strptime(date, fmt).strftime("%Y-%m-%d")
        except ValueError:
            pass
    return ""


@register.filter
def get_simplified_date(date):
    for fmt in ("%m/%d/%Y", "%Y-%m-%d", "%d.%m.%Y", "%d/%m/%Y", "%d-%m-%Y"):
        try:
            return datetime.strptime(date, fmt).strftime("%a %m/%d")
        except ValueError:
            pass
    return ""


@register.filter
def parse_lead_time(lead_time):
    lead_time_split = lead_time.split(":")
    if len(lead_time_split) == 3:
        (h, m, s) = lead_time_split
        return int(h) + int(m) / 60
    return lead_time


@register.filter
def get_availability_for_date(offer, url_date):
    offer_availables = offer["availables"]
    if len(offer['ready_dates']) == 0:
        return 0
    if len(offer_availables) > 0:
        date = url_date or offer['ready_dates'][0]["name"]
        for available_obj in offer_availables:
            if available_obj["date"] == date:
                return available_obj["available"] if available_obj["available"] and available_obj["available"] >= 0 else 0
    return offer["total_available"]


@register.filter
def available_for_purchase(offer, url_date):
    return (
        not offer["is_expired"]
        and get_availability_for_date(offer, url_date) > 0
        and (offer.get("can_deliver", False) or offer.get("can_pickup", False))
    )


@register.filter
def make_safe(value, default_value=[]):
    if value:
        return defaultfilters.safe(value)
    return default_value
