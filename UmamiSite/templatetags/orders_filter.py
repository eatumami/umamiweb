from django import template
from datetime import datetime
from decimal import *

register = template.Library()


@register.filter
def convert_to_simple_date(value):
    if value:
        d = datetime.strptime(value.split("T")[0], "%Y-%m-%d")
        return d.strftime("%A, %B %d %Y")
    return ""


@register.filter
def calculate_item_total(item):
    item_subtotal = int(item["quantity"]) * Decimal(item["offer"]["price"])
    extra_subtotal = 0
    for extra in item["extras"]:
        extra_subtotal += int(extra["quantity"]) * Decimal(extra["price"])
    return round(item_subtotal + extra_subtotal, 2)


@register.filter
def get_readable_address(address):
    if address["line2"] != "":
        return "{}".format(
            ", ".join(
                [
                    address["line1"].title(),
                    address["line2"].title(),
                    address["city"].title(),
                    address["prov"].upper(),
                    address["postal_code"].upper(),
                    address["country"].title(),
                ]
            )
        )
    else:
        return "{}".format(
            ", ".join(
                [
                    address["line1"].title(),
                    address["city"].title(),
                    address["prov"].upper(),
                    address["postal_code"].upper(),
                    address["country"].title(),
                ]
            )
        )


@register.filter
def get_shortened_address(address):
    if address["line2"] != "":
        return "{}".format(
            ", ".join(
                [
                    address["line1"].title(),
                    address["line2"].title(),
                    address["city"].title(),
                ]
            )
        )
    else:
        return "{}".format(
            ", ".join([address["line1"].title(), address["city"].title()])
        )


@register.filter
def get_pretty_time(time):
    return datetime.strptime(time, "%H:%M:%S").strftime("%I:%M%p")
