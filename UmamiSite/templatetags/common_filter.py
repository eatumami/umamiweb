from django import template
from phonenumbers import NumberParseException
import phonenumbers

register = template.Library()


@register.filter
def prettify_phone_number(phone):
    try:
        return phonenumbers.format_number(
            phonenumbers.parse(phone, "US"), phonenumbers.PhoneNumberFormat.NATIONAL
        )
    except NumberParseException:
        return ""


@register.filter(is_safe=False)
def add_floats(value, arg):
    """Add the arg to the value."""
    try:
        return float(value) + float(arg)
    except (ValueError, TypeError):
        try:
            return value + arg
        except Exception:
            return ""
