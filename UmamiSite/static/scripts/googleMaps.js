const googleApiKey = "AIzaSyDtpMmoVmNAIlheOK53ta272ltkp18FR00";

const componentForm = {
    locality: 'long_name',
    administrative_area_level_1: 'short_name',
    country: 'long_name',
    postal_code: 'short_name'
};
const addressForm = ['line2', 'locality', 'administrative_area_level_1', 'country', 'postal_code'];

let autocomplete;


const user_ready_date = getURLParameter('d') || moment().local().format("Y-MM-DD");

function initAutocomplete() {
    // Create the autocomplete object, restricting the search to geographical
    // location types.

    autocomplete = new google.maps.places.Autocomplete(
        /** @type {!HTMLInputElement} */(document.getElementById('address-autocomplete')),
        {types: ['geocode']});

    // When the user selects an address from the dropdown, populate the address
    // fields in the form.
    autocomplete.addListener('place_changed', function (e) {
        let place = autocomplete.getPlace();
        let components = place.address_components;
        // If user inputs an address that is not selected from the autocomplete picklist
        if (!components) {
            $.ajax({
                url: `https://maps.googleapis.com/maps/api/geocode/json?address=${place.name}&key=${googleApiKey}`,
                type: 'get',
                success: function (response) {
                    place = response.results[0];
                    components = place.address_components;
                    components.forEach(function (component) {
                        if (component.types.includes("postal_code")) {
                            localStorage.userPostalCode = component.long_name;
                        }
                    });
                    localStorage.userAddress = place.formatted_address;
                    localStorage.userLongitude = document.getElementsByName("lon")[0].value = place.geometry.location.lng;
                    localStorage.userLatitude = document.getElementsByName("lat")[0].value = place.geometry.location.lat;
                    localStorage.userReadyDate = user_ready_date;
                    document.getElementsByName('d')[0].value = user_ready_date;
                    document.getElementById('delivery-street-address').submit()
                },
                error: function () {
                    //TODO: input validation, show error msg
                }

            });
        } else {
            components.forEach(function (component) {
                if (component.types.includes("postal_code")) {
                    localStorage.userPostalCode = component.long_name;
                }
            });
            localStorage.userAddress = place.formatted_address;
            localStorage.userLongitude = document.getElementsByName("lon")[0].value = place.geometry.location.lng();
            localStorage.userLatitude = document.getElementsByName("lat")[0].value = place.geometry.location.lat();
            localStorage.userReadyDate = user_ready_date;
            document.getElementsByName('d')[0].value = user_ready_date;
            document.getElementById('delivery-street-address').submit()
        }
    });

    google.maps.event.addDomListener(document.getElementById('delivery-street-address'), 'keydown', function (e) {
        if (e.keyCode === 13) {
            e.preventDefault();
        }
    });

    // We need this to attach the Google Maps autocomplete to the top
    setTimeout(function () {
        "use strict";
        $(".pac-container").prependTo("#top-navigation");
    }, 300);
}


function initGoogleMapsForForm(callback) {
    "use strict";
    // Init Google maps search for Line1
    autocomplete = new google.maps.places.Autocomplete(
        /** @type {!HTMLInputElement} */(document.getElementById('line1')),
        {types: ['geocode']});

    google.maps.event.addListener(autocomplete, 'place_changed', function () {
        let i = 5;
        while (i--) {
            document.getElementById(addressForm[i]).value = "";
        }
        const place = autocomplete.getPlace();
        document.getElementById("line1").value = document.getElementById("line1").value.split(",")[0];
        document.getElementById("lon").value = place.geometry.location.lng();
        document.getElementById("lat").value = place.geometry.location.lat();
        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (let i = 0; i < place.address_components.length; i++) {
            let addressType = place.address_components[i].types[0];
            if (componentForm[addressType]) {
                document.getElementById(addressType).value = place.address_components[i][componentForm[addressType]];
            }
        }
        if (callback) {
            callback();
        }
    });
}

function geolocate() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            const geolocation = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            const circle = new google.maps.Circle({
                center: geolocation,
                radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
        });
    }
}

async function getGooglePlace(userAddress) {
    return await $.ajax({
        url: `https://maps.googleapis.com/maps/api/geocode/json?address=${userAddress}&key=${googleApiKey}`,
        type: 'get',
        success: (function (data) {
            return data;
        })
    });
}
