$(function () {
    if (localStorage.cartFoodItems) {
        let myOrder = JSON.parse(localStorage.cartFoodItems);
        if (myOrder && myOrder.cartItems.length > 0 && getTotalCartQty()) {
            $("#cart-indicator").html(getTotalCartQty()).show();
        }
    }

    localStorage.userReadyDate = getURLParameter('d');

    let user_address = getURLParameter('address') || localStorage.userAddress;

    const user_lon = getURLParameter('lon');
    const user_lat = getURLParameter('lat');
    if (user_address) {
        localStorage.userAddress = user_address;
    }
    if (user_lon && user_lat) {
        localStorage.userLongitude = user_lon;
        localStorage.userLatitude = user_lat;
    } else {
        if (user_address) {
            getGooglePlace(user_address).then(function (response) {
                const place = response.results[0];
                const components = place.address_components;
                components.forEach(function (component) {
                    if (component.types.includes("postal_code")) {
                        localStorage.userPostalCode = component.long_name;
                    }
                });
                localStorage.userAddress = place.formatted_address;
                localStorage.userLongitude = document.getElementsByName("lon")[0].value = place.geometry.location.lng;
                localStorage.userLatitude = document.getElementsByName("lat")[0].value = place.geometry.location.lat;
                document.getElementsByName('d')[0].value = moment().local().format("Y-MM-DD");
            })
        }
    }
    if (localStorage.userAddress && localStorage.userAddress !== null) {
        document.getElementById("address-autocomplete").value = localStorage.userAddress;
    }
    if (!localStorage.profanities) {
        localStorage.setItem('profanities', /2g1c\b|\b2 girls 1 cup\b|\bacrotomophilia\b|\banal\b|\banilingus\b|\banus\b|\barsehole\b|\bass\b|\basshole\b|\bassmunch\b|\bauto erotic\b|\bautoerotic\b|\bbabeland\b|\bbaby batter\b|\bball gag\b|\bball gravy\b|\bball kicking\b|\bball licking\b|\bball sack\b|\bball sucking\b|\bbangbros\b|\bbareback\b|\bbarely legal\b|\bbarenaked\b|\bbastardo\b|\bbastinado\b|\bbbw\b|\bbdsm\b|\bbeaver cleaver\b|\bbeaver lips\b|\bbestiality\b|\bbi curlous\b|\bbig black\b|\bbig breasts\b|\bbig knockers\b|\bbig tits\b|\bbimbos\b|\bbirdlock\b|\bbitch\b|\bblack cock\b|\bblonde action\b|\bblonde on blonde action\b|\bblow j\b|\bblow your l\b|\bblue waffle\b|\bblumpkin\b|\bbollocks\b|\bbondage\b|\bboner\b|\bboob\b|\bboobs\b|\bbooty call\b|\bbrown showers\b|\bbrunette action\b|\bbukkake\b|\bbulldyke\b|\bbullet vibe\b|\bbung hole\b|\bbunghole\b|\bbusty\b|\bbutt\b|\bbuttcheeks\b|\bbutthole\b|\bcamel toe\b|\bcamgirl\b|\bcamslut\b|\bcamwhore\b|\bcarpet muncher\b|\bcarpetmuncher\b|\bchocolate rosebuds\b|\bcirclejerk\b|\bcleveland steamer\b|\bclit\b|\bclitoris\b|\bclover clamps\b|\bclusterfuck\b|\bcock\b|\bcocks\b|\bcoprolagnia\b|\bcoprophilia\b|\bcornhole\b|\bcum\b|\bcumming\b|\bcunnilingus\b|\bcunt\b|\bdarkie\b|\bdate rape\b|\bdaterape\b|\bdeep throat\b|\bdeepthroat\b|\bdick\b|\bdildo\b|\bdirty pillows\b|\bdirty sanchez\b|\bdog style\b|\bdoggie style\b|\bdoggiestyle\b|\bdoggy style\b|\bdoggystyle\b|\bdolcett\b|\bdomination\b|\bdominatrix\b|\bdommes\b|\bdonkey punch\b|\bdouble dong\b|\bdouble penetration\b|\bdp action\b|\beat my ass\b|\becchi\b|\bejaculation\b|\berotic\b|\berotism\b|\bescort\b|\bethical slut\b|\beunuch\b|\bfaggot\b|\bfecal\b|\bfelch\b|\bfellatio\b|\bfeltch\b|\bfemale squirting\b|\bfemdom\b|\bfigging\b|\bfingering\b|\bfisting\b|\bfoot fetish\b|\bfootjob\b|\bfrotting\b|\bfuck\b|\bfuck buttons\b|\bfudge packer\b|\bfudgepacker\b|\bfutanari\b|\bg-spot\b|\bgang bang\b|\bgay sex\b|\bgenitals\b|\bgiant cock\b|\bgirl on\b|\bgirl on top\b|\bgirls gone wild\b|\bgoatcx\b|\bgoatse\b|\bgokkun\b|\bgolden shower\b|\bgoo girl\b|\bgoodpoop\b|\bgoregasm\b|\bgrope\b|\bgroup sex\b|\bguro\b|\bhand job\b|\bhandjob\b|\bhard core\b|\bhardcore\b|\bhentai\b|\bhomoerotic\b|\bhonkey\b|\bhooker\b|\bhot chick\b|\bhow to kill\b|\bhow to murder\b|\bhuge fat\b|\bhumping\b|\bincest\b|\bintercourse\b|\bjack off\b|\bjail bait\b|\bjailbait\b|\bjerk off\b|\bjigaboo\b|\bjiggaboo\b|\bjiggerboo\b|\bjizz\b|\bjuggs\b|\bkike\b|\bkinbaku\b|\bkinkster\b|\bkinky\b|\bknobbing\b|\bleather restraint\b|\bleather straight jacket\b|\blemon party\b|\blolita\b|\blovemaking\b|\bmake me come\b|\bmale squirting\b|\bmasturbate\b|\bmenage a trois\b|\bmilf\b|\bmissionary position\b|\bmotherfucker\b|\bmound of venus\b|\bmr hands\b|\bmuff diver\b|\bmuffdiving\b|\bnambla\b|\bnawashi\b|\bnegro\b|\bneonazi\b|\bnig nog\b|\bnigga\b|\bnigger\b|\bnimphomania\b|\bnipple\b|\bnipples\b|\bnsfw images\b|\bnude\b|\bnudity\b|\bnympho\b|\bnymphomania\b|\boctopussy\b|\bomorashi\b|\bone cup two girls\b|\bone guy one jar\b|\borgasm\b|\borgy\b|\bpaedophile\b|\bpanties\b|\bpanty\b|\bpedobear\b|\bpedophile\b|\bpegging\b|\bpenis\b|\bphone sex\b|\bpiece of shit\b|\bpiss pig\b|\bpissing\b|\bpisspig\b|\bplayboy\b|\bpleasure chest\b|\bpole smoker\b|\bponyplay\b|\bpoof\b|\bpoop chute\b|\bpoopchute\b|\bporn\b|\bporno\b|\bpornography\b|\bprince albert piercing\b|\bpthc\b|\bpubes\b|\bpussy\b|\bqueef\b|\braghead\b|\braging boner\b|\brape\b|\braping\b|\brapist\b|\brectum\b|\breverse cowgirl\b|\brimjob\b|\brimming\b|\brosy palm\b|\brosy palm and her 5 sisters\b|\brusty trombone\b|\bs&m\b|\bsadism\b|\bscat\b|\bschlong\b|\bscissoring\b|\bsemen\b|\bsex\b|\bsexo\b|\bsexy\b|\bshaved beaver\b|\bshaved pussy\b|\bshemale\b|\bshibari\b|\bshit\b|\bshota\b|\bshrimping\b|\bslanteye\b|\bslut\b|\bsmut\b|\bsnatch\b|\bsnowballing\b|\bsodomize\b|\bsodomy\b|\bspic\b|\bspooge\b|\bspread legs\b|\bstrap on\b|\bstrapon\b|\bstrappado\b|\bstrip club\b|\bstyle doggy\b|\bsuck\b|\bsucks\b|\bsuicide girls\b|\bsultry women\b|\bswastika\b|\bswinger\b|\btainted love\b|\btaste my\b|\btea bagging\b|\bthreesome\b|\bthroating\b|\btied up\b|\btight white\b|\btit\b|\btits\b|\btitties\b|\btitty\b|\btongue in a\b|\btopless\b|\btosser\b|\btowelhead\b|\btranny\b|\btribadism\b|\btub girl\b|\btubgirl\b|\btushy\b|\btwat\b|\btwink\b|\btwinkie\b|\btwo girls one cup\b|\bundressing\b|\bupskirt\b|\burethra play\b|\burophilia\b|\bvagina\b|\bvenus mound\b|\bvibrator\b|\bviolet wand\b|\bvorarephilia\b|\bvoyeur\b|\bvulva\b|\bwank\b|\bwet dream\b|\bwetback\b|\bwhite power\b|\bwomen rapping\b|\bwrapping men\b|\bwrinkled starfish\b|\bxx\b|\bxxx\b|\byaoi\b|\byellow showers\b|\byiffy\b|\bzoophilia/gi);
    }

    $('.umami-logo-wrapper').on("click", function () {
        "use strict";
        navigateHome();
    });

});

function toggleOptions() {
    if ($(".options-menu").hasClass("open-options")) {
        closeOptions();
    } else {
        $(".options-menu").addClass("open-options");
        $(".options-menu-button").removeClass("fa-bars").addClass("fa-times");
    }
}

function closeOptions() {
    $(".options-menu").removeClass("open-options");
    $(".options-menu-button").removeClass("fa-times").addClass("fa-bars");
}
