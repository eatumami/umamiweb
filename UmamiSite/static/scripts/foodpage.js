var umamiWeb = angular.module('umamiWeb', []);
let page = 2; // Initial value for offers pagination
let foodpage_offers;
// Angularjs
umamiWeb.config(function ($interpolateProvider, $locationProvider, $compileProvider) {
    $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|javascript):/);
    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
    });
});

umamiWeb.directive('onFinishRender', function ($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            if (scope.$last === true) {
                $(".additional").css("display", "block");
                $timeout(function () {
                    scope.$emit(attr.onFinishRender);
                });
            }
        }
    }
});

umamiWeb.controller("overlayOfferCtrl", function ($scope, $http) {
    $(".loading").show();
    $scope.food_item = Object.assign({}, foodpage_offers.singles.find(_offer => _offer.id === $("#overlay-offer").data("food-id").toString()));
    $scope.food_item.order_by = parse_datetime($scope.food_item.order_by);
    $scope.food_item.ready_date = parse_ready_date();

    const ready_date = localStorage.overlayReadyDate || localStorage.userReadyDate;
    const available_obj = $scope.food_item.availables.find(available => available.date === ready_date);
    $scope.food_item.available = available_obj ? available_obj.available : $scope.food_item.total_available;
    if ($scope.food_item.can_deliver) {
        $scope.food_item.deliver_time_range = parse_time_range($scope.food_item.deliver_time.start, $scope.food_item.deliver_time.end);
    }
    if ($scope.food_item.can_pickup) {
        $scope.food_item.pickup_time_range = parse_time_range($scope.food_item.pickup_time.start, $scope.food_item.pickup_time.end);
    }
    $http({
        method: 'GET',
        url: `/api/dish/reviews/${$scope.food_item.dishes[0].id}`
    }).then(function successCallback(response) {
        $scope.food_item.reviews = response.data;
    });
    overlayDishRating($scope.food_item.dishes[0].rating.score);
    $(".loading").hide();
    $("#food-dialog").show();
});


umamiWeb.controller("overlayOfferBundleCtrl", function ($scope, $http) {
    $(".loading").show();
    $scope.food_item = Object.assign({}, foodpage_offers.bundles.find(_offer => _offer.id === $("#overlay-bundle").data("food-id").toString()));
    $scope.food_item.order_by = parse_datetime($scope.food_item.order_by);
    $scope.food_item.ready_date = parse_ready_date();

    if ($scope.food_item.can_deliver) {
        $scope.food_item.deliver_time_range = parse_time_range($scope.food_item.deliver_time.start, $scope.food_item.deliver_time.end);
    }
    if ($scope.food_item.can_pickup) {
        $scope.food_item.pickup_time_range = parse_time_range($scope.food_item.pickup_time.start, $scope.food_item.pickup_time.end);
    }
    const dish_ids = $scope.food_item.dishes.map(dish => dish.id);
    $http({
        method: 'GET',
        url: `/api/dish/reviews/${dish_ids.join(",")}`
    }).then(function successCallback(response) {
        $scope.food_item.reviews = response.data;
    });

    overlayDishRating(getAvgBundleRating($scope.food_item.dishes));
    $(".loading").hide();
    $("#food-dialog").show();
});


umamiWeb.controller("overlayDishCtrl", function ($scope, $http) {
    $(".loading").show();
    $http({
        method: 'GET',
        url: $("#overlay-dish").data("food-url")
    }).then(function successCallback(response) {
        $scope.food_item = {};
        $scope.food_item['dishes'] = response.data;
        overlayDishRating($scope.food_item.dishes[0].rating.score);
        $(".loading").hide();
        $("#food-dialog").show();
    }, function errorCallback(response) {
        // called asynchronously if an error occurs
        // or server returns response with an error status.
    });
    $http({
        method: 'GET',
        url: `/api/dish/reviews/${$("#overlay-dish").data('id')}`
    }).then(function successCallback(response) {
        $scope.food_item.reviews = response.data;
    }, function errorCallback(response) {
        // called asynchronously if an error occurs
        // or server returns response with an error status.
    });
});

umamiWeb.controller("overlayKitchenCtrl", function ($scope, $http) {
    $(".loading").show();
    $http({
        method: 'GET',
        url: $("#overlay-kitchen").data("kitchen-url"),
        params: {
            "address": localStorage.userAddress,
            "lat": localStorage.userLatitude,
            "lon": localStorage.userLongitude
        }
    }).then(function successCallback(response) {
        $scope.kitchen = response.data;

        $scope.kitchen.current_offers.forEach(function (current_offer) {
            current_offer["display_date"] = parse_date(current_offer.date);
            current_offer.offers.forEach(function (offer) {
                if (offer.is_bundle) {
                    foodpage_offers.bundles = [...foodpage_offers.bundles.filter(_offer => _offer.id !== offer.id)];
                    foodpage_offers.bundles.push(offer)
                } else {
                    foodpage_offers.singles = [...foodpage_offers.singles.filter(_offer => _offer.id !== offer.id)];
                    foodpage_offers.singles.push(offer)
                }
            });
        });
        overlayKitchenRating($scope.kitchen.rating.score);
        $(".loading").hide();
        $("#chef-dialog").show();
    }, function errorCallback(response) {
    });
});


umamiWeb.controller("mainCtrl", function ($scope, $compile) {
    $scope.activateView = function (ele) {
        $compile(ele.contents())($scope);
        $scope.$apply();
    };
});

umamiWeb.controller("foodlistCtrl", function ($scope, $http) {
    $scope.food_items = [];
    $scope.loadMore = function () {
        $("#foodpage-loader").show();
        $http({
            method: 'GET',
            url: '/api/singles',
            params: {
                'page': page,
                'address': getUserAddress(),
                'lon': getUserLongitude(),
                'lat': getUserLatitude(),
                'd': localStorage.userReadyDate
            }
        }).then(function successCallback(response) {
            // Concat the existing foodpage results with additional results
            if (foodpage_offers && foodpage_offers.singles) {
                foodpage_offers.singles.push(...response.data);
            }
            if (response.data.length > 0) {
                for (let i = 0; i < response.data.length; i++) {
                    if (response.data[i].can_deliver) {
                        response.data[i].parsed_deliver_time = {};
                        response.data[i].parsed_deliver_time.start = get_pretty_time(response.data[i].deliver_time.start);
                    } else {
                        response.data[i].parsed_pickup_time = {};
                        response.data[i].parsed_pickup_time.start = get_pretty_time(response.data[i].pickup_time.start);
                    }

                    $scope.food_items.push(response.data[i]);
                }
                if (!$scope.$$phase) {
                    $scope.$apply();
                }
                loading = false;
                page++ //Increment page number
            } else {
                $(".footer").show()
            }
            $("#foodpage-loader").hide();
        }, function errorCallback(response) {

        });
    };
});

umamiWeb.directive("rateStar", function () {
    return {
        restrict: "A",
        link: function (scope, ele, attr) {
            let starWidth = "15px";
            if (attr.starWidth) {
                starWidth = attr.starWidth;
            }
            $(ele).rateYo({
                rating: attr.rateStar,
                starWidth: starWidth,
                ratedFill: "#f5e258",
                normalFill: "lightgrey",
                readOnly: true
            });

        }
    };
});

function setOverlayCtrl(wrap) {
    var mainCtrl = angular.element(document.body);
    mainCtrl.scope().activateView(wrap);
}

let loading = false;
let lastScrollTop = 0;

function getAvgBundleRating(dishes) {
    let sum = 0;
    let i = dishes.length;

    while (i--) {
        sum += dishes[i].rating.score
    }
    return sum / dishes.length;
}


function fillRatings() {
    const $dishRatings = $(".home-dish-rating");
    for (let i = 0; i < $dishRatings.length; i++) {
        $($dishRatings[i]).rateYo("rating", $($dishRatings[i]).data("rating"));
    }
}

function isMobile() {
    return (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent));
}

$("body").popover({
    selector: ".food-distance",
    html: true,
    trigger: 'hover',
    placement: 'top',
    container: 'body',
    content: function () {
        return '<img style="object-fit: cover;" height="300" width="500" src="' + $(this).data('img') + '" />';
    }
}).popover({
    selector: '[data-toggle="popover"]',
    trigger: "hover",
    html: true,
    container: 'body',
    content: function () {
        return $(this).attr('data-content');
    }
});

$(".bundle-image-hover").hover(function () {
    if ($(this).hasClass("bundle-carousel-controls")) {
        $(this).show()
    } else {
        $('#' + $(this).data("carousel-control-id")).show(80);

    }
}, function () {
    if ($(this).hasClass("bundle-carousel-controls")) {
        $(this).hide();
    } else {
        $('#' + $(this).data("carousel-control-id")).hide();
    }
});

//Visual
var overlayKitchenRating = function (val) {
    $('#overlay-kitchen-rating').rateYo({
        starWidth: "25px",
        rating: parseFloat(val),
        ratedFill: "#f5e258",
        normalFill: "lightgrey",
        readOnly: true
    });
};

var overlayDishRating = function (val) {
    $('#overlay-dish-rating').rateYo({
        starWidth: "25px",
        rating: parseFloat(val),
        ratedFill: "#f5e258",
        normalFill: "lightgrey",
        readOnly: true
    });
};

$(".bundle-preview-container").mouseenter(function () {
    $(this).hide(80);
});

$('.carousel').carousel({
    pause: true,
    interval: false
}).mouseleave(function () {
    $(".bundle-preview-container").show();
});


$(function () {
    foodpage_offers = JSON.parse(document.getElementById('foodpage-offers').textContent);
    $(window).on("resize", function () {
        resizeOverlay();
    }).resize();

    $('.home-dish-rating').rateYo({
        starWidth: "15px",
        ratedFill: "#f5e258",
        normalFill: "lightgrey",
        readOnly: true
    });
    fillRatings();
    sessionStorage.setItem("foodpage_url", window.location.href.replace(/^(?:\/\/|[^\/]+)*\//, ""));

    $(window).scroll(function () {
        closeOptions();
        const scrollDelta = $(document).height() - ($(window).scrollTop() + $(window).height());
        const st = $(this).scrollTop();

        if (st > lastScrollTop && st >= 10 && !isMobile()) {
            $("#top-weekly-calendar").fadeOut("fast");
        } else {
            $("#top-weekly-calendar").fadeIn("fast");
        }

        lastScrollTop = st;
        if (scrollDelta <= 60 && scrollDelta >= 0) {
            if (!loading) {
                loading = true;
                angular.element(document.getElementById('offerList')).scope().loadMore();
            }
        }
    });
});