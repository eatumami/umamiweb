$("#foodModal,#food-form").on("click", ".bundle-offer-collapse-button", function() {
    $('.collapse').collapse('hide');
    $("#BundleOverlayCarousel").carousel($(this).data("carousel-collapse-id"));
}).on('slide.bs.carousel', "#BundleOverlayCarousel", function (e) {
    $('.collapse').collapse('hide');
    var slideTo = $(e.relatedTarget).index();
    var offerCollapseId = `#bundle-offer-collapse-${slideTo.toString()}`;
    $(offerCollapseId).collapse('show');
});

