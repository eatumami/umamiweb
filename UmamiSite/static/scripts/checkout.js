const stripe_public_key = 'pk_live_Ixa9CLboPPF9RNmzYS9lEGtk';
const stripe_public_test_key = 'pk_test_dmjpvYIbDbbgs7gmKfrPojMe';
const google_maps_api_key = 'AIzaSyB0fUOwiokY5Vf84S2iv8posGFBeR2DgMo';
let timeout = null;
let newAddress = !!localStorage.userAddress;

if ($('#use-last-address').length > 0) {
    const addressString = $('#use-last-address').data('last-used-address').replace(/'/g, '"');
    const lastUsedAddress = JSON.parse(addressString);
    const subStringLimit = Math.min(lastUsedAddress['line1'].length, 6);
    const lastUsedStreetAddress = lastUsedAddress['line1'].substring(0, subStringLimit);
    if (!!localStorage.userAddress) {
        newAddress = !localStorage.userAddress.startsWith(lastUsedStreetAddress);
    }
}

function renderOrders(data) {
    $('.delivery-order-wrapper .order-items').empty();
    $('.pickup-order-wrapper .order-items').empty();

    if (data.delivery_orders.length > 0) {
        data.delivery_orders.forEach(function (order) {
            createOrderTemplate(order, true);
        });
        $(".delivery-order-wrapper").show();
    }
    if (data.pickup_orders.length > 0) {
        data.pickup_orders.forEach(function (order) {
            createOrderTemplate(order, false);
        });
        $(".pickup-order-wrapper").show();
    }
}


function createOrderTemplate(order, delivery) {
    const _orderTemplate = $("#order-item-template").html();
    const _orderItemTemplate = $("#order-info-template").html();
    const orderTemplate = $(_orderTemplate).clone();
    order.items.forEach(function (item) {
        const orderItemTemplate = $(_orderItemTemplate).clone();
        orderItemTemplate.attr('data-food-id', item.offer);
        orderItemTemplate.attr('data-uuid', item.uuid);
        orderItemTemplate.find(".order-item-subtotal .numeric-value").html(item.subtotal.toFixed(2));
        orderItemTemplate.find(".order-item-name").html(item.name);


        item.choices.forEach(function (choice) {
            if (choice.option) {
                orderItemTemplate.find(".order-item-option").html("&#183; " + choice.option.name);
            }
        });
        const $select = orderItemTemplate.find(".qty-dropdown");
        for (let i = 1; i <= item.available; i++) {
            $select.append($('<option></option>').val(i).html(i))
        }
        $select.val(item.quantity);

        item.choices.forEach(function (choice) {
            choice.extras.forEach(function (extra) {
                orderItemTemplate.find(".order-extra-items").append(`<span>&#43;${extra.name}</span>`);
            });
        });
        orderTemplate.find(".order-items-container").append(orderItemTemplate);
    });
    orderTemplate.find(".order-item-handoff-date").html(parse_date(order.handoff_date));
    orderTemplate.find(".order-item-handoff-time").html(` &#183; ${parse_time_range(order.handoff_time_start, order.handoff_time_end)}`);
    orderTemplate.find(".order-items-kitchen").html(createKitchenLink(order.kitchen));


    if (delivery) {
        orderTemplate.find(".order-item-delivery-fee").html(` &#183; $${Number(order.delivery_fee).toFixed(2)}`);
        $(".delivery-order-wrapper .order-items").append(orderTemplate);
    } else {
        const mapMarker = "<i class='glyphicon glyphicon-map-marker'></i>";
        orderTemplate.find(".order-item-pickup-location").html(mapMarker + ` <span>${parseAddress(order.handoff_address)}</span>`);
        $(".pickup-order-wrapper .order-items").append(orderTemplate);
    }
}

function renderPrice(data) {
    const sub_total = Number(data["subtotal"]).toFixed(2);
    const delivery_fee = Number(data["delivery_fee"]).toFixed(2);
    const transaction_fee = Number(data["stripe_fee"]).toFixed(2);
    const total_price = Number(data["total"]).toFixed(2);

    $("#order-breakdown #breakdown-sub-total .numeric-value").html(sub_total);
    $("#order-breakdown #other-fees .numeric-value").html(transaction_fee);

    $("#order-breakdown #delivery-fee .numeric-value").html(delivery_fee);
    $("#order-total .numeric-value").html(total_price);

    const feesTooltip = `Transaction Fee: $${transaction_fee}; This helps us run our platform and provide you with great service!`;
    $(".tax-and-fees .glyphicon-info-sign").attr('data-content', feesTooltip);

    if (data['handoff_type'] === "pickup") {
        $(".checkout-pickup-address-wrapper span").html(parseAddress(data['handoff_address']));
        $(".handoff-date").html(parse_date(data["handoff_date"]));
        $(".handoff-time-range").html(parse_time_range(data["handoff_time_start"], data["handoff_time_end"]));
        $("#pickup-address-wrapper").show()
    } else {

    }
}

function createKitchenLink(kitchen) {
    return `Made By: <a target="_blank" href='${kitchen.social_url}'>${kitchen.title}</a>`;
}


async function newPaymentCheckout(e) {
    if (addressIsValid()) {
        // Open Checkout with further options:
        await handler.open({
            name: 'Secure Checkout',
            description: '',
            amount: Number($("#order-total .numeric-value").html()) * 100,
            email: $("#user-email-address").html(),
            allowRememberMe: true,
            zipCode: false,
        });
    }
}

async function existingPaymentCheckout() {
    const order_data = await getOrderRequest();
    await createOrder(order_data)
}

const handler = StripeCheckout.configure({
    key: isProd() ? stripe_public_key : stripe_public_test_key,
    image: 'https://eatumami.s3.amazonaws.com/static/assets/umami-logo.png',
    locale: 'auto',
    token: async function (token) {
        $(".loading").show();
        const order_data = await getOrderRequest();
        order_data['stripe_token'] = token.id;
        await createOrder(order_data);
    }
});

function hasValidUserAddress() {
    return $(".use-last-address").length > 0 || addressIsValid()
}

$(async function () {
    $("body").popover({
        selector: '[data-toggle="popover"]',
        trigger: "hover",
        html: true,
        content: function () {
            return $(this).attr('data-content');
        }
    }).on("change", ".qty-dropdown", async function () {
        const foodItems = JSON.parse(localStorage.cartFoodItems);
        let uuid = $(this).parent().data('uuid');
        const newQty = $(this).val();
        foodItems.cartItems.forEach(function (obj) {
            if (obj.uuid === uuid) {
                obj.qty = newQty;
            }
        });
        updateCartItems(foodItems);
        await validateOrderTotal().catch();
    }).on('click', ".order-item-remove", async function () {
        let uuid = $(this).parent().data('uuid');
        await removeOrderItem(uuid);
        await validateOrderTotal().catch();
    });

    if ($("input[name='delivery_address']:checked").val() === "old") {
        $("#new-address-form :input").prop("disabled", true);
    }

    $("input[name='delivery_address']").on('change', async function () {
        if ($("input[name='delivery_address']:checked").val() === "old") {
            newAddress = false;
            $("#new-address-error").empty();
            $("#new-address-form :input").prop("disabled", true);
            await validateOrderTotal().then().catch();
        } else {
            $("#new-address-form :input").prop("disabled", false);
            await validateOrderTotal().then().catch();
        }
    });

    // Close Checkout on page navigation:
    window.addEventListener('popstate', function () {
        handler.close();
    });

    $("#line1, #locality, #postal_code").on("input", function () {
        clearTimeout(timeout);
        timeout = setTimeout(async function () {
            if (isLine1Valid() && isLocalityValid() && isPostalCodeValid()) {
                await getCompleteUserAddressForm();
                await validateOrderTotal().catch();
            }
        }, 2300);
    });
});

async function showAddressModal() {
    $("#force-address-input").val("");
    $("#address-modal-submit-button").attr("disabled", true);
    $('#address-modal-form').modal({
        backdrop: 'static',
        keyboard: false
    });
    await initGoogleMapsForModal();
}

async function initAddressFormAutocomplete() {
    try {
        if (!localStorage.cartFoodItems) {
            await navigateHome();
        }
        let myOrder;
        myOrder = JSON.parse(localStorage.cartFoodItems);

        if (!myOrder || myOrder.cartItems.length === 0) {
            await navigateHome();
        }

        if (localStorage.userAddress) {
            await autoFillAddressForm();
        }
        if (!hasValidUserAddress() && hasDeliveryOrder()) {
            await showAddressModal();
        } else {
            await validateOrderTotal().catch();
            initGoogleMapsForForm(validateOrderTotal);
        }
    } catch (e) {
        console.log(e.responseJSON.message);
    }
}

function initGoogleMapsForModal() {
    "use strict";
    // Init Google maps search for Line1
    autocomplete = new google.maps.places.Autocomplete(
        /** @type {!HTMLInputElement} */(document.getElementById('force-address-input')),
        {types: ['geocode']});

    google.maps.event.addListener(autocomplete, 'place_changed', function () {
        const place = autocomplete.getPlace();
        localStorage.userAddress = place.formatted_address;
        $("input[name='delivery_address'][value='new']").prop("checked", true);
        $("#new-address-form :input").prop("disabled", false);
        autoFillAddressForm();
        if (place.address_components) {
            $("#address-modal-submit-button").attr("disabled", false);
        }
    });
}

function handleAddressModalSubmit() {
    $('#address-modal-form').modal("hide");
    $(".loading").show();
    newAddress = true;
    validateOrderTotal().then().catch();
}

function hasDeliveryOrder() {
    return JSON.parse(localStorage.cartFoodItems).cartItems.some(function (item) {
        return item.delivery
    });
}

function hasPickupOrder() {
    return JSON.parse(localStorage.cartFoodItems).cartItems.some(function (item) {
        return !item.delivery
    });
}

async function getOrderRequest() {
    const cartItems = JSON.parse(localStorage.cartFoodItems).cartItems;

    let notes = $("#special-notes").val();

    const orderItems = [];

    cartItems.forEach(function (cart_item) {
        const choices = [];

        // Currently we only support Options and Extras for Single Offers, so we just append them to the only dish choice
        cart_item.offer.dishes.forEach(function (dish) {
            "use strict";
            let choice = {
                "dish": dish.id,
                "version": dish.version,
                "extras": [],
                "option": null,
            };

            if (cart_item.extras && cart_item.extras.length > 0) {
                choice['extras'] = cart_item.extras;
            }
            if (cart_item.option) {
                choice['option'] = cart_item.option;
            }

            choices.push(choice);
        });

        let offerItem = {
            "uuid": cart_item.uuid,
            "offer": cart_item.offer.id,
            "kitchen": cart_item.offer.kitchen.id,
            "quantity": cart_item.qty,
            'delivery': cart_item.delivery,
            "date": cart_item.date,
            "choices": choices,
        };

        orderItems.push(offerItem);
    });
    const orderRequest = {
        'items': orderItems,
        'notes': notes || "",
        // "when": new Date().toISOString(),
        "stripe_token": $("input:radio:checked").val(),
    };


    if (hasDeliveryOrder()) {
        orderRequest['delivery_address'] = await getCurrentlyFocusedAddress();
    }
    return orderRequest;
}

async function getCurrentlyFocusedAddress() {
    if ($("input[name='delivery_address']:checked").val() === "old" && !newAddress) {
        const address_string = $('#use-last-address').data('last-used-address').replace(/'/g, '"');
        return JSON.parse(address_string);

    } else {
        $("input[name='delivery_address'][value='new']").click();
        if (isLine1Valid() && isLocalityValid() && isPostalCodeValid()) {
            return await getCompleteUserAddressForm();
        }
    }
}


async function getCompleteUserAddressForm() {
    let userAddress = objectifyAddressForm($('#new-address-form').serializeArray());
    const googleMapsData = await getGooglePlace(getLongAddress(userAddress));
    let place = googleMapsData.results[0];
    userAddress['lon'] = place.geometry.location.lng;
    userAddress['lat'] = place.geometry.location.lat;
    userAddress['prov'] = findAddressComponent("administrative_area_level_1", place.address_components);
    userAddress['country'] = findAddressComponent("country", place.address_components);
    return userAddress;
}


function findAddressComponent(address_key, components) {
    return components.find(component => component.types.includes(address_key)).short_name;
}

function isLine1Valid() {
    return $("#line1").val() !== '';
}

function isLocalityValid() {
    return $("#locality").val() !== '';
}

function isPostalCodeValid() {
    return $("#postal_code").val() !== '';
}

function addressIsValid() {
    if ($("input[name='delivery_address']:checked").val() === "new") {
        let isValid = true;
        const $newAddressError = $('#new-address-error');
        $newAddressError.empty();
        if (!isLine1Valid()) {
            $newAddressError.append("<span>Missing Line1</span>");
            isValid = false;
        }
        if (!isLocalityValid()) {
            $newAddressError.append("<span>Missing City</span>");
            isValid = false;
        }
        if (!isPostalCodeValid()) {
            $newAddressError.append("<span>Missing Zip code</span>");
            isValid = false;
        }
        return isValid;
    }
    return true;
}


function parseAddress(address) {
    const line1StringList = address.line1.split(" ");
    let line1 = address.line1;
    // If first part of line1 contains number, it is most likely a street number, so we want to obfuscate it
    if (/\d/.test(line1StringList[0])) {
        line1StringList.shift();
        line1 = line1StringList.join(" ")
    }

    const parsed_address = (address.line2 === "") ? `${line1}, ${address.city}` : `${line1} ${address.line2}, ${address.city}`;

    return `<a href="http://maps.google.com/?q=${address.lat},${address.lon}" target="blank">${parsed_address}</a>`;
}


async function validateOrderTotal() {
    "use strict";
    $(".loading").show();
    const order_data = await getOrderRequest();
    if (order_data) {
        order_data['stripe_token'] = null;
        return await $.ajax({
            headers: {
                "Authorization": `Token ${getCookie("umamiAuthToken")}`,
                "tz_offset": new Date().getTimezoneOffset()
            },
            type: "POST",
            url: "/api/order",
            data: JSON.stringify(order_data),
            contentType: "application/json",
            success: function (data) {
                renderOrders(data);
                renderPrice(data);
                $(".checkout-error").hide();
                $("#place-order-with-existing-card-btn").attr("disabled", false);
                $(".checkout-wrapper, .payment-wrapper").show();
                $(".loading").hide();
            }, error: async function (XMLHttpRequest, textStatus, errorThrown) {
                await handleCheckoutError(XMLHttpRequest.responseJSON);
                $(".checkout-wrapper, .payment-wrapper").hide();
                $(".loading").hide();
            }
        });
    }
}

async function handleCheckoutError(responseJSON) {
    const $checkoutError = $(".checkout-error");
    $checkoutError.empty();
    $checkoutError.show();
    $checkoutError.append(`<div class="checkout-error-message">*${responseJSON.message}</div>`);
    for (let key in responseJSON.detail) {
        if (key === "delivery_out_of_range") {
            await showAddressModal();
        } else if (key === "delivery_address") {
            await showAddressModal();
            $("input[name='delivery_address'][value='old']").remove();
        }
        $checkoutError.append(`<div class="checkout-error-line">Reason: <b>${key}</b></div>`);
    }
    $checkoutError.append(`<a class="checkout-error-line" href="/contact">Contact Us</a>`);
    $checkoutError.append(`<a class="checkout-error-line" href="javascript:void(0)" onclick="navigateHome()">Go Back</a>`);
}

function createOrder(orderRequest) {
    $(".loading").append("<span>Processing your order, please do not refresh...</span>").show();
    $.ajax({
        headers: {"Authorization": `Token ${getCookie("umamiAuthToken")}`, "tz_offset": new Date().getTimezoneOffset()},
        type: "POST",
        url: "/api/order",
        data: JSON.stringify(orderRequest),
        contentType: "application/json",
        success: function (data) {
            const ids = data.map(order => order.id).join();
            localStorage.removeItem("cartFoodItems");
            window.location.href = `/order?orderIds=${ids}`;
        }, error: function () {
            $(".loading").empty().hide();
        }
    });
}

function getLongAddress(userAddress) {
    return userAddress['line1'] + ',' + userAddress['city'] + ',' + userAddress['prov'] + ',' + userAddress['country'];
}

function objectifyAddressForm(formArray) {//serialize data function
    let returnArray = {};
    for (let i = 0; i < formArray.length; i++) {
        let arrayValue = formArray[i]['value'];
        if (formArray[i]['name'] === "postal_code") {
            arrayValue = arrayValue.replace(" ", "");
        }
        returnArray[formArray[i]['name']] = arrayValue;
    }
    return returnArray;
}


function updateCartItems(myOrder) {
    localStorage.cartFoodItems = JSON.stringify(myOrder);
}

function autoFillAddressForm() {
    const user_address = localStorage.userAddress.split(',');

    if (user_address.length === 4) {
        const line1 = user_address[0].trim();
        const city = user_address[1].trim();
        const country = user_address[3].trim();
        const postal_with_prov_array = user_address[2].trim().split(' ');
        const prov = postal_with_prov_array[0];
        const postal_code = postal_with_prov_array[-1] || localStorage.userPostalCode;

        $("#line1").val(line1);
        $("#locality").val(city);
        $("#administrative_area_level_1").val(prov);
        $("#postal_code").val(postal_code);
        $("#country").val(country);
    }
}

function removeOrderItem(uuid) {
    const cart = JSON.parse(localStorage.cartFoodItems);
    let i = cart.cartItems.length;
    while (i--) {
        if (cart.cartItems[i].uuid === uuid) {
            cart.cartItems.splice(i, 1);
        }
    }
    localStorage.cartFoodItems = JSON.stringify(cart);

    const updatedCart = JSON.parse(localStorage.cartFoodItems);

    if (!hasDeliveryOrder()) {
        $(".delivery-order-wrapper").remove();
    }
    if (!hasPickupOrder()) {
        $(".pickup-order-wrapper").remove();
    }
    if (updatedCart.cartItems.length === 0) {
        $(".checkout-wrapper").remove();
        navigateHome();
    }
}


function getCookie(name) {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        let cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            let cookie = jQuery.trim(cookies[i]);

            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
