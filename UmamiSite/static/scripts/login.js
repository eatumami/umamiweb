$(function () {
    "use strict";
    $("body").on("click", "#sign-up-or-in-button", function () {
        if ($(this).html() === "Sign up") {
            loadSignUpOverlay();
        } else {
            loadSignInOverlay();
        }
    }).on("submit", ".sign-up-form", function () {
        $(".loading").show();
        const request = objectifyForm($(this).serializeArray());
        const isValid = validateSignup(request);
        request['chef_signup'] = window.location.href.includes("/partner/new");
        request['password2'] = request['password1'];
        if (isValid) {
            $.ajax({
                type: "POST",
                url: "/api/registration",
                data: request,
                success: function (data) {
                    window.location.href = "/accounts/confirm-email";
                },
                error: function (data) {
                    $(".loading").hide();
                    displaySignupError(data.responseJSON);
                }
            });
        }
        return false;
    }).on("submit", ".sign-in-form", function () {
        $(".loading").show();
        const request = $(this).serializeArray();
        const isValid = validateSignin(request);
        if (isValid) {
            $.ajax({
                type: "POST",
                url: "/api/auth/login/",
                dataType: 'json',
                data: request,
                success: function (data) {
                    writeCookie('umamiAuthToken', data.key, 30);
                    window.location.reload();
                },
                error: function (data) {
                    const response = data.responseJSON;
                    const errors = response.non_field_errors;
                    if (errors) {
                        const $signInErrors = $(".sign-in-errors");
                        $signInErrors.show();
                        errors.forEach(error => $signInErrors.append(error));
                        $(".sign-in-button").attr("disabled", true);
                    } else {
                        window.location.reload();
                    }
                    $(".loading").hide();
                }
            });
        }
        return false;
    }).on("input", "input[name='email'], input[name='password']", function () {
        $(".sign-in-button").attr("disabled", false);
    }).on("click", "#logout-button", function () {
        console.log("logging you out...");
        logoutUser();
    });

    function validateSignin(request) {
        const $signInErrors = $(".sign-in-errors");
        const requestObj = objectifyForm(request);
        $signInErrors.html('');
        if (!(requestObj['password'] && requestObj['email'])) {
            $signInErrors.append("*Please enter email and password<br>");
            $(".loading").hide();
            return false;
        }
        if (requestObj['password'].length < 6) {
            $signInErrors.append("*Password needs to be at least 6 digits long<br>");
            $(".loading").hide();
            return false;
        }
        return true;
    }

    function validateSignup(requestObj) {
        const $signUpErrors = $(".sign-up-errors");
        let isValidRequest = true;
        $signUpErrors.html('');
        if (!(requestObj['password1'] && requestObj['email'] && requestObj['phone'] && requestObj['first_name'] && requestObj['last_name'])) {
            $signUpErrors.append("*Please enter all required fields<br>");
            return false;
        }
        if (requestObj['password1'].length < 6) {
            $signUpErrors.append("*Password needs to be at least 6 digits long<br>");
            isValidRequest = false;
        }
        if (requestObj['phone'].length < 10) {
            $signUpErrors.append("*Phone number not valid<br>");
            isValidRequest = false;
        }
        return isValidRequest;
    }

    function displaySignupError(responseJSON) {
        if (responseJSON && responseJSON.detail) {
            for (let key in responseJSON.detail) {
                if (responseJSON.detail[key][0]) {
                    $(".sign-up-errors").append(`*${responseJSON.detail[key][0]} <br>`);
                }
            }
        } else {
            $(".sign-up-errors").append(`*An error has occurred, please contact us if problem persists<br>`);
        }

    }

    function objectifyForm(formArray) { //serialize data function

        let returnArray = {};
        for (let i = 0; i < formArray.length; i++) {
            returnArray[formArray[i]['name']] = formArray[i]['value'];
        }
        return returnArray;
    }


    $('#top-menu-bar').on("click", ".user-action-btn", function () {
        if ($(this).prop('id') === "sell-dish") {
            window.location.href = '/dish/new';
        } else {
            window.location.href = '/partner/new';
        }
    });
});
