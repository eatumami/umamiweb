const randomKey = `?x-eat-umami=${parseInt(Math.random() * 100000)}`;

$(function () {
    const bannerUploads = [];
    let $profileUploadCrop;

    function readFile(input, $uploadCrop) {
        if (input.files && input.files[0]) {
            const reader = new FileReader();

            reader.onload = function (e) {
                $('.inner-wrapper').addClass('ready');
                $uploadCrop.croppie('bind', {
                    url: e.target.result
                }).then(function () {
                });

            };

            reader.readAsDataURL(input.files[0]);
        } else {
            swal("Sorry - you're browser doesn't support the FileReader API");
        }
    }

    $profileUploadCrop = $('.upload-logo-image').croppie({
        url: $("#logo-image").data("image-src") + randomKey,
        viewport: {
            width: 200,
            height: 200,
            type: 'circle'
        },
        boundary: {
            width: 200,
            height: 200,
            type: 'circle'
        },
        mouseWheelZoom: false,
        enableExif: true,
    });

    for (let i = 0; i < 5; i++) {
        let $bannerImageViewport = $(`#banner-image-viewport-${i}`);
        let $bannerImage = $(`#banner-image-${i}`);
        if ($bannerImage.data("image-src") !== 'None') {
            bannerUploads.push(initCroppie($bannerImageViewport, $bannerImage.data("image-src")));
        } else {
            bannerUploads.push(initCroppie($bannerImageViewport));
        }
    }

    $('#logo-upload').on('change', function () {
        $(this).data("dirty", true);
        readFile(this, $profileUploadCrop);
    });

    $('.cover-upload').on('change', function () {
        $(this).data("dirty", true);
        readFile(this, bannerUploads[parseInt($(this).data("banner-id"))]);
    });

    $(".banner-image-reset").on("click", function () {
        "use strict";
        const $viewportSelector = $(`#banner-image-viewport-${$(this).data("viewport-id")}`);
        const $coverUpload = $(`#cover-upload-${$(this).data("viewport-id")}`);
        $viewportSelector.croppie('destroy');
        $coverUpload.data("dirty", true);
        initCroppie($viewportSelector);
        return false;
    });

    function initCroppie($selector, init_url = null) {
        if (init_url) {
            return $selector.croppie({
                url: init_url + randomKey,
                viewport: {
                    width: 365,
                    height: 160
                },
                boundary: {
                    width: 365,
                    height: 160
                },
                mouseWheelZoom: false,
                enableExif: true
            })
        }
        return $selector.croppie({
            viewport: {
                width: 365,
                height: 160
            },
            boundary: {
                width: 365,
                height: 160
            },
            mouseWheelZoom: false,
            enableExif: true
        })
    }

    function isChefFormValid() {
        let $kitchenEditError = $("#kitchen_edit_error");
        let kitchenName = $("input[name=title]").val();
        $kitchenEditError.html('');
        if (kitchenName && !kitchenName.match(/^[ a-zA-Z0-9.\-_$@*'&#+=!]{3,30}$/)) {
            $kitchenEditError.append("*Kitchen can only contain letters numbers and special characters");
            $('body').scrollTop(0);
            return false;
        }
        return true;
    }


    function formatAvailability(method, request_data) {
        "use strict";
        const weekdays = [
            'monday',
            'tuesday',
            'wednesday',
            'thursday',
            'friday',
            'saturday',
            'sunday',
        ];
        const delivery_schedule = {};
        weekdays.forEach(function (day_of_week) {
            let availability = request_data[`${method}-${day_of_week}`];
            if (availability) {
                delivery_schedule[day_of_week] = true;

            } else {
                delivery_schedule[day_of_week] = false;
            }
            delete request_data[`${method}-${day_of_week}`];
        });
        request_data[`offers_${method}`] = delivery_schedule;
    }

    $('#kitchen-edit').on('submit', async function (evt) {
        evt.preventDefault();
        $(".loading").show();
        if (await isChefFormValid()) {
            let request_data = await convertToJSON($('#kitchen-edit').serializeArray());
            formatAvailability("delivery", request_data);
            formatAvailability("pickup", request_data);
            request_data["pickup_address"] = {
                "line1": document.getElementById("line1").value,
                "line2": document.getElementById("line2").value,
                "city": document.getElementById("locality").value,
                "prov": document.getElementById("administrative_area_level_1").value,
                "country": document.getElementById("country").value,
                "postal_code": document.getElementById("postal_code").value.replace(" ", ""),
                "lon": document.getElementById("lon").value,
                "lat": document.getElementById("lat").value
            };
            await $profileUploadCrop.croppie('result', {
                type: 'canvas',
                format: "jpeg",
                quality: 0.9,
                size: {width: 250, height: 250}
            }).then(async function (resp) {
                    if ($("#logo-upload").data("dirty")) {
                        request_data["logo_image"] = resp;
                    }
                    await bannerUploads.forEach(async function (banner_upload, i) {
                        await banner_upload.croppie('result', {
                            type: 'canvas',
                            quality: 0.9,
                            format: "jpeg",
                            size: {width: 1095, height: 480}
                        }).then(function (resp) {
                            "use strict";
                            const $coverUpload = $(`#cover-upload-${i}`);
                            if (resp !== EMPTY_IMAGE_BASE_64 && $coverUpload.data("dirty")) {
                                request_data[`banner_image${i}`] = resp;
                            } else if (resp === EMPTY_IMAGE_BASE_64 && $coverUpload.data("dirty")) {
                                request_data[`banner_image${i}`] = null;
                            }
                        });
                    });
                    await $.ajax({
                        headers: {
                            "Authorization": `Token ${getCookie("umamiAuthToken")}`,
                            "tz_offset": new Date().getTimezoneOffset()
                        },
                        type: "POST",
                        url: "/api/kitchen",
                        data: JSON.stringify(request_data),
                        contentType: "application/json",
                        success: function (data) {
                            if (data['created']) {
                                window.location.href = "/kitchen/verification";
                            } else {
                                window.location.href = "/accounts/edit/kitchen"
                            }
                        }, error: function () {
                            $(".loading").hide();
                        }
                    });
                }
            );
        }
    }).bind("keypress", function (e) {
        if (e.keyCode === 13) {
            //add more buttons here
            return false;
        }
    });

    // We remove the top search bar
    $("#address-autocomplete").remove();


    // Init Google maps search for Line1
    const kitchen_address = new google.maps.places.Autocomplete(
        /** @type {!HTMLInputElement} */(document.getElementById('line1')),
        {types: ['geocode']});

    google.maps.event.addListener(kitchen_address, 'place_changed', function () {
        let i = 5;
        while (i--) {
            document.getElementById(addressForm[i]).value = "";
        }
        const place = kitchen_address.getPlace();
        document.getElementById("line1").value = document.getElementById("line1").value.split(",")[0];
        document.getElementById("lon").value = place.geometry.location.lng();
        document.getElementById("lat").value = place.geometry.location.lat();
        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (let i = 0; i < place.address_components.length; i++) {
            let addressType = place.address_components[i].types[0];
            if (componentForm[addressType]) {
                document.getElementById(addressType).value = place.address_components[i][componentForm[addressType]];
            }
        }
    });

    $(".profile-field-wrapper input, .profile-field-wrapper textarea").on("focusout", function () {
        "use strict";
        if ($('input:focus').length === 0) {
            $(".kitchen-input-tooltip *").fadeOut();
        }
    });

    $(".kitchen-overview").fadeIn(1000);

    // We need this to attach the Google Maps autocomplete to address input inside form
    setTimeout(function () {
        "use strict";
        $(".pac-container").appendTo(".pickup-address");
    }, 500);
});

function geolocate() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            const geolocation = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            const circle = new google.maps.Circle({
                center: geolocation,
                radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
        });
    }
}

function showAddressTooltip() {
    showTooltip(address_tooltip);
}

function showNameTooltip() {
    showTooltip(name_tooltip);
}

function showRangeTooltip() {
    showTooltip(range_tooltip);
}

function showBioTooltip() {
    showTooltip(bio_tooltip);
}

function showCuisineTooltip() {
    showTooltip(cuisine_tooltip);
}

function showTooltip(blob) {
    $(".kitchen-input-tooltip *").fadeOut(300, function () {
        $(".kitchen-input-tooltip *").fadeIn();
        $(".kitchen-input-tooltip .tooltip-text").html(blob);
    });
}

function initAddressFormAutocomplete() {
    initGoogleMapsForForm();
}

const EMPTY_IMAGE_BASE_64 = "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAoHBwgHBgoICAgLCgoLDhgQDg0NDh0VFhEYIx8lJCIfIiEmKzcvJik0KSEiMEExNDk7Pj4+JS5ESUM8SDc9Pjv/2wBDAQoLCw4NDhwQEBw7KCIoOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozv/wAARCAFAAtoDASIAAhEBAxEB/8QAFQABAQAAAAAAAAAAAAAAAAAAAAf/xAAUEAEAAAAAAAAAAAAAAAAAAAAA/8QAFAEBAAAAAAAAAAAAAAAAAAAAAP/EABQRAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhEDEQA/AIyAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD/2Q==";

const name_tooltip = "Every great kitchen starts with a great name, it represents your brand, and will be what people recognize. You should avoid changing it once you've established your business.";
const cuisine_tooltip = "Please specify the cuisines your kitchen will specialize in.";
const range_tooltip = "We recommend setting the delivery range to a distance which will include surrounding hot zones (colleges, offices). You will always be able to change this.";
const address_tooltip = "Your pickup address can be your home kitchen address or another address you like customers to pick up from.";
const bio_tooltip = "Please write a summary of your brand, highlight what makes your food special.";