var week_delta = 0;
var date_in_url = new URL(window.location).searchParams.get("d");


var url_date = moment();

if (date_in_url && moment(date_in_url).isSameOrAfter(moment(), "days")) {
    url_date = moment(date_in_url)
}



!function () {

    var today = moment();

    function Calendar(selector) {
        this.el = document.querySelector(selector);
        this.current = moment().startOf('week');
        this.draw_calendar();
    }


    Calendar.prototype.draw_calendar = function () {
        this.draw_week();

        if (url_date.week() > this.current.week()) {
            this.next_week();
        }
        else if (url_date.week() < this.current.week()) {
            this.prev_week();
        }
    };

    Calendar.prototype.draw_week = function () {
        var self = this;
        var right = createElement('div', 'calendar-right');
        right.addEventListener('click', function () {
            self.next_week();
        });

        var left = createElement('div', 'calendar-left');
        left.addEventListener('click', function () {
            self.prev_week();
        });

        if (this.week) {
            this.old_week = this.week;
            this.old_week.className = 'week out ' + (self.next ? 'next' : 'prev');
            self.old_week.parentNode.removeChild(self.old_week);
            self.week = createElement('div', 'week');
            self.current_week();
            self.el.appendChild(self.week);
            window.setTimeout(function () {
                self.week.className = 'week in ' + (self.next ? 'next' : 'prev');
            }, 16);
        }
        else {
            this.week = createElement('div', 'week');
            this.el.appendChild(left);
            this.el.appendChild(this.week);
            this.el.appendChild(right);
            this.current_week();
            this.week.className = 'week current';
        }
    };

    Calendar.prototype.current_week = function () {
        var clone = this.current.clone();
        while (clone.week() === this.current.week()) {
            this.draw_day(clone);
            clone.add(1, 'days');
        }
    };

    Calendar.prototype.next_week = function () {
        if (week_delta < 1) {
            this.current.add(1, 'weeks');
            this.next = true;
            this.draw_calendar();
            week_delta += 1;
        }
    };

    Calendar.prototype.prev_week = function () {
        if (week_delta > 0) {
            this.current.add(-1, 'weeks');
            this.next = false;
            this.draw_calendar();
            week_delta -= 1;
        }
    };

    Calendar.prototype.draw_day = function (day) {
        if (!this.get_day_class(day).includes('other')) {
            var day_wrapper = createElement('a', this.get_day_class(day));
            var day_name = createElement('div', 'day-name', "Today");
            var day_url = new URL(window.location);
            day_url.searchParams.set("d", day.format("YYYY-MM-DD"));

            if (day_wrapper.classList.contains("today")) {
                localStorage.umamiHomeURL = day_url;
            }
            else {
                day_name = createElement('div', 'day-name', day.locale('en').format('ddd'));
            }

            if (!day_wrapper.classList.contains("today") || !day_wrapper.classList.contains("selected")) {
                if (!day_wrapper.classList.contains("past")) {
                    day_wrapper.href = day_url;
                }
            }
            var day_number = createElement('div', 'day-number', day.format('D'));
            day_wrapper.appendChild(day_name);
            day_wrapper.appendChild(day_number);
            this.week.appendChild(day_wrapper);

        }
    };

    // A function to add a different class to "today"
    Calendar.prototype.get_day_class = function (day) {
        classes = ['day'];

        if (today.isSame(day, 'days')) {
            classes.push('today');
        }
        else if (today.diff(day) > 0) {
            classes.push('past');
        }

        if (url_date.isSame(day, 'days')) {
            classes.push('selected');
        }
        return classes.join(' ');
    };

    // A function to create html elements
    function createElement(tagName, className, innerText, dataAttributeName, dataAttributeValue) {
        var html_element = document.createElement(tagName);
        if (className) {
            html_element.className = className;
        }
        if (innerText) {
            html_element.innerText = html_element.textContent = innerText;
        }
        if (dataAttributeName && dataAttributeValue) {
            html_element.setAttribute(dataAttributeName, dataAttributeValue);
        }
        return html_element;
    }

    window.Calendar = Calendar;

}();

!function () {

    var calendar = new Calendar('#calendar');

}();