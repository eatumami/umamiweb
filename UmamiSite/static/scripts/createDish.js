$(document).ready(function () {
    $('.btn-info').tooltip({trigger: "hover"});

    $('#image').on('change', function () {
        readFile(this, $imageCropper);
    });

    const randomKey = `?x-eat-umami=${parseInt(Math.random() * 100000)}`;

    const dish_image = food_image || $(".dish-placeholder").data("image-src");
    const $imageCropper = $('#image-cropper').croppie({
        url: dish_image + randomKey,
        viewport: {
            width: 500,
            height: 360,
        },
        boundary: {
            width: 500,
            height: 360,
        },
        mouseWheelZoom: false,
        enableExif: true
    });


    function isValidDishForm() {
        const $dishError = $("#dish-error");
        const $inputName = $("input[name=name]");
        const $dishDescription = $('#description');

        $dishError.html('');
        let isValid = true;

        if (!$inputName.val()) {
            $dishError.append("*Dish needs a name<br>");
            isValid = false;
        }
        if (!$('#image').val() && !food_image) {
            $dishError.append("*Dish needs a photo<br>");
            isValid = false;
        }
        if (!$dishDescription.val()) {
            $dishError.append("*Dish needs a description<br>");
            isValid = false;
        }
        if (containsProfanities($inputName.val())) {
            $dishError.append("*Dish name contains banned word(s)<br>");
            isValid = false;
        }
        if (containsProfanities($dishDescription.val())) {
            $dishError.append("*Dish description contains banned word(s)<br>");
            isValid = false;
        }
        if (!isValid) {
            $('html, body').animate({scrollTop: '0px'}, 300);
        }
        return isValid;

    }

    $('#new-dish-form, #update-dish-form').submit(function (event) {
        event.preventDefault();
        const request_data = convertToJSON($(this).serializeArray());
        const isNewDish = $(this).is("#new-dish-form");
        $imageCropper.croppie('result', {
            type: 'canvas',
            quality: 0.7,
            format: "jpeg",
            size: {width: 800, height: 576}
        }).then(function (resp) {
            if (isValidDishForm()) {
                $(".loading").show();
                const ingredients = [];
                const tags = [];
                const options = [];
                const extras = [];

                $('#ingredients-list li').each(function () {
                    ingredients.push({'name': $(this).data('name')})
                });
                $('#tags-list li').each(function () {
                    tags.push({'name': $(this).data('name')})
                });
                $("#extras-list li").each(function () {
                    extras.push(
                        {
                            'name': $(this).data("name"),
                            'price': $(this).data("price")
                        }
                    )
                });
                $("#options-list li").each(function () {
                    options.push(
                        {
                            'name': $(this).data("name"),
                            'price': $(this).data("price")
                        }
                    )
                });

                request_data['image'] = resp;
                request_data['ingredients'] = ingredients;
                request_data['tags'] = tags;
                request_data['extras'] = extras;
                request_data['options'] = options;

                $.ajax({
                    headers: {"Authorization": `Token ${getCookie("umamiAuthToken")}`, "tz_offset": new Date().getTimezoneOffset()},
                    type: isNewDish ? "POST" : "PUT",
                    url: "/api/dish",
                    data: JSON.stringify(request_data),
                    contentType: "application/json",
                    success: function (data) {
                        window.location.href = `/my/dishes`;
                    }, error: function () {
                        $(".loading").hide();
                    }
                });
                return false;
            }
        });
    });
});


function containsProfanities(string) {
    return string.split(" ").every(function (word) {
        return word.toLowerCase().match(localStorage.getItem('profanities')) !== null;
    });
}

function readFile(input, $uploadCrop) {
    if (input.files && input.files[0]) {
        const reader = new FileReader();

        reader.onload = function (e) {
            $('.inner-wrapper').addClass('ready');
            $uploadCrop.croppie('bind', {
                url: e.target.result
            }).then(function () {
            });

        };

        reader.readAsDataURL(input.files[0]);
    } else {
        swal("Sorry - you're browser doesn't support the FileReader API");
    }
}

let cached_tags = [];
let cached_ingredients = [];
let cached_extras = [];

// constructs the suggestion engine
let tags_list = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.whitespace,
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        local: cached_tags,
        prefetch: {
            cache: false,
            url: '/api/tags',
            cacheKey: 'tags'
        },
        remote: {
            url: '/api/tags/%QUERY',
            wildcard: '%QUERY',
            transform: function (response) {
                for (var i = 0; i < response.length; i++) {
                    cached_tags.push(response[i]);
                }
                tags_list.clear();
                tags_list.initialize(true);
                return response;
            },
            rateLimitBy: 'throttle',
            rateLimitWait: 500
        },
    })
;


let ingredients_list = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.whitespace,
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    local: cached_ingredients,
    prefetch: {
        cache: false,
        url: '/api/ingredients',
        cacheKey: 'ingredients'
    },
    remote: {
        url: '/api/ingredients/%QUERY',
        wildcard: '%QUERY',
        transform: function (response) {
            for (var i = 0; i < response.length; i++) {
                cached_ingredients.push(response[i]);
            }
            ingredients_list.clear();
            ingredients_list.clearRemoteCache();
            ingredients_list.initialize(true);
            return response;
        },
        rateLimitBy: 'throttle',
        rateLimitWait: 500
    }
});


$('.add-ingredient').click(function () {
    addIngredient();
});

$('.add-tag').click(function () {
    addTag();
});

$('.add-extra').click(function () {
    $(this).html("Add extra");
    $("#extras-section").show();
    addExtra();
});

$('.add-option').click(function () {
    $(this).html("Add option");
    $("#options-section").show();
    addOption();
});


$("#ingredients-search")
    .keydown(function (e) {
        if (e.which === 13 || e.which === 188 || e.which === 186 || e.which === 220) {
            e.preventDefault();
            addIngredient();
        }
    })
    .typeahead({
        hint: false,
        highlight: true,
        minLength: 1,
    }, {
        name: 'ingredients',
        source: ingredients_list,
    })
    .bind('typeahead:selected', function (evt, item) {
        $("#ingredients-search").typeahead('val', item);
    });


$("#tags-search")
    .keydown(function (e) {
        if (e.which === 13 || e.which === 188 || e.which === 186 || e.which === 220) {
            e.preventDefault();
            addTag();
        }
    })
    .typeahead({
        hint: false,
        highlight: true,
        minLength: 1
    }, {
        name: 'tags',
        source: tags_list,
    })
    .bind('typeahead:selected', function (evt, item) {
        $("#tags-search").typeahead('val', item);
    });


$("#extra-name").keydown(function (e) {
    if (e.which === 13) {
        e.preventDefault();
        addExtra();
    }
});


$("#option-name")
    .keydown(function (e) {
        if (e.which === 13) {
            e.preventDefault();
            addOption();
        }
    });

function addIngredient() {
    //Use Regex to validate entry, ingredients can only be made up of words separated by a single space
    const re = /^([\w]+\s)*[\w]+$/;
    const $ingredientSearch = $("#ingredients-search");
    const ingredient = $ingredientSearch.val();
    const matches = ingredient.toLowerCase().match(localStorage.getItem('profanities')) || [];
    if (re.test(ingredient) && matches.length === 0) {
        const ingredientWrapper = createElement('li', 'list-entry', `${ingredient}`, {
            "data-name": ingredient,
        });
        $('#ingredients-list').append(ingredientWrapper);
        $ingredientSearch.typeahead('val', '');
    } else {
        throw 500;
    }
}

function addTag() {
    //Use Regex to validate entry, tags can only be made up of alphanumeric words/phrases separated by a single space
    const re = /^([\w\d]+\s)*[\w\d]+$/;
    const $tagSearch = $("#tags-search");
    const tag = $tagSearch.val();

    const matches = tag.toLowerCase().match(localStorage.getItem('profanities')) || [];
    if (re.test(tag) && matches.length === 0) {
        const tagWrapper = createElement('li', 'list-entry', `#${tag}`, {
            "data-name": tag,
        });
        $('#tags-list').append(tagWrapper);
        $tagSearch.typeahead('val', '');
    } else {
        throw 500;
    }
}

function addOption() {
    //Use Regex to validate entry, tags can only be made up of alphanumeric words/phrases separated by a single space
    const re = /^([\w\d]+\s)*[\w\d]+$/;
    const $optionName = $("#option-name");
    const $optionPrice = $("#option-price");
    const option = $optionName.val();
    const price = $optionPrice.val();

    if (price !== '') {
        const matches = option.toLowerCase().match(localStorage.getItem('profanities')) || [];
        if (re.test(option) && matches.length === 0) {
            const optionWrapper = createElement('li', 'new-dish-option-item', `-${option}: $${price}`, {
                "data-name": option,
                "data-price": price
            });
            $('#options-list').append(optionWrapper);
            $optionName.val('');
            $optionPrice.val(0)
        } else {
            throw 500;
        }
    } else {
        $('#offer-error').html('*Need option item price, if free put 0<br>')
    }

}

function addExtra() {
    //Use Regex to validate entry, tags can only be made up of alphanumeric words/phrases separated by a single space
    const re = /^([\w\d]+\s)*[\w\d]+$/;
    const $extraName = $("#extra-name");
    const extra = $extraName.val();
    const price = $("#extra-price").val();

    if (price !== '') {
        const matches = extra.toLowerCase().match(localStorage.getItem('profanities')) || [];
        if (re.test(extra) && matches.length === 0) {
            const extraWrapper = createElement('li', 'new-dish-extra-item', `-${extra}: $${price}`, {
                "data-name": extra,
                "data-price": price
            });

            $('#extras-list').append(extraWrapper);
            $extraName.typeahead('val', '');
        } else {
            throw 500;
        }
    } else {
        $('#offer-error').html('*Need extra item price, if free put 0<br>')
    }
}


$('#dish-form').on('mouseenter', 'li', function () {
    $(this).find('span').wrapInner('<strike>');
})
    .on('mouseleave', 'li', function () {
        $(this).find('strike').contents().unwrap();
    })
    .on('click', 'li', function () {
        $(this).remove();
    });
