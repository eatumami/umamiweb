$(function () {
    $("[data-hide]").on("click", function () {
        $("." + $(this).attr("data-hide")).fadeOut()
    })
});

$("#cart-button").on("click", function () {
    openShoppingCart();
});

$("#close-cart-btn").on("click", function () {
    closeShoppingCart();
});

function openShoppingCart() {
    $("#close-cart-btn").show();
    onShoppingCartOpen();
    if (window.innerWidth >= 470) {
        $("#shopping-cart").css("width", "470px");
    } else {
        $("#shopping-cart").css("width", "100%");
    }

    $("#cart-button").hide();
}

function closeShoppingCart() {
    $("#close-cart-btn").hide();
    $("#shopping-cart").css("width", "0");
    $("#cart-button").show();
}

function onShoppingCartOpen() {
    if (localStorage.cartFoodItems && JSON.parse(localStorage.cartFoodItems).cartItems.length > 0) {
        var template = $("#cart-food-template").html();
        var cartItems = JSON.parse(localStorage.cartFoodItems).cartItems;
        var markupItems = [];
        var $cartTotalPrice = $("#cart-total-price").find(".numeric-value");

        $cartTotalPrice.html((parseFloat(0).toFixed(2)));
        if (cartItems && cartItems.length > 0) {
            $("#sad-face").hide();
            cartItems.forEach(function (cart_item) {
                var templateClone = $(template).clone();
                var itemQty = Number(cart_item.qty);
                var subTotal = Number(cart_item.perUnitTotal) * itemQty;
                var totalPrice = Number($cartTotalPrice.html()) + subTotal;
                const kitchenLink = `${cart_item.offer.kitchen.social_url}`;
                var extraTotal = 0;

                $cartTotalPrice.html((totalPrice).toFixed(2));

                const extras_container = $('<div/>');
                cart_item.extras.forEach(function (extra) {
                    extraTotal += Number(extra.price);
                    extras_container.append(
                        `<div class='extra-item' data-price='${extra.price}'>+${extra.name}`
                    );
                });
                templateClone.find(".c-extra-items").html(extras_container);

                let readyTime;
                let deliveryOrPickup;

                if (cart_item.delivery) {
                    deliveryOrPickup = "Delivery";
                    readyTime = parse_time_range(cart_item.offer.deliver_time.start, cart_item.offer.deliver_time.end);
                } else {
                    deliveryOrPickup = "Pickup";
                    readyTime = parse_time_range(cart_item.offer.pickup_time.start, cart_item.offer.pickup_time.end);
                }

                templateClone.find("h3").html(cart_item.offer.name);
                templateClone.find(".c-ready-date").html(parse_date(cart_item.date));
                templateClone.find(".c-ready-time").html(` &#183; ${readyTime}`);
                templateClone.find(".c-delivery-or-pickup").html(deliveryOrPickup);
                templateClone.find(".c-food-option").html(cart_item.option ? cart_item.option['name'] : '');
                templateClone.find(".c-food-image").attr("src", cart_item.offer.dishes[0].image);
                templateClone.find(".c-qty--input").val(Number(cart_item.qty));
                templateClone.find(".c-qty--input").attr('max', cart_item.offer.available);
                templateClone.find(".sub-total .numeric-value").html(subTotal.toFixed(2));
                templateClone.find(".c-kitchen-link").attr("href", kitchenLink);
                templateClone.find("*").attr("data-cart-id", cart_item.cartId);
                templateClone.find("*").attr("data-uuid", cart_item.uuid);
                templateClone.attr("data-uuid", cart_item.uuid);
                templateClone.attr("data-food-id", cart_item.offer.id);
                markupItems.push(templateClone);
            });
            $("#cart-content").html(markupItems);
        }
    } else {
        $("#cart-total-price .numeric-value").html((parseFloat(0).toFixed(2)));
    }
}

function decrementCartItemStorage(uuid) {
    var foodItems = JSON.parse(localStorage.cartFoodItems);
    var i = foodItems.cartItems.length;
    var $cartIndicator = $("#cart-indicator");
    while (i--) {
        if (foodItems.cartItems[i].uuid === uuid) {
            if (foodItems.cartItems[i].qty > 1) {
                foodItems.cartItems[i].qty--;
                var foodItem = foodItems.cartItems[i];
            } else {
                foodItems.cartItems[i].qty--;
                var foodItem = foodItems.cartItems.splice(i, 1)[0];
            }
            if (foodItems.cartItems.length === 0) {
                $cartIndicator.hide();
                $(".c-delivery-pickup").html('');
                $(".c-kitchen-link").hide();
                $("#sad-face").show();
            }
            localStorage.cartFoodItems = JSON.stringify(foodItems);
            $cartIndicator.html(getTotalCartQty());
            return foodItem;
        }
    }

}

function incrementCartItemStorage(uuid) {
    var foodItems = JSON.parse(localStorage.cartFoodItems);
    var i = foodItems.cartItems.length;
    while (i--) {
        if (foodItems.cartItems[i].uuid === uuid) {
            foodItems.cartItems[i].qty++;
            localStorage.cartFoodItems = JSON.stringify(foodItems);
            $("#cart-indicator").html(getTotalCartQty());
            return foodItems.cartItems[i];
        }
    }
}


$("#cart-content").on("click", "span.c-qty--plus", function () {
    var uuid = $(this).data('uuid');

    var concatenatedUUID = `[data-uuid=${uuid}]`;
    var $qty = $(".c-qty--input" + concatenatedUUID);
    var qty = $qty.val();
    if (qty < parseInt($qty.attr('max'))) {
        var foodItem = incrementCartItemStorage(uuid);
        var $subTotal = $(".sub-total" + concatenatedUUID + " .numeric-value");
        $qty.get(0).value++;
        var subTotal = parseFloat($subTotal.html());
        var $totalPriceSelector = $("#cart-total-price").find(".numeric-value");
        var totalPrice = Number($totalPriceSelector.html());
        $subTotal.html((subTotal + foodItem.perUnitTotal).toFixed(2));
        $totalPriceSelector.html((totalPrice + foodItem.perUnitTotal).toFixed(2));
    }
}).on("click", "span.c-qty--minus", function () {
    var uuid = $(this).data('uuid');
    var concatenatedUUID = `[data-uuid=${uuid}]`;
    var foodItem = decrementCartItemStorage(uuid);
    var qty = foodItem.qty;
    var $subTotal = $(".sub-total" + concatenatedUUID + " .numeric-value");
    var subTotal = parseFloat($subTotal.html());
    var $totalPriceSelector = $("#cart-total-price").find(".numeric-value");
    var totalPrice = Number($totalPriceSelector.html());
    $subTotal.html((subTotal - foodItem.perUnitTotal).toFixed(2));
    if (qty > 0) {
        $(".c-qty--input" + concatenatedUUID).get(0).value--;
        $totalPriceSelector.html((totalPrice - foodItem.perUnitTotal).toFixed(2));
    } else {
        $(".cart-food-item" + concatenatedUUID).empty();
        $totalPriceSelector.html((totalPrice - foodItem.perUnitTotal).toFixed(2));
    }
});


$("#cart-wrapper").on('click', ".checkout-button", function () {
    //TODO: some validation here
    if (localStorage.cartFoodItems) {
        if (JSON.parse(localStorage.cartFoodItems).cartItems.length > 0) {
            window.location.href = '/checkout';
        }
    }
});
