$(function () {
    let offerTimer;
    $("body").on("mouseenter", ".current-food-item", function () {
        "use strict";
        const $selector = $(this);
        slideImage($selector);
        offerTimer = setInterval(function () {
            $selector.find(".hs-wrapper img:first")
                .show()
                .next()
                .hide()
                .end()
                .appendTo($selector.find('.hs-wrapper'));
        }, 1200)
    }).on("mouseleave", ".current-food-item", function () {
        "use strict";
        clearInterval(offerTimer);
    }).on("click", ".nav a", function () {
        $(".nav").find(".active").removeClass("active");
        $(this).addClass("active");
    }).popover(popoverSettings);

    $('.kitchen-rating').rateYo({
        starWidth: "25px",
        rating: parseFloat($('.kitchen-rating').data("rating")),
        ratedFill: "#f5e258",
        normalFill: "lightgrey",
        readOnly: true
    });
});

function loadMoreCurrentOffers() {
    $(".current-offers").show();
    $(".load-more-button").remove();
}

function navigateOffer(param) {
    const socialUrl = $(param).data("social-url");
    const offerDate = $(param).data("offer-date");
    window.location.href = `${socialUrl}?d=${offerDate}`;
}

function getCurrentAddressForUrl() {
    return `address=${localStorage.userAddress}&lon=${localStorage.userLongitude}&lat=${localStorage.userLatitude}`;
}

function slideImage($selector) {
    $selector.find(".hs-wrapper img:first")
        .show()
        .next()
        .hide()
        .end()
        .appendTo($selector.find('.hs-wrapper'));
}

function follow_unfollow_user(kitchenId, param) {
    $.ajax({
        headers: {"Authorization": `Token ${getCookie("umamiAuthToken")}`, "tz_offset": new Date().getTimezoneOffset()},
        type: "POST",
        url: "/api/" + param,
        dataType: 'json',
        data: {'id': kitchenId},
        success: function (data) {
            toggleFollowBtn();
        },
        error: function (data) {
            if (data.status === 401) {
                window.location.href = '/accounts/login';
            }
        }
    });
}

function toggleFollowBtn() {
    const $followBtn = $("#follow-btn");
    if ($followBtn.data('following') === "0") {
        $followBtn.html("Following");
        $followBtn.data("following", "1")
    } else {
        $followBtn.html("Follow");
        $followBtn.data("following", "0")
    }
}

$("body").on("click", "#follow-btn", function () {
    if ($(this).data('following') === "0") {
        follow($(this).data('kitchen-id'));
    } else {
        unfollow($(this).data('kitchen-id'));
    }
});

function follow(kitchenId) {
    follow_unfollow_user(kitchenId, 'follow');
}

function unfollow(kitchenId) {
    follow_unfollow_user(kitchenId, 'unfollow');
}

