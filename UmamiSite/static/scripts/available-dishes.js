/**
 * Created by ericwang on 2019-02-11.
 */
const selectedIds = [];
const bundleSelectOrders = ["1", "2", "3"];

function fillRatings() {
    const $dishRatings = $(".dish-rating-cr");
    for (let i = 0; i < $dishRatings.length; i++) {
        $($dishRatings[i]).rateYo("rating", $($dishRatings[i]).data("rating"));
    }
}

function toggleDishSelect($selector) {
    "use strict";
    const dishId = $selector.data('dish-id');
    if ($selector.hasClass("selected-dish")) {
        bundleSelectOrders.push($selector.find("span").html());
        bundleSelectOrders.sort();

        $selector.removeClass("selected-dish");
        $selector.find("span").html("<span>BUNDLE</span>");
        selectedIds.splice(selectedIds.indexOf(dishId), 1);

        if ($(".selected-dish").length === 0) {
            $("#sell-dishes-btn").hide();
        }
    }
    else {
        if ($(".selected-dish").length + 1 > 3) {
            showAlert();
        }
        else {
            const dishOrder = parseInt(bundleSelectOrders.shift());
            $selector.addClass("selected-dish");
            selectedIds.splice(dishOrder - 1, 0, dishId);
            $selector.find("span").html(dishOrder);
            $("#sell-dishes-btn").show();
        }
    }
}

function showAlert() {
    $(".alert-too-many-dishes").fadeTo(3000, 500).slideUp(500, function () {
        $(".alert-too-many-dishes").slideUp(500);
    });
}

$(function () {
    "use strict";
    $('.dish-selector').tooltip({trigger: "hover"});

    $('.dish-rating-cr').rateYo({
        starWidth: "15px",
        ratedFill: "#f5e258",
        normalFill: "lightgrey",
        readOnly: true
    });
    fillRatings();

    $('.dish-selector').on("click", function () {
        if ($(".selected-dish").length >= 0) {
            toggleDishSelect($(this));
        }
    });

    $("#sell-dishes-btn").on("click", function () {
        const date_in_url = new URL(window.location).searchParams.get("d") || "";
        window.location.href = `/offer/new/${selectedIds.join(",")}?d=${date_in_url}`
    });

});


