// Angularjs
"use strict";

if (typeof umamiWeb === "undefined") {
    var umamiWeb = angular.module('umamiWeb', []);
    umamiWeb.config(function ($interpolateProvider, $locationProvider, $compileProvider) {
        $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|javascript):/);
        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
    });
}

umamiWeb.controller('reviewsPaginationCtrl', function ($scope) {
    $scope.all_reviews = [];
    $scope.reviews = [];
    $scope.getMore = function () {
        if ($scope.all_reviews.length > 3) {
            $scope.reviews = $scope.reviews.concat($scope.all_reviews.slice(0, 3));
            $scope.all_reviews = $scope.all_reviews.slice(3, $scope.all_reviews.length)
        } else if ($scope.all_reviews.length > 0) {
            $scope.reviews = $scope.reviews.concat($scope.all_reviews.slice(0, $scope.all_reviews.length));
            $scope.all_reviews = [];
            $('.review-pagination').html('All reviews loaded');
        }
    };
    $scope.init = function (all_reviews) {
        $scope.all_reviews = all_reviews;
        $scope.reviews = $scope.reviews.concat($scope.all_reviews.slice(0, 3));
        $scope.all_reviews = $scope.all_reviews.slice(3, $scope.all_reviews.length)
    };
    $scope.$watch('all_reviews', function () {
        if ($scope.all_reviews.length === 0) {
            $('.review-pagination').html('All reviews loaded');
        }
    })
});

umamiWeb.directive("rateStar", function () {
    return {
        restrict: "A",
        link: function (scope, ele, attr) {
            let starWidth = "15px";
            if (attr.starWidth) {
                starWidth = attr.starWidth;
            }
            $(ele).rateYo({
                rating: attr.rateStar,
                starWidth: starWidth,
                ratedFill: "#f5e258",
                normalFill: "lightgrey",
                readOnly: true
            });
        }
    };
});


function refreshTotal() {
    const qtyValue = Number($(".d-qty--input").get(0).value);
    const itemPrice = Number($("#item-price").data("item-price"));

    let optionTotal = Number($('input[name=option]:checked', '#food-form').data("price")) || 0;
    let extraTotal = 0;
    $('.extra-select :checked').each(function () {
        extraTotal += Number($(this).data('extra-price'));
    });


    const singleTotal = optionTotal + extraTotal + itemPrice;

    $("#dish-total-price").html((singleTotal * qtyValue).toFixed(2));
}

function decrementDishQuantity() {
    const $qtyInput = $(".d-qty--input");
    let qtyValue = $qtyInput.get(0).value;
    if (qtyValue > 1) {
        $qtyInput.get(0).value--;
    }
    refreshTotal();
}


function incrementDishQuantity() {
    const $qtyInput = $(".d-qty--input");
    let qtyValue = $qtyInput.get(0).value;
    const offerMax = $qtyInput.attr('max');
    if (Number(qtyValue) < Number(offerMax)) {
        $qtyInput.get(0).value++;
    }
    refreshTotal();
}


$('#food-wrapper').on('change', '.extra-checkbox, .option-selector', function () {
    refreshTotal();
});

function addOrUpdateOfferInCart(cart_item) {
    const cart = JSON.parse(localStorage.cartFoodItems);
    if (cart.cartItems) {
        let i = cart.cartItems.length;
        while (i--) {
            if (_.isEqual(cart.cartItems[i]["cartId"], cart_item["cartId"]) && _.isEqual(cart.cartItems[i]["option"], cart_item["option"]) && _.isEqual(cart.cartItems[i]["extras"], cart_item["extras"]) && cart.cartItems[i]["delivery"] === cart_item.delivery) {
                const newQty = Number(cart.cartItems[i]["qty"]) + Number(cart_item["qty"]);
                if (newQty <= cart_item.offer.available) {
                    cart.cartItems[i]["qty"] = newQty;
                    localStorage.cartFoodItems = JSON.stringify(cart);
                } else {
                    $("#qty-exceeded-alert").show();
                }
                return;
            }
        }
    }
    cart.cartItems.push(cart_item);
    localStorage.cartFoodItems = JSON.stringify(cart);
}

$("#food-form").on("submit", function () {
    const $offer = $(this);
    addOfferToCart($offer);
    return false;
});

$(".availability-date-picker").on("change", function () {
    const selectedDate = $('.availability-date-picker option:selected').val();
    const match = availables.find(available => available.date === selectedDate);
    $(".d-qty--input").get(0).value = 1;
    refreshTotal();
    if (match) {
        if (match.available <= 0) {
            $("#add-to-cart").hide();
            $("#food-available").html(0);
            $(".sold-out-text").show();
        } else {
            $("#add-to-cart").show();
            $(".sold-out-text").hide();
            $("#food-available").html(match.available);
            $("#add-to-cart .d-qty--input").prop("max", match.available)
        }
    } else {
        $("#add-to-cart").show();
        $(".sold-out-text").hide();
        $("#food-available").html(total_available);
        $("#add-to-cart .d-qty--input").prop("max", total_available)
    }

});

function addOfferToCart($offer) {
    const form = $offer.serializeArray();
    const foodpage_offers = $('#foodpage-offers').html();
    const offer = $('#offer').html();
    const uuid = Math.floor(Math.random() * 100000);
    const qty = parseInt($(".d-qty--input").get(0).value);
    const food_type = $offer.parent().data("food-type");

    let date;
    let offer_for_cart;

    if (foodpage_offers) {
        if (food_type === "single") {
            offer_for_cart = JSON.parse(foodpage_offers).singles.find(_offer => _offer.id === $offer.parent().data("food-id"));
        } else {
            offer_for_cart = JSON.parse(foodpage_offers).bundles.find(_offer => _offer.id === $offer.parent().data("food-id"));
        }
    } else {
        offer_for_cart = JSON.parse(offer);
    }

    const [option, optionTotal] = getOfferOption(form);
    const [extras, extraTotal] = getOfferExtras();

    if ($(".availability-date-picker").length) {
        date = $(".availability-date-picker option:selected").val();
    } else {
        date = localStorage.overlayReadyDate || localStorage.userReadyDate;
    }
    const cart_item = JSON.parse(JSON.stringify({
        "offer": offer_for_cart,
        "date": date,
        "qty": qty,
        "extras": extras,
        "option": option,
        "cartId": `${food_type}-${offer_for_cart.id}`,
        "delivery": isDelivery(form),
        "uuid": uuid,
        "perUnitTotal": extraTotal + offer_for_cart.price + optionTotal,
    }));


    // First time adding to cart
    if (!localStorage.cartFoodItems || JSON.parse(localStorage.cartFoodItems).cartItems.length === 0) {
        const cartFoodItems = {'cartItems': [cart_item], "expires": moment().add(3, "hours").unix()};
        localStorage.cartFoodItems = JSON.stringify(cartFoodItems);
    } else {
        addOrUpdateOfferInCart(cart_item);
    }
    if (getTotalCartQty()) {
        $("#cart-indicator").html(getTotalCartQty()).show();
    }

    openShoppingCart();
    if ($(".food-item").length) {
        closeOverlay();
    }
}


function isDelivery(form) {
    let delivery = false;
    for (let i = 0; i < form.length; i++) {
        if (form[i].name === "transportation-opt") {
            delivery = form[i].value === "delivery";
        }
    }
    return delivery;
}

function getOfferOption(form) {
    let option;
    let optionTotal = 0;
    for (let i = 0; i < form.length; i++) {
        if (form[i].name === "option") {
            option = JSON.parse(form[i].value);
            optionTotal = Number(option["price"]);
        }
    }
    return [option, optionTotal];
}


function getOfferExtras() {
    const extras = [];
    let extraTotal = 0;

    $('.extra-select :checked').each(function () {
        extraTotal += Number($(this).data('extra-price'));
        extras.push(
            {
                'name': $(this).data('extra-name'),
                'price': $(this).data('extra-price'),
            })
    });
    return [extras, extraTotal];
}


$(function () {
    const url_date = new URL(window.location).searchParams.get("d");
    if (url_date) {
        $(".availability-date-picker").val(url_date);
    }
});