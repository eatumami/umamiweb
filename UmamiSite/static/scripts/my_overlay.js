var overlay_state = 0;
var back_to_dish_clicked = 0;
var popstate = 0;
var overlay_opened;
var foodpage_url;
var last_opened_foodId;
var last_opened_foodURL;
var last_opened_is;
var chef_trigger;
var food_trigger;

$(function () {
    foodpage_url = `/${window.location.href.replace(/^(?:\/\/|[^\/]+)*\//, "")}`;

    $(".food-container").on("click", ".food-item", function () {
        if ($(this).hasClass("no_click")) {
            $(this).attr("data-toggle", "");
            $(this).removeClass("no_click");
            return;
        } else {
            $(this).attr("data-toggle", "modal");
        }
    });

    function makeOverlayURL(id, overlayType) {
        let url = `/${overlayType}?id=${id}`;
        // if (getUserAddress()) {
        //     url += `&address=${getUserAddress()}`;
        // }
        // if (getUserLatitude() && getUserLongitude()) {
        //     url += `&lon=${getUserLongitude()}&lat=${getUserLatitude()}`;
        // }
        return url;
    }

    $('body').on('click', '.overlay-past-dishes .past-food-item, .overlay-current-dishes .select-kitchen-offer, .food-item, .bundle-select', function (e) {
        closeOverlay();
        food_trigger = $(this);
        if (!food_trigger.is('.past-food-item') &&
            !food_trigger.is('.select-kitchen-offer') &&
            !food_trigger.is('.bundle-select') &&
            last_opened_is === "offer") {
            last_opened_is = "offer";
            window.history.pushState("", "Offer", food_trigger.data("social-url"));
        } else if (food_trigger.is('.past-food-item')) {
            last_opened_is = "dish";
            window.history.pushState("", "Dish",food_trigger.data("social-url"));
        } else if (last_opened_is === "dish" && !food_trigger.is('.food-item') && !food_trigger.is('.bundle-select')) {
            last_opened_is = "dish";
            window.history.pushState("", "Dish", food_trigger.data("social-url"));
        } else if (food_trigger.is('.bundle-select')) {
            last_opened_is = "bundle";
            window.history.pushState("", "Bundle", food_trigger.data("social-url"));
        } else {
            last_opened_is = "offer";
            window.history.pushState("", "Dish", food_trigger.data("social-url"));
        }
    }).on('click', '#overlay-made-by', function (e) {
        e.stopPropagation();
        closeOverlay();
        chef_trigger = $(this);
        if (!chef_trigger.is('#overlay-made-by')) {
            window.history.pushState("", "Kitchen", chef_trigger.data("social-url"));
        } else {
            window.history.pushState("", "Kitchen", chef_trigger.data("social-url"));
        }
        $('#kitchenModal').modal('show');
    }).on('click', '#back-to-dish', function () {
        back_to_dish_clicked = 1;
        closeOverlay();
        $('#foodModal').modal('show');
    });


    $('footer').on('show.bs.modal', '#foodModal', function () {
        var $foodDialog = $("#food-dialog");
        $foodDialog.hide();
        if (food_trigger.is('.past-food-item') || last_opened_is === "dish") {
            loadDishOverlay($foodDialog, food_trigger);
        } else if (last_opened_is === "offer" && !food_trigger.data("is-bundle")) {
            loadOfferOverlay($foodDialog, food_trigger);
        } else {
            loadBundleOverlay($foodDialog, food_trigger);
        }
        onModalOpen();

    }).on('show.bs.modal', '#kitchenModal', function () {
        var $chefDialog = $("#chef-dialog");
        $chefDialog.hide();
        loadKitchenOverlay($chefDialog, chef_trigger);
        onModalOpen();
    }).on('show.bs.modal', '#loginModal', function () {
        loadSignInOverlay();
        onModalOpen();
    });


    $('#foodModal, #kitchenModal, #loginModal').on('hidden.bs.modal', function (e) {
        overlay_state = 0;
        if (back_to_dish_clicked) {
            const back_to = makeOverlayURL(last_opened_foodId, last_opened_is);
            window.history.replaceState("", "Umami Offers", back_to);
            back_to_dish_clicked = 0;
        } else {
            window.history.replaceState("", "Umami Offers", foodpage_url);
        }
        $('#back-to-dish').remove();
        popstate = 0;
    });
});

$(window).on("popstate", function () {
    if (overlay_opened) {
        popstate = 1;
        if (overlay_state) {
            closeOverlay();
        } else {
            if (overlay_opened === "offer" || overlay_opened === "bundle" || overlay_opened === "dish") {
                $('#foodModal').modal('show');
            } else if (overlay_opened === "kitchen") {
                $('#kitchenModal').modal('show');
            }
        }
    } else {
        location.reload();
    }
});

function resizeOverlay() {
    const $overlayContent = $(".overlay-content");
    let imageHeight;
    let bodyHeight = window.innerHeight;
    $(".overlay-wrapper").height(bodyHeight - 15);
    let overlayHeight = $(".overlay-dish").height();
    if (bodyHeight > 400) {
        imageHeight = 240;
    }
    if (bodyHeight > 880) {
        imageHeight = 320;
    }
    $(".carousel-inner").css("max-height", (overlayHeight - imageHeight));
    if ($("#add-to-cart").length) {
        $overlayContent.css("max-height", (overlayHeight - imageHeight));
    } else {
        $overlayContent.css("max-height", (overlayHeight - imageHeight + 75));
    }
    $overlayContent.css("width", "700");
}

function onModalOpen() {
    closeShoppingCart();
    resizeOverlay();
    overlay_state = 1;
    popstate = 0;

    $('body').bind('touchmove', function (e) {
        e.preventDefault()
    });
}

var templateMap = {
    "offer": "offer-template",
    "dish": "dish-template",
    "bundle": "bundle-template"
};

function loadOfferOverlay(wrap, trigger) {
    loadOverlay(wrap, trigger, "offer");
}

function loadBundleOverlay(wrap, trigger) {
    loadOverlay(wrap, trigger, "bundle");
}

function loadDishOverlay(wrap, trigger) {
    loadOverlay(wrap, trigger, "dish");
}

function loadOverlay(wrap, trigger, opened) {
    overlay_opened = opened;
    last_opened_foodId = trigger.data("food-id");
    last_opened_foodURL = trigger.data("food-url");

    if (food_trigger.is('.select-kitchen-offer')) {
        localStorage.overlayReadyDate = trigger.data("offer-date");
    }
    const templateClone = $($("#" + templateMap[opened]).html()).clone();
    templateClone.attr({"data-food-id": last_opened_foodId, "data-food-url": last_opened_foodURL});

    wrap.html(templateClone);
    $.getScript($("#dishScript").prop("src"));
    setOverlayCtrl(wrap);
}


function loadKitchenOverlay(wrap, trigger) {
    overlay_opened = "kitchen";
    var templateClone = $($("#kitchen-template").html()).clone();
    templateClone.attr({"data-kitchen-url": trigger.data("kitchen-url")});
    wrap.html(templateClone);
    $('.overlay-chef-rating').rateYo({
        starWidth: "25px",
        ratedFill: "#f5e258",
        normalFill: "lightgrey",
        readOnly: true
    });
    if (trigger.is('#overlay-made-by')) {
        $('#kitchenModal').append("<div id='back-to-dish' class='back-button glyphicon glyphicon-chevron-left' " +
            "style='cursor:pointer' data-food-url=" + last_opened_foodId + "></div>");
    }
    $('.overlay-kitchen').popover(popoverSettings);
    setOverlayCtrl(wrap);
}

function closeOverlay() {
    $('#foodModal').modal("hide");
    $('#kitchenModal').modal("hide");
    $('#loginModal').modal("hide");
    localStorage.removeItem("overlayReadyDate");
}

$("body").on("click touchend touchstart", ".close-overlay", function (e) {
    e.stopPropagation();
    if (e.target === this) {
        closeOverlay();
    }
    return false;
});

$("#foodModal").on("click touchend touchstart", ".overlay-dish", function (e) {
    e.stopPropagation();
    if (e.target === this) {
        closeOverlay();
    }
    return false;
});
