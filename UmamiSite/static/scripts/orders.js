$(".write-review").on("submit", function () {
    const rating = $(this).find(".user-rating").rateYo("rating");
    if (rating > 0) {
        const text = $(this).find(".user-text").val();
        const matches = text.toLowerCase().match(localStorage.getItem('profanities')) || [];
        if (matches.length > 0) {
            alert("Inappropriate word detected: " + matches[0]);
        } else {
            const dishId = $(this).data("dish-id");
            ajaxUserReview(dishId, {'text': text, 'rating': rating});
            showUserReview($(this).parent(), text, rating);
        }
    } else {
        alert("Please give this dish a rating")
    }
    return false;
});

function showUserReview($item, text, rating) {
    const $itemReview = $item.find(".item-review");
    const $itemRating = $itemReview.find(".readonly-item-rating");
    $item.find(".edit-existing-review-button").show();
    $itemReview.show();

    $itemRating.rateYo("rating", rating);

    $itemReview.find("p").html(text);

    $itemReview.show();

    $item.parent().find(".write-review").hide();
}

function ajaxUserReview(dishId, data) {
    $.ajax({
        headers: {"Authorization": `Token ${getCookie("umamiAuthToken")}`, "tz_offset": new Date().getTimezoneOffset()},
        type: "POST",
        url: `/api/review/${dishId}`,
        data: JSON.stringify(data),
        contentType: "application/json",
        success: function (response) {
        }
    });
}


function editReview(e) {
    $(e).parent().parent().find(".item-review").hide();
    $(e).parent().parent().find(".write-review").show();
}

$(function () {
    $('.user-rating').rateYo({
        starWidth: "25px",
        ratedFill: "#f5e258",
        normalFill: "lightgrey",
        halfStar: true
    });
    fillItemRatings();
});


function refundOrder(orderId, refundMax) {
    const data = {
        "order_id": orderId
    };
    const $modalTemplate = $($("#refund-template").html()).clone();
    $modalTemplate.find(".refund-amount").attr("max", refundMax).attr("value", refundMax);
    BootstrapDialog.show({
        title: 'Refund Order',
        message: $modalTemplate.prop("outerHTML"),
        closeByBackdrop: false,
        buttons: [{
            label: 'Confirm',
            cssClass: 'btn-primary',
            autospin: true,
            action: function (dialogRef) {
                dialogRef.enableButtons(false);
                dialogRef.setClosable(false);
                const $refundForm = $("#refund-form");
                const refundAmount = parseFloat($refundForm.find(".refund-amount").val());

                if (refundAmount && refundAmount <= refundMax && refundAmount > 1) {
                    data['amount'] = refundAmount;
                    $.ajax({
                        headers: {
                            "Authorization": `Token ${getCookie("umamiAuthToken")}`,
                            "tz_offset": new Date().getTimezoneOffset()
                        },
                        type: "POST",
                        url: `/api/refund`,
                        data: JSON.stringify(data),
                        contentType: "application/json",
                        success: function (response) {
                            window.location.reload();
                        },
                        error: function (data) {
                            dialogRef.close();
                        }
                    });
                } else {
                    dialogRef.enableButtons(true);
                    dialogRef.setClosable(true);
                    $refundForm.find(".refund-errors").show();
                    setInterval(function () {
                        $('.modal-dialog span.icon-spin').removeClass('glyphicon-asterisk icon-spin');
                    }, 500);
                }
            }
        }],
    });
}

function cancelOrder(orderId) {
    const data = {
        "order_id": orderId
    };
    const $modalTemplate = $($("#cancel-template").html()).clone();
    BootstrapDialog.show({
        title: 'Refund Order',
        message: $modalTemplate.prop("outerHTML"),
        closeByBackdrop: false,
        buttons: [{
            label: 'Confirm',
            cssClass: 'btn-primary',
            autospin: true,
            action: function (dialogRef) {
                dialogRef.enableButtons(false);
                dialogRef.setClosable(false);
                $.ajax({
                    headers: {
                        "Authorization": `Token ${getCookie("umamiAuthToken")}`,
                        "tz_offset": new Date().getTimezoneOffset()
                    },
                    type: "POST",
                    url: '/api/cancel',
                    data: JSON.stringify(data),
                    contentType: "application/json",
                    success: function (response) {
                        window.location.reload();
                    },
                    error: function (data) {
                        dialogRef.close();
                    }
                });
            }
        }],
    });
}


function fillItemRatings() {
    const $itemRatings = $(".readonly-item-rating");
    let i = $itemRatings.length;
    while (i--) {
        $($itemRatings[i]).rateYo({
            starWidth: "25px",
            rating: $($itemRatings[i]).data("rating"),
            ratedFill: "#f5e258",
            normalFill: "lightgrey",
            readOnly: true
        });
    }
}