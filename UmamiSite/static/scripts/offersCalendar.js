/**
 * Created by ericwang on 2019-01-31.
 */
/**
 * Created by lenafaure on 20/09/2017.
 */
let week_delta = 0;
let myCalendar;

if (performance.navigation.type === 2) {
    location.reload(true);
}

const today = moment();

function Calendar(selector, time_slots) {
    this.el = document.querySelector(selector);
    this.time_slots = time_slots;
    this.offers_copy = jQuery.extend(true, {}, time_slots);
    this.current = moment().startOf('week');
    this.selection = JSON.parse(localStorage.getItem('availablities')) || [];
    this.draw_calendar();
}


Calendar.prototype.draw_calendar = function () {
    // Draw Header
    this.draw_header();

    // Draw Week
    this.draw_week();
}

Calendar.prototype.draw_header = function () {
    // Refer to Calendar Object with "self" when "this" refers to an event
    const self = this;

    if (!this.header) {
        // Create header elements
        this.header = createElement('div', 'header');
        this.title = createElement('h1');

        const right = createElement('div', 'right');
        right.addEventListener('click', function () {
            self.next_week();
        });

        const left = createElement('div', 'left');
        left.addEventListener('click', function () {
            self.prev_week();
        });

        // Append header elements to Calendar
        this.header.appendChild(this.title);
        this.header.appendChild(right);
        this.header.appendChild(left);
        this.el.appendChild(this.header);
    }

    this.title.innerHTML = "Week of " + this.current.startOf('week').format("MMMM Do");
}

Calendar.prototype.draw_week = function () {
    const self = this;
    if (this.week) {
        this.old_week = this.week;
        this.old_week.className = 'week out ' + (self.next ? 'next' : 'prev');
        self.old_week.parentNode.removeChild(self.old_week);
        self.week = createElement('div', 'week');
        self.current_week();
        self.el.appendChild(self.week);
        window.setTimeout(function () {
            self.week.className = 'week in ' + (self.next ? 'next' : 'prev');
        }, 16);
    } else {
        this.week = createElement('div', 'week');
        this.el.appendChild(this.week);
        this.current_week();
        this.week.className = 'week current';
    }
    if (!isCalendarDirty()) {
        hideSave();
    }
    hideCopyTo();
}

Calendar.prototype.current_week = function () {
    const clone = this.current.clone();
    while (clone.week() === this.current.week()) {
        this.draw_day(clone);
        clone.add(1, 'days');
    }
}

Calendar.prototype.next_week = function () {
    if (week_delta < 2) {
        this.current.add(1, 'weeks');
        this.next = true;
        this.draw_calendar();
        week_delta += 1;
        hideCopyTo();
    }
};

Calendar.prototype.prev_week = function () {
    if (week_delta > -10) {
        this.current.add(-1, 'weeks');
        this.next = false;
        this.draw_calendar();
        week_delta -= 1;
        hideCopyTo();

    }
};

Calendar.prototype.draw_day = function (day) {
    if (!this.get_day_class(day).includes('other')) {
        const day_wrapper = createElement('div', this.get_day_class(day));
        const day_name = createElement('div', 'day-name', day.locale('fr').format('ddd'));
        const day_number = createElement('div', 'day-number', day.format('DD'));
        const day_month = createElement('div', 'day-month', day.locale('fr').format('MMM'));
        let day_slot = createElement('div', 'day-slot', '', {
            'data-date': day.format("YYYY-MM-DD"),
            'id': day.format("YYYY-MM-DD"),
        });
        if (!day_wrapper.classList.contains("past")) {
            day_slot = createElement('div', 'day-slot', '', {
                'data-date': day.format("YYYY-MM-DD"),
                'id': day.format("YYYY-MM-DD"),
            }, {
                'drop': drop,
                'dragover': allowDrop,
            });
            const day_adder = createElement('a', 'day-adder glyphicon glyphicon-plus', "", {
                "href": `/my/dishes?d=${day.format("YYYY-MM-DD")}`,
            });
            day_wrapper.appendChild(day_adder);
        }
        this.draw_time_slot(day, day_slot);

        day_wrapper.appendChild(day_name);
        day_wrapper.appendChild(day_number);
        day_wrapper.appendChild(day_month);
        day_wrapper.appendChild(day_slot);

        this.week.appendChild(day_wrapper);
    }
}

Calendar.prototype.draw_time_slot = function (day, element) {
    const self = this;

    const selected_slots = localStorage.getItem('availablities');
    const today_offers = this.time_slots[day.format("YYYY-MM-DD")];

    if (today_offers) {
        today_offers.forEach(function (ts) {
            const dish_names = [];
            let offer_wrapper;
            let offer_end_date;
            let offer_edit_link = createElement('a', 'offer-edit-link', "");

            if ((ts.is_copy || !ts.is_expired) && !ts.is_recurring && ts.available === ts.total_available) {
                offer_wrapper = createElement('div', 'time-slot', '', {
                    'id': ts.id,
                    'data-date': day.format("YYYY-MM-DD"),
                    'draggable': true,
                    'data-recurring': ts.is_recurring
                }, {
                    'dragstart': drag,
                });
                const offer_movable = createElement('i', 'glyphicon glyphicon-menu-hamburger', '');
                offer_wrapper.append(offer_movable);
                const offer_copiable = createElement('i', 'glyphicon glyphicon-duplicate', '');
                offer_wrapper.append(offer_copiable);
            } else {
                offer_wrapper = createElement('div', 'recurring-time-slot time-slot', '', {
                    'id': ts.id,
                    'data-date': day.format("YYYY-MM-DD"),
                    'data-recurring': ts.is_recurring
                });
                if (!ts.is_recurring) {
                    const offer_copiable = createElement('i', 'glyphicon glyphicon-duplicate', '');
                    offer_wrapper.append(offer_copiable);
                }
            }


            const offer_id = createElement('div', 'offer-id', `#${ts.id}`);
            offer_wrapper.append(offer_id);

            if (!ts.is_expired) {
                offer_end_date = createElement('div', 'offer-line-item', `<span class="offer-label">Ends</span><span class="offer-value">${get_pretty_time(ts.order_by)}</span>`);
            } else {
                offer_end_date = createElement('div', 'offer-line-item', `<span class="offer-label">Ended</span><span class="offer-value">${get_pretty_time(ts.order_by)}</span>`);
            }

            if (ts.is_recurring) {
                const end_date = moment(ts.end_date, "YYYY-MM-DD").format("MM/DD");
                const repeat_text = createElement('span', '', `Repeat every ${ts.repeat_every} until ${end_date}`);
                offer_wrapper.append(repeat_text);
            }

            if (!ts.is_expired && !ts.is_copy) {
                offer_edit_link = createElement('a', 'offer-edit-link', "Edit Offer", {
                    "href": `/offer/edit/${ts.id}?next=/my_offers/calendar`,
                });
            }

            if (ts.is_copy && !ts.is_expired) {
                offer_edit_link = createElement('a', 'delete-copy', "Delete", {
                    "href": "javascript:void(0)",
                }, {"click": deleteOffer});
            }

            ts.dishes.forEach(function (dish, i) {
                "use strict";
                dish_names.push(createElement('div', 'offer-line-item', `<span class="offer-label">Dish ${i + 1}</span><span class="offer-value">${dish.name}</span>`));
            });
            const offer_name = createElement('div', 'title', `<h3>${ts.name}</h3>`);
            const dish_image = createElement('img', 'calendar-dish-image', '', {'src': ts.dishes[0]['image']});
            const offer_available = createElement('div', 'offer-line-item', `<span class="offer-label">Available</span><span class="offer-value">${ts.available}</span>`);

            offer_wrapper.appendChild(offer_name);

            if (ts.available === 0) {
                const offer_not_available = createElement('div', '', "<h3>(SOLD OUT)</h3>");
                offer_wrapper.appendChild(offer_not_available);
            }

            offer_wrapper.appendChild(dish_image);
            offer_wrapper.appendChild(offer_available);

            if (dish_names.length > 1) {
                dish_names.forEach(function (dishName) {
                    "use strict";
                    offer_wrapper.appendChild(dishName);
                });
            }

            offer_wrapper.appendChild(offer_end_date);

            if (ts.can_deliver) {
                const delivery_times = createElement('div', 'offer-line-item', `<span class="offer-label">Delivery</span><span class="offer-value">${parse_time_range(ts.deliver_time.start, ts.deliver_time.end)}</span>`);
                offer_wrapper.appendChild(delivery_times);
            }

            if (ts.can_pickup) {
                const pickup_times = createElement('div', 'offer-line-item', `<span class="offer-label">Pickup</span><span class="offer-value">${parse_time_range(ts.pickup_time.start, ts.pickup_time.end)}</span>`);
                offer_wrapper.appendChild(pickup_times);
            }

            offer_wrapper.appendChild(offer_edit_link);

            if (!ts.is_copy && !ts.is_expired && ts.available > 0) {
                const offer_delete_link = createElement('a', 'delete-offer', "Delete", {
                    "href": `javascript:void(0)`,
                }, {"click": deleteOfferFromDB});
                offer_wrapper.appendChild(offer_delete_link);
            }

            if (selected_slots) {
                if (selected_slots.indexOf(JSON.stringify([day.format(), ts])) !== -1) {
                    offer_wrapper.className = "time-slot selected";
                }
            }
            element.appendChild(offer_wrapper);

            if (!ts.is_recurring) {
                offer_wrapper.addEventListener('click', function () {
                    self.select_time_slot(this, day);
                });
            }
        });
    }
};


// A function to add a different class to "today"
Calendar.prototype.get_day_class = function (day) {
    classes = ['day'];

    if (day.week() !== this.current.week()) {
        classes.push('other');
    } else if (today.isSame(day, 'day')) {
        classes.push('today');
    } else if (today.isAfter(day, 'day')) {
        classes.push('past');
    }
    return classes.join(' ');
}

Calendar.prototype.select_time_slot = function (element) {
    const element_data = [element.parentNode.getAttribute('data-date'), element.getAttribute('data-event')];
    if (element.className.includes('selected')) {
        element.className = "time-slot";

        // Find and remove element in selections array
        const element_index = this.selection.findIndex(function (item) {
            return JSON.stringify(item) === JSON.stringify(element_data);
        });

        this.selection.splice(element_index, 1);

        if ($(".selected").length === 0) {
            hideCopyTo();
        }
    } else if (!element.className.includes('delete-copy')) {
        showCopyTo();

        element.className = "time-slot selected";
        this.selection.push(element_data);
    }

    localStorage.setItem('availablities', JSON.stringify(this.selection));
}

function allowDrop(ev) {
    ev.preventDefault();
}

function drag(ev) {
    ev.dataTransfer.setData("id", ev.target.id);
    ev.dataTransfer.setData("date", ev.target.dataset.date);
}

function drop(ev) {
    ev.preventDefault();
    const foodId = ev.dataTransfer.getData("id");
    const prev_date = ev.dataTransfer.getData("date");
    const new_date = ev.target.dataset.date || ev.target.parentNode.dataset.date || ev.target.parentNode.parentNode.dataset.date;

    if (new_date && prev_date !== new_date) {
        for (let i = 0; i < myCalendar.time_slots[prev_date].length; i++) {
            if (myCalendar.time_slots[prev_date][i]['id'] === foodId) {
                const moved_item = myCalendar.time_slots[prev_date][i];
                moved_item['is_dirty'] = true;
                if (myCalendar.time_slots[new_date]) {
                    myCalendar.time_slots[new_date].push(moved_item);
                } else {
                    myCalendar.time_slots[new_date] = [moved_item];
                }
                myCalendar.time_slots[prev_date].splice(i, 1);

                showSave();
                break;
            }
        }
        myCalendar.draw_week();
    }
}

window.Calendar = Calendar;

function isOfferMovedBack(date, offerId) {
    if (myCalendar.offers_copy[date]) {
        return myCalendar.offers_copy[date].some(offer => offer.id === offerId);
    }
    return false;
}

function initCalendar(offers) {
    "use strict";
    myCalendar = new Calendar('#calendar', offers);
}

function getRandomID() {
    "use strict";
    return parseInt(Math.random() * 10000)
}


function deleteOffer(ev) {
    ev.stopPropagation();
    const idToDelete = ev.target.parentNode.id;
    for (let date in myCalendar.time_slots) {
        for (let i = 0; i < myCalendar.time_slots[date].length; i++) {
            if (myCalendar.time_slots[date][i]['id'] === idToDelete) {
                myCalendar.time_slots[date].splice(i, 1);
            }
        }
    }
    myCalendar.draw_week();
}

function deleteOfferFromDB(ev) {
    const idToDelete = ev.target.parentNode.id;
    const deletionDate = (ev.target.parentNode.getAttribute("data-date"));

    const $modalTemplate = $($("#offer-delete-template").html()).clone();

    if (ev.target.parentNode.getAttribute("data-recurring") === 'false') {
        $modalTemplate.find('.delete-options').hide();
    }
    BootstrapDialog.show({
        title: 'Remove Offer',
        message: $modalTemplate.prop("outerHTML"),
        closeByBackdrop: false,
        buttons: [{
            label: 'Confirm',
            cssClass: 'btn-primary',
            autospin: true,
            action: function (dialogRef) {
                dialogRef.enableButtons(false);
                dialogRef.setClosable(false);
                const isMultiple = $("#offer-delete-form").find("input[name='is_multiple']:checked").val();
                $.ajax({
                    headers: {
                        "Authorization": `Token ${getCookie("umamiAuthToken")}`,
                        "tz_offset": new Date().getTimezoneOffset()
                    },
                    type: "DELETE",
                    url: `/api/offer/${idToDelete}?d=${deletionDate}&multiDelete=${isMultiple}`,
                    success: function (data) {
                        window.location.reload();
                    }
                });
            }
        }],
    });
}


function showCopyTo() {
    "use strict";
    $("#copy-to").show();
    $("#calendar").css({"padding-bottom": "50px"});
}

function hideCopyTo() {
    "use strict";
    $("#copy-to").hide();
    $("#calendar").css({"padding-bottom": "0"});
}

function showSave() {
    "use strict";
    $("#save-button").show();
    $("#calendar").css({"padding-top": "50px"});
}

function hideSave() {
    "use strict";
    $("#save-button").hide();
    $("#calendar").css({"padding-top": "0"});
}

function isCalendarDirty() {
    const toPersist = getPersistData();
    return toPersist.move.length > 0 || toPersist.copy.length > 0;
}

function getPersistData() {
    const toPersist = {
        move: [],
        copy: [],
    };
    if (myCalendar) {
        $.each(myCalendar.time_slots, function (date, offers) {
            offers.forEach(function (offer) {
                if (offer.is_copy) {
                    toPersist.copy.push(
                        {
                            "id": offer.offer_id,
                            "date": date,
                            "label": offer.id,
                        });
                } else if (offer.is_dirty) {
                    if (!isOfferMovedBack(date, offer.id)) {
                        toPersist.move.push({
                            "id": offer.id,
                            "date": date,
                        });
                    }
                }
            });
        });
    }
    return toPersist;
}


$(function () {
    "use strict";
    $("#copy-to").datepicker({
        weekStart: 1,
        format: 'mm/dd/yyyy',
        startDate: '0d',
        endDate: '+21d',
        autoclose: true,
        todayHighlight: true
    }).on('changeDate', function (selected) {
        $('#datepicker').val("");
        const selectedDate = moment(selected.date).format("YYYY-MM-DD");
        const selectedOffers = [];
        const selectedOfferIds = [];

        $(".selected").each(function (_, selectedElem) {
            selectedOfferIds.push(selectedElem.id);
        });
        $.each(myCalendar.time_slots, function (_, offers) {
            offers.forEach(function (offer) {
                console.log(offer)
                if (selectedOfferIds.includes(offer['id'])) {
                    const copiedOffer = Object.assign({'is_copy': true, "offer_id": offer.id}, offer);
                    copiedOffer["is_dirty"] = false;
                    copiedOffer['id'] = getRandomID();
                    selectedOffers.push(copiedOffer);
                }
            });
        });


        if (myCalendar.time_slots[selectedDate]) {
            myCalendar.time_slots[selectedDate] = myCalendar.time_slots[selectedDate].concat(selectedOffers);
        } else {
            myCalendar.time_slots[selectedDate] = selectedOffers;
        }

        let delta = moment(selected.date).diff(myCalendar.current, "weeks");

        if (delta > 0) {
            while (delta--) {
                myCalendar.next_week();
            }
        } else if (delta < 0) {
            while (delta++) {
                myCalendar.prev_week();
            }
        } else {
            myCalendar.draw_week();
        }
        showSave();
    });

    $("#save-button").on("click", function () {
        const toPersist = getPersistData();
        $.ajax({
            headers: {
                "Authorization": `Token ${getCookie("umamiAuthToken")}`,
                "tz_offset": new Date().getTimezoneOffset()
            },
            type: "POST",
            url: "/api/bulk_manipulate",
            data: JSON.stringify(toPersist),
            contentType: "application/json",
            success: function (data) {
                window.location.reload();
            }
        });
    });
});