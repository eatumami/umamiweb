"use strict";
/**
 * Created by ericwang on 2019-02-01.
 */

const weekdayArray = [
    'Sun',
    'Mon',
    'Tue',
    'Wed',
    'Thu',
    'Fri',
    'Sat',
];

function isTodayDay(date) {
    const today = new moment();
    const inputDate = new moment(date, "YYYY-MM-DD").local();
    return inputDate.isSame(today, 'day');
}


function getTimeHHMM(hourOffset) {
    const today = new Date();
    const h = today.getHours() + 1 + hourOffset;
    let m = today.getMinutes();
    if (m > 30) {
        m = "30";
    } else {
        m = "00";
    }
    return `${h}:${m}`;
}

function offsetHour(time, hourOffset) {
    const [h, m] = time.split(":");
    return `${parseInt(h) + hourOffset}:${m}`;
}

function refreshDeliveryTimes() {
    const $deliveryTimeSlot = $("#delivery_time_slots");
    if ($(".delivery-checkbox").prop("checked") && chefTimeSlots && chefTimeSlots.deliver_times) {
        const time_slots_for_date = chefTimeSlots.deliver_times[$("#ready-date-input").val()];
        $deliveryTimeSlot.empty();
        if (time_slots_for_date && time_slots_for_date.length > 0) {
            hideDeliveryTimes();
            time_slots_for_date.forEach(function (slot) {
                $deliveryTimeSlot.append(`<div><label>
                                              <input type="radio" class="existing_time_slot" name="delivery_time_slot" value=${slot.id} 
                                              data-start=${get_pretty_time(slot.start)} data-end=${get_pretty_time(slot.end)}>
                                              <span>${parse_time_range(slot.start, slot.end)}</span>
                                          </label></div>`);
            });
            $($("#delivery_time_slots").children()[0]).find("input").prop("checked", "checked");
            $deliveryTimeSlot.append(`<div><label><input id="new-deliver-time-slot" name="delivery_time_slot" value="new"  type="radio"><span>New time slot</span></label></div>`);
        } else {
            showDeliveryStart();
        }
    } else if ($(".delivery-checkbox").prop("checked")) {
        showDeliveryStart();
    }
}

function refreshPickupTimes() {
    const $pickupTimeSlot = $("#pickup_time_slots");
    if ($(".pickup-checkbox").prop("checked") && chefTimeSlots && chefTimeSlots.pickup_times) {
        const time_slots_for_date = chefTimeSlots.pickup_times[$("#ready-date-input").val()];
        $pickupTimeSlot.empty();
        if (time_slots_for_date && time_slots_for_date.length > 0) {
            hidePickupTimes();
            time_slots_for_date.forEach(function (slot) {
                $pickupTimeSlot.append(`<div><label>
                                              <input type="radio" class="existing_time_slot" name="pickup_time_slot" value=${slot.id}
                                              data-start=${get_pretty_time(slot.start)} data-end=${get_pretty_time(slot.end)}>
                                              <span>${parse_time_range(slot.start, slot.end)}</span>
                                          </label></div>`);
            });
            $($("#pickup_time_slots").children()[0]).find("input").prop("checked", "checked");
            $pickupTimeSlot.append(`<div><label><input id="new-pickup-time-slot" name="pickup_time_slot" value="new" type="radio"><span>New time slot</span></label></div>`);
        } else {
            showPickupStart();
        }
    } else if ($(".pickup-checkbox").prop("checked")) {
        showPickupStart();
    }
}


function showDeliveryStart() {
    const $deliveryStartTime = $("#delivery-start-time");
    $deliveryStartTime.prop('disabled', false);
    initTimeRange($deliveryStartTime, 0);
}

function showDeliveryTimes() {
    if ($("#ready-date-input").val()) {
        refreshDeliveryTimes();
        return;
    }
    showDeliveryStart();
}

function hideDeliveryTimes() {
    $("#delivery-start-time").prop('disabled', true).val("");
    $("#delivery-end-time").prop('disabled', true).val("");
    $("#delivery_time_slots").empty();
}


function showPickupStart() {
    const $pickupStartTime = $("#pickup-start-time");
    $pickupStartTime.prop('disabled', false);
    initTimeRange($pickupStartTime, 0);
}

function showPickupTimes() {
    if ($("#ready-date-input").val()) {
        refreshPickupTimes();
        return;
    }
    showPickupStart();
}


function hidePickupTimes() {
    $("#pickup-start-time").prop('disabled', true).val("");
    $("#pickup-end-time").prop('disabled', true).val("");
    $("#pickup_time_slots").empty();
}

function hideRecurringOptions() {
    $(".recurring-field").hide();
    $("#end-date-input").prop("disabled", true);
    $("#repeat-input").prop("disabled", true);
    $("#offer-ready-date .offer-input-label").html("Date");
    const startDate = new moment().local().add(24, 'hours');
    initDatePicker($("#ready-date-input"), 21, startDate.format('Y-MM-DD'));
}

function showRecurringOptions() {
    $(".recurring-field").show();
    $("#end-date-input").prop("disabled", false);
    $("#repeat-input").prop("disabled", false);
    $("#offer-ready-date .offer-input-label").html("Start Date");
    initDatePicker($("#ready-date-input"), 100);
    initDatePicker($("#end-date-input"), 100);
}

function initDatePicker($dateSelector, endDate = 21, startDate) {
    const _startDate = startDate || "0d";
    $dateSelector
        .datepicker("destroy")
        .datepicker({
            format: 'yyyy-mm-dd',
            startDate: _startDate,
            endDate: `+${endDate}d`,
            autoclose: true,
            todayHighlight: true
        });
}

function initTimeRange($timeSelector, hourOffset, defaultMin, defaultMax) {
    "use strict";
    $timeSelector.val('').timepicker('remove');

    let minTime = defaultMin ? offsetHour(defaultMin, hourOffset) : "6:00am";
    const maxTime = defaultMax || "11:59pm";

    if (isTodayDay($("#ready-date-input").val()) && $(".recurring-choice:checked").val() === "false" &&
        !($timeSelector.is("#pickup-end-time") || $timeSelector.is("#delivery-end-time"))
    ) {
        minTime = getTimeHHMM(hourOffset)
    }
    $timeSelector.timepicker({
        'forceRoundTime': true,
        'minTime': minTime,
        'maxTime': maxTime,
        'step': "15"
    });

}

function getDeliveryEndMax(start, inc) {
    const [h, m] = start.split(":");
    return `${parseInt(h) + inc}:${m}`;
}

function handleDeliverySelect(checkbox) {
    if (checkbox.checked === true) {
        showDeliveryTimes();
    } else {
        hideDeliveryTimes();
    }
}

function handlePickupSelect(checkbox) {
    if (checkbox.checked === true) {
        showPickupTimes();
    } else {
        hidePickupTimes();
    }
}

function openFBShare(offerSocialUrl) {
    FB.ui({
        method: 'share',
        display: 'popup',
        href: offerSocialUrl,
    }, function (response) {
        window.location.href = "/my/dishes";
    });
}

function get24HourTime(timeString) {
    if (timeString.includes("am")) {
        const [h, m] = timeString.replace("am", "").split(":");
        if (parseInt(h) === 12 || parseInt(h) === 0) {
            return `00:${m}`;
        }
        return timeString.replace("am", "");
    } else {
        const [h, m] = timeString.replace("pm", "").split(":");
        if (parseInt(h) === 12) {
            return `12:${m}`
        }
        return `${parseInt(h) + 12}:${m}`
    }
}

function serializeOfferForm($form) {
    const serializedData = convertToJSON($form.serializeArray());

    serializedData.can_deliver = 'can_deliver' in serializedData;
    serializedData.can_pickup = 'can_pickup' in serializedData;
    serializedData.lead_time = serializedData.lead_time * 60 * 60;
    serializedData.tz_offset = new Date(serializedData.date).getTimezoneOffset();

    if (serializedData.can_deliver) {
        if (serializedData.delivery_time_slot === "new" || !serializedData.delivery_time_slot) {
            serializedData.deliver_time = {
                "start": get24HourTime(serializedData.deliver_start),
                "end": get24HourTime(serializedData.deliver_end),
            };
        } else {
            serializedData.delivery_time_id = serializedData.delivery_time_slot;
        }
    }
    if (serializedData.can_pickup) {
        if (serializedData.pickup_time_slot === "new" || !serializedData.pickup_time_slot) {
            serializedData.pickup_time = {
                "start": get24HourTime(serializedData.pickup_start),
                "end": get24HourTime(serializedData.pickup_end),
            };
        } else {
            serializedData.pickup_time_id = serializedData.pickup_time_slot;
        }
    }
    serializedData.is_recurring = $(".recurring-choice:checked").val() === "true";

    return serializedData;
}

function isValidDeliveryTimeRange(offerData) {
    if ($('input[name=delivery_time_slot]:checked').length === 0) {
        return true;
    }
    if (!offerData.deliver_time) {
        return !!offerData.delivery_time_id;
    }
    const deliver_start = offerData.deliver_time.start;
    const d_h = deliver_start.split(":")[0];

    return parseInt(d_h) >= 6;
}

function isValidPickupTimeRange(offerData) {
    if ($('input[name=pickup_time_slot]:checked').length === 0) {
        return true;
    }
    if (!offerData.pickup_time) {
        return !!offerData.pickup_time_id;
    }
    const pickup_start = offerData.pickup_time.start;
    const p_h = pickup_start.split(":")[0];

    return parseInt(p_h) >= 6;
}

function isValidDeliveryLeadTime(offerData) {
    if ($('input[name=delivery_time_slot]:checked').length === 0) {
        return true;
    }
    if (!offerData.deliver_time) {
        return !!offerData.delivery_time_id;
    }

    const deliver_start = offerData.deliver_time.start;
    const d_h = deliver_start.split(":")[0];

    return parseInt(d_h) >= offerData.lead_time / (60 * 60);
}

function isValidPickupLeadTime(offerData) {
    if ($('input[name=pickup_time_slot]:checked').length === 0) {
        return true;
    }
    if (!offerData.pickup_time) {
        return offerData.pickup_time_id || offerData.delivery_time_id;
    }

    const pickup_start = offerData.pickup_time.start;

    const p_h = pickup_start.split(":")[0];

    return parseInt(p_h) >= offerData.lead_time / (60 * 60);
}

$(function () {
    $("body").popover({
        selector: '[data-toggle="popover"]',
        trigger: "focus",
        html: true,
        content: function () {
        }
    });

    $("#delivery-start-time").timepicker();
    $("#delivery-end-time").timepicker();
    $("#pickup-start-time").timepicker();
    $("#pickup-end-time").timepicker();

    const $readyDateInput = $("#ready-date-input");
    initDatePicker($readyDateInput, 100);

    $readyDateInput.on('changeDate', function (result) {
        refreshDeliveryTimes();
        refreshPickupTimes();
        $(".ready-weekday").html(weekdayArray[result.date.getDay()]);
        initTimeRange($("#delivery-start-time"), 0);
        initTimeRange($("#delivery-end-time"), 1);
        initTimeRange($("#pickup-start-time"), 0);
        initTimeRange($("#pickup-end-time"), 1);
    });

    const $endDateInput = $("#end-date-input");
    initDatePicker($endDateInput, 100);
    $endDateInput.on('changeDate', function (result) {
        $(".end-weekday").html(weekdayArray[result.date.getDay()]);
    });


    $("#new-offer-form").on("submit", function (ev) {
        $(".dish-offer-submit").attr("disabled", true);
        ev.preventDefault();
        const serializedData = serializeOfferForm($(this));
        if (!serializedData.can_deliver && !serializedData.can_pickup) {
            $("#offer-form-errors").html("*Please select delivery or pickup");
            $(".dish-offer-submit").attr("disabled", false);
            return false;
        }
        if (!isValidDeliveryTimeRange(serializedData)) {
            $("#offer-form-errors").html("*Please enter a Delivery time between <b>6:00am and 11:45pm</b>");
            $(".dish-offer-submit").attr("disabled", false);
            return false;
        }
        if (!isValidPickupTimeRange(serializedData)) {
            $("#offer-form-errors").html("*Please enter a Pickup between <b>6:00am and 11:45pm</b>");
            $(".dish-offer-submit").attr("disabled", false);
            return false;
        }
        if (!isValidDeliveryLeadTime(serializedData)) {
            $("#offer-form-errors").html("*Lead time too long. Make sure it is within the same calendar day as your delivery date");
            $(".dish-offer-submit").attr("disabled", false);
            return false;
        }
        if (!isValidPickupLeadTime(serializedData)) {
            $("#offer-form-errors").html("*Lead time too long. Make sure it is within the same calendar day as your pickup date");
            $(".dish-offer-submit").attr("disabled", false);
            return false;
        }
        $.ajax({
            headers: {
                "Authorization": `Token ${getCookie("umamiAuthToken")}`,
                "tz_offset": new Date().getTimezoneOffset()
            },
            type: "POST",
            url: "/api/offer",
            data: JSON.stringify(serializedData),
            contentType: "application/json",
            success: function (data) {
                openFBShare(data.social_url);
            },
            error: function (data) {
                $(".dish-offer-submit").attr("disabled", false);
            }
        });
    });

    $("#edit-offer-form").on("submit", function (ev) {
        "use strict";
        ev.preventDefault();
        const serializedData = serializeOfferForm($(this));
        if (!serializedData.can_deliver && !serializedData.can_pickup)
            $("#offer-form-errors").html("*Please select delivery or pickup");
        else {
            $.ajax({
                headers: {
                    "Authorization": `Token ${getCookie("umamiAuthToken")}`,
                    "tz_offset": new Date().getTimezoneOffset()
                },
                type: "PUT",
                url: "/api/offer",
                data: JSON.stringify(serializedData),
                contentType: "application/json",
                success: function (data) {
                    const redirect_url = getURLParameter("next");
                    if (redirect_url) {
                        window.location.href = redirect_url;
                    } else {
                        window.location.reload();
                    }
                }
            });
        }
    });

    $("#delivery_time_slots").on("click", "#new-deliver-time-slot", function () {
        showDeliveryStart();
    }).on("click", ".existing_time_slot", function () {
        $("#delivery-start-time").prop('disabled', true);
        $("#delivery-end-time").prop('disabled', true);
    });

    $("#pickup_time_slots").on("click", "#new-pickup-time-slot", function () {
        showPickupStart();
    }).on("click", ".existing_time_slot", function () {
        $("#pickup-start-time").prop('disabled', true);
        $("#pickup-end-time").prop('disabled', true);
    });

    $("#pickup-start-time").on("change", function () {
        "use strict";
        const $pickupEndTime = $("#pickup-end-time");
        $pickupEndTime.prop('disabled', false);
        const minTime = get24HourTime($(this).val());
        initTimeRange($pickupEndTime, 1, minTime, getDeliveryEndMax(minTime, 5));
    });

    $("#delivery-start-time").on("change", function () {
        "use strict";
        const $deliveryEndTime = $("#delivery-end-time");
        $deliveryEndTime.prop('disabled', false);
        const minTime = get24HourTime($(this).val());
        initTimeRange($deliveryEndTime, 1, minTime, getDeliveryEndMax(minTime, 2));
    });
});
