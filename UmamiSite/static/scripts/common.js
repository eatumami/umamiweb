/**
 * Created by ericwang on 2018-12-31.
 */
"use strict";

const stripe_connect_client_id = "ca_EFEE9lreFsEJVtY1PjqFL2QsUFqnHJ3A";
const stripe_connect_client_id_test = "ca_EFEEGalFOYM20E77nrnwG1zdHsejqg67";

const stripe_connect_url = `https://connect.stripe.com/oauth/authorize?response_type=code&client_id=${stripe_connect_client_id}&scope=read_write`;
const stripe_connect_test_url = `https://connect.stripe.com/oauth/authorize?response_type=code&client_id=${stripe_connect_client_id_test}&scope=read_write`;


const urls_to_not_direct = ['/dish', '/bundle', '/foodpage', '/offer', '/kitchen', '/'];

function getUserAddress() {
    return localStorage.userAddress || "";
}

function getUserLongitude() {
    return localStorage.userLongitude;
}

function getUserLatitude() {
    return localStorage.userLatitude;
}

function connectWithStripe() {
    if (isProd()) {
        window.open(stripe_connect_url)
    } else {
        window.open(stripe_connect_test_url)
    }
}

function isProd() {
    return window.location.hostname === "eatumami.co"
}

moment.updateLocale('en', {
    week: {
        dow: 1 // Monday is the first day of the week.
    }
});

const popoverSettings = {
    selector: '[data-toggle="popover"]',
    trigger: "hover",
    html: true,
    content: function () {
        return $(this).attr('data-content');
    }
};

function parse_datetime(orderBy) {
    const ready_date = localStorage.overlayReadyDate || localStorage.userReadyDate;
    const _Date = moment(ready_date + orderBy, "YYYY-MM-DDHH:mm:ss").local();
    const today = moment();
    const tomorrow = today.clone().subtract(1, 'days').startOf('day');
    const nextWeek = moment().add(1, 'weeks').startOf('isoWeek');
    const endNextWeek = moment().add(1, 'weeks').endOf('isoWeek');

    if (_Date.isSame(today, 'day')) {
        return `Today at ${_Date.format('h:mma')}`
    } else if (_Date.isSame(tomorrow, 'days')) {
        return `Tomorrow at ${_Date.format('h:mma')}`;
    } else if (_Date.isBetween(nextWeek, endNextWeek, 'days', '[]')) {
        return `Next ${_Date.format("ddd")} (${_Date.format("M/D")}) at ${_Date.format('h:mma')}`;
    } else if (_Date.isSame(today, 'weeks')) {
        return `${_Date.format("ddd")} (${_Date.format("M/D")}) at ${_Date.format('h:mma')}`;
    } else {
        return `${_Date.format("MMM Do")} at ${_Date.format('h:mma')}`;
    }
}

function parse_date(date) {
    const _Date = moment(date, "YYYY-MM-DD").local();
    const today = moment();
    const tomorrow = today.clone().subtract(1, 'days').startOf('day');
    const nextWeek = moment().add(1, 'weeks').startOf('isoWeek');
    const endNextWeek = moment().add(1, 'weeks').endOf('isoWeek');

    if (_Date.isSame(today, 'day')) {
        return `Today`
    } else if (_Date.isSame(tomorrow, 'days')) {
        return `Tomorrow`;
    } else if (_Date.isBetween(nextWeek, endNextWeek, 'days', '[]')) {
        return `Next ${_Date.format("ddd")} (${_Date.format("M/D")})`;
    } else if (_Date.isSame(today, 'weeks')) {
        return `${_Date.format("ddd")} (${_Date.format("M/D")})`;
    } else {
        return `${_Date.format("MMM Do")}`;
    }
}

function get_pretty_time(time) {
    const [h, m, s] = time.split(":");
    let hour = String(h);
    let am_pm = "am";
    if (hour === "00") {
        return `12:${m}am`
    }
    if (parseInt(h) > 12) {
        am_pm = "pm";
        hour = String(parseInt(h) - 12);
    }
    if (parseInt(h) === 12) {
        am_pm = "pm";
        return `${h}:${m}${am_pm}`
    }
    if (hour[0] === "0") {
        return `${hour[1]}:${m}${am_pm}`
    }

    return `${hour}:${m}${am_pm}`;
}

function parse_time_range(start, end) {
    return `${get_pretty_time(start)}-${get_pretty_time(end)}`;
}

function parse_ready_date() {
    const ready_date = localStorage.overlayReadyDate || localStorage.userReadyDate;
    return parse_date(ready_date);
}

function loadSignUpOverlay() {
    $('#overlay-login').css({'height': '650', 'max-height': '650px'});
    $("#login-wrapper").css("display", "none");
    $("#signup-wrapper").css("display", "block");
    $("#sign-up-or-in-desc").html("Have an account?");
    $("#sign-up-or-in-button").html("Sign in");
}

function loadSignInOverlay() {
    $('#overlay-login').css({'max-height': '510px', 'max-height': '550px'});
    $("#signup-wrapper").css("display", "none");
    $("#login-wrapper").css("display", "block");
    $("#sign-up-or-in-desc").html("First time?");
    $("#sign-up-or-in-button").html("Sign up");
}

function writeCookie(key, value, days) {
    let date = new Date();

    // Default at 365 days.
    days = days || 365;

    // Get unix milliseconds at current time plus number of days
    date.setTime(+date + (days * 86400000)); //24 * 60 * 60 * 1000

    window.document.cookie = `${key}=${value}; expires=${date.toGMTString()};path=/`;
}

function getCookie(name) {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        let cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            let cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function deleteCooke(name) {
    document.cookie = `${name}=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;`;
}


function logoutUser() {
    deleteCooke("umamiAuthToken");
    $.ajax({
        type: "POST",
        url: "/api/auth/logout/",
        success: function () {
            if (urls_to_not_direct.indexOf(window.location.pathname) < 0) {
                window.location.href = '/accounts/login';
            } else {
                if (window.location.pathname === "/") {
                    setTimeout(function () {
                        displayLinkToLoginPage()
                    }, 500);
                } else if (window.location.pathname === "/foodpage") {
                    window.location.reload();
                } else {
                    setTimeout(function () {
                        displayUserLoginButton()
                    }, 500);
                }

            }
        }, error: function () {
        }
    });
}

function getURLParameter(name) {
    return new URL(window.location).searchParams.get(name) || null;
}

function displayLinkToLoginPage() {
    const loginLinkWrapper = createElement("div", "", "", {"id": "sign-in-sign-up"});
    const loginLink = createElement("a", "", "Sign In", {
        "href": "/accounts/login"
    });
    loginLinkWrapper.appendChild(loginLink);
    $(".account-wrapper")
        .html(loginLinkWrapper);
}


function displayUserLoginButton() {
    const userAccountButton = createElement("div", "user-account-button", "", {
        "data-target": "#loginModal",
        "data-toggle": "modal",
        "id": "login-modal"
    });

    const userAccountButtonInner = createElement("div", "glyphicon glyphicon-lock", "", {

        "data-toggle": "modal",
        "id": "login-modal"
    });

    const innerSpan = createElement("span", "", "SIGN IN", {"style": "margin: 0 5px;"});

    userAccountButton.appendChild(userAccountButtonInner);
    userAccountButton.appendChild(innerSpan);
    $("#account-button").html(userAccountButton);
    $(".user-action-btn").remove();
}

// A function to create html elements
function createElement(tagName, className, innerText, dataAttributes, eventListeners) {
    var html_element = document.createElement(tagName);
    if (className) {
        html_element.className = className;
    }
    if (innerText) {
        html_element.innerHTML = html_element.textContent = innerText;
    }

    for (let key in dataAttributes) {
        html_element.setAttribute(key, dataAttributes[key]);
    }

    for (let key in eventListeners) {
        html_element.addEventListener(key, eventListeners[key]);
    }

    return html_element;
}

function convertToJSON(array) {
    let i = array.length;
    const result = {};
    while (i--) {
        result[array[i]['name']] = array[i]['value'];
    }
    return result;
}


function getTotalCartQty() {
    try {
        var cart = JSON.parse(localStorage.cartFoodItems);
        var cartItems = cart.cartItems;

        if (cart.expires && moment().isAfter(moment.unix(cart.expires))) {
            localStorage.removeItem("cartFoodItems");
        } else {
            var i = cartItems.length;
            var totalCartQty = 0;
            while (i--) {
                totalCartQty += Number(cartItems[i]['qty']);
            }
            return totalCartQty
        }
    } catch {
        localStorage.removeItem("cartFoodItems");
    }
}

function navigateHome() {
    if (window.location.href.indexOf("foodpage") > -1) {
        window.location = "/";
    } else {
        window.location = localStorage.umamiHomeURL ? localStorage.umamiHomeURL : window.location.origin;
    }
}