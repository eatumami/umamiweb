import googlemaps
import requests
import json
import urllib.parse
import dateutil.parser

from django.conf import settings
from django.contrib.auth.decorators import login_required, user_passes_test
from django.http import HttpResponseRedirect
from django.shortcuts import render

from UmamiSite.helper.UmamiRequests import *
from UmamiSite.helper.OfferHelper import *
from UmamiCore import entities

stripe_connect_access_token_url = "https://connect.stripe.com/oauth/token"
gmaps = googlemaps.Client(key=settings.GOOGLE_API_KEY)


def chef_check(user):
    try:
        return entities.Kitchen.fetch_with_chef(user) is not None
    except entities.Kitchen.DoesNotExist:
        return False


def kitchen_complete(user):
    try:
        return (
            entities.Kitchen.fetch_with_chef(user).has_verification
            and entities.Kitchen.fetch_with_chef(user).has_stripe
        )
    except entities.Kitchen.DoesNotExist:
        return False


def kitchen_incomplete(user):
    return not kitchen_complete(user)


def not_chef_check(user):
    return not chef_check(user)


def partner(request):
    return render(request, "kitchen/landing.html")


def not_logged_in(user):
    return user.is_anonymous


def logged_in(user):
    return not user.is_anonymous


def is_california(user_address=None, lon=None, lat=None):
    """ Uses Google Maps api to determine address components and evaluate whether search address is in CA """
    if user_address:
        geocode_result = gmaps.geocode(user_address)
    else:
        geocode_result = gmaps.reverse_geocode((float(lat), float(lon)))
    return any(
        "California" in component["long_name"]
        for component in geocode_result[0]["address_components"]
    )


def foodpage(request):
    user_address = request.GET.get("address")
    user_lon = request.GET.get("lon")
    user_lat = request.GET.get("lat")
    date_iso = request.GET.get("d")

    client_adjusted_server_time = get_current_client_adjusted_datetime()
    parsed_date = client_adjusted_server_time.date()
    default_to_today = False

    try:
        parsed_date = dateutil.parser.parse(date_iso).date()
    except ValueError:
        default_to_today = True
    except TypeError:
        default_to_today = True

    if parsed_date < client_adjusted_server_time.date() or default_to_today:
        new_request_params = request.GET.dict()
        new_request_params["d"] = client_adjusted_server_time.strftime("%Y-%m-%d")
        params = urllib.parse.urlencode(new_request_params)
        return HttpResponseRedirect(f"/foodpage?{params}")

    if not user_address and not (user_lat and user_lon):
        # If no address is provided we cannot conduct search
        return HttpResponseRedirect("/")
    elif user_address and not (user_lat and user_lon):
        # Default case
        geocode_result = gmaps.geocode(user_address)
        if not geocode_result:
            return HttpResponseRedirect("/")
        user_lat = geocode_result[0]["geometry"]["location"]["lat"]
        user_lon = geocode_result[0]["geometry"]["location"]["lng"]
    elif not user_address and user_lat and user_lon:
        # In unlikely case that address is not available use lon and lat if they are
        reverse_geocode_result = gmaps.reverse_geocode(
            (float(user_lat), float(user_lon))
        )
        if not reverse_geocode_result:
            return HttpResponseRedirect("/")
        user_address = reverse_geocode_result[0]["formatted_address"]

    # Check to see if search address is in California
    if not is_california(user_address, user_lon, user_lat):
        return HttpResponseRedirect("/unavailable")

    offers_url = f"http://{request.META['HTTP_HOST']}/api/singles"
    bundles_url = f"http://{request.META['HTTP_HOST']}/api/bundles"

    # Since server renders the first results, it is "page 1"
    singles_offer = requests.get(
        url=offers_url,
        params={
            "page": 1,
            "lon": user_lon,
            "lat": user_lat,
            "d": date_iso,
            "address": user_address,
        },
    )
    bundles = requests.get(
        url=bundles_url,
        params={
            "lon": user_lon,
            "lat": user_lat,
            "d": date_iso,
            "flag": "all",
            "address": user_address,
        },
    )

    food_items = {
        "food_items": {"singles": singles_offer.json(), "bundles": bundles.json()}
    }
    return render(request, "main/foodpage.html", food_items)


@login_required
def checkout_view(request):
    user_url = f"http://{request.META['HTTP_HOST']}/api/user"
    cards_url = f"http://{request.META['HTTP_HOST']}/api/user/cards"
    user = get_with_header(user_url, request)
    cards = get_with_header(cards_url, request)
    if bool(user):
        return render(
            request,
            "checkout/checkout.html",
            {"last_used_address": user["last_used_address"], "cards": cards},
        )
    return render(request, "checkout/checkout.html", {})


def offer_view(request, title=None, pk=None):
    user_address = request.GET.get("address", "")
    user_lat = request.GET.get("lat", "")
    user_lon = request.GET.get("lon", "")
    url_date = request.GET.get("d", "")
    offer_url = f"http://{request.META['HTTP_HOST']}/api/offer/{pk}"
    offer = get_with_header(
        offer_url,
        request,
        params={"address": user_address, "lat": user_lat, "lon": user_lon},
    )

    dish_ids = ",".join([str(dish.get("id")) for dish in offer.get("dishes", {})])
    reviews_url = f"http://{request.META['HTTP_HOST']}/api/dish/reviews/{dish_ids}"
    reviews = get_with_header(reviews_url, request)

    if offer:
        food_item = {"food_item": offer, "reviews": reviews, "url_date": url_date}
        if offer["is_bundle"]:
            return render(request, "bundle/bundle.html", food_item)
        return render(request, "offer/offer.html", food_item)
    return render(request, "404.html", {})


def bundle_view(request, title=None, pk=None):
    user_address = request.GET.get("address", "")
    user_lat = request.GET.get("lat", "")
    user_lon = request.GET.get("lon", "")
    url_date = request.GET.get("d", "")
    url = f"http://{request.META['HTTP_HOST']}/api/bundle/{pk}"
    bundle = get_with_header(
        url, request, params={"address": user_address, "lat": user_lat, "lon": user_lon}
    )

    dish_ids = ",".join([str(dish["id"]) for dish in bundle["dishes"]])
    reviews_url = f"http://{request.META['HTTP_HOST']}/api/dish/reviews/{dish_ids}"
    reviews = get_with_header(reviews_url, request)

    if bundle:
        food_item = {"food_item": bundle, "reviews": reviews, "url_date": url_date}
        return render(request, "bundle/bundle.html", food_item)
    return render(request, "404.html", {})


def dish_view(request):
    dish_url = f"http://{request.META['HTTP_HOST']}/api/dish/{request.GET.get('id')}"
    dish = get_with_header(dish_url, request)

    reviews_url = (
        f"http://{request.META['HTTP_HOST']}/api/dish/reviews/{request.GET.get('id')}"
    )
    reviews = get_with_header(reviews_url, request)
    if dish:
        return render(
            request, "dish/dish.html", {"food_item": dish, "reviews": reviews}
        )
    return render(request, "404.html", {})


@login_required
@user_passes_test(chef_check, login_url="/partner/new")
def stripe_redirect(request):
    stripe_code = request.GET.get("code")
    if stripe_code:
        user_kitchen = entities.Kitchen.fetch_with_chef(request.user)
        payload = {
            "code": stripe_code,
            "client_secret": settings.STRIPE_SECRET_KEY,
            "grant_type": "authorization_code",
        }
        response = requests.post(
            url=stripe_connect_access_token_url, data=payload
        ).json()

        if not response.get("error"):
            user_kitchen.stripe_account_token = response.get("stripe_user_id")
            user_kitchen.stripe_access_token = response.get("access_token")
            user_kitchen.stripe_refresh_token = response.get("refresh_token")

            user_kitchen.save()
            return render(request, "account/stripe_connect_success.html", {})
        return render(request, "account/stripe_connect_failed.html", response)
    return render(request, "account/stripe_connect_failed.html", {})


@login_required
@user_passes_test(chef_check, login_url="/partner/new")
@user_passes_test(kitchen_incomplete, login_url="/accounts/dashboard")
def kitchen_not_complete(request):
    kitchen_obj = entities.Kitchen.fetch_with_chef(request.user)
    url = f"http://{request.META['HTTP_HOST']}/api/kitchen/details/"
    kitchen_data = get_with_header(url, request)

    return render(
        request,
        "kitchen/not_complete.html",
        {
            "social_url": kitchen_data['social_url'],
            "title": kitchen_obj.title,
            "need_verification": not kitchen_obj.has_verification,
            "need_stripe": not kitchen_obj.has_stripe,
        },
    )


@login_required
@user_passes_test(chef_check, login_url="/partner/new")
def kitchen_verification(request):
    return render(
        request,
        "kitchen/verification.html",
        {"kitchen": entities.Kitchen.fetch_with_chef(request.user)},
    )


@login_required
@user_passes_test(chef_check, login_url="/partner/new")
def new_dish(request):
    return render(request, "dish/new_dish.html")


@login_required
@user_passes_test(chef_check, login_url="/partner/new")
@user_passes_test(kitchen_complete, login_url="/kitchen/not_complete")
def new_offer(request, pks):
    date = request.GET.get("d", "")
    dishes_url = f"http://{request.META['HTTP_HOST']}/api/dish/use/{','.join(pks)}"
    time_slots_url = f"http://{request.META['HTTP_HOST']}/api/offer/times/"
    dish_data = get_with_header(dishes_url, request)
    time_slots_data = get_with_header(time_slots_url, request)

    if dish_data:
        dish_ids = ",".join([str(dish.get("id")) for dish in dish_data])
        result = {
            "food_item": {"dishes": dish_data, "kitchen": dish_data[0].get("kitchen")},
            "date": date,
            "time_slots": time_slots_data,
            "dish_ids": dish_ids,
        }
        return render(request, "offer/new_offer.html", result)
    return HttpResponseRedirect("/my/dishes")


@login_required
@user_passes_test(chef_check, login_url="/partner/new")
@user_passes_test(kitchen_complete, login_url="/kitchen/not_done")
def edit_offer(request, pk):
    offer_url = f"http://{request.META['HTTP_HOST']}/api/offer/use/{pk}"
    time_slots_url = f"http://{request.META['HTTP_HOST']}/api/offer/times"
    offer_data = get_with_header(offer_url, request)
    time_slots_data = get_with_header(time_slots_url, request)

    if offer_data:
        return render(
            request,
            "offer/edit_offer.html",
            {
                "food_item": offer_data,
                "dish_ids": offer_data["dishes"][0]["id"],
                "time_slots": time_slots_data,
            },
        )
    return HttpResponseRedirect("/my/dishes")


@login_required
@user_passes_test(chef_check, login_url="/partner/new")
def edit_dish(request, pk):
    url = f"http://{request.META['HTTP_HOST']}/api/dish/use/{pk}"
    data = get_with_header(url, request)

    if data:
        return render(request, "dish/edit_dish.html", {"food_item": data[0]})
    return render(request, "dish/new_dish.html")


@login_required
@user_passes_test(chef_check, login_url="/partner/new")
def offers_calendar(request):
    url = f"http://{request.META['HTTP_HOST']}/api/my/offers/all"
    data = get_with_header(url, request)
    return render(request, "offer/offers_calendar.html", {"offers": json.dumps(data)})


@login_required
@user_passes_test(chef_check, login_url="/partner/new")
def sell_dish(request):
    date = request.GET.get("d", "")
    url = f"http://{request.META['HTTP_HOST']}/api/my/dishes"
    data = get_with_header(url, request)
    return render(request, "dish/available_dishes.html", {"dishes": data, "date": date})


@login_required
@user_passes_test(not_chef_check, login_url="/accounts/dashboard")
def new_kitchen(request):
    return render(request, "kitchen/kitchen_profile.html")


def kitchen(request, title=None, pk=None):
    url = f"http://{request.META['HTTP_HOST']}/api/kitchen/{pk}"
    kitchen_data = get_with_header(url, request)
    if kitchen_data:
        data = {"kitchen": kitchen_data}
        return render(request, "kitchen/kitchen.html", data)
    return render(request, "404.html", {})


@login_required
def order(request):
    id = request.GET.get("orderId") or request.GET.get("orderIds")
    url = f"http://{request.META['HTTP_HOST']}/api/order/{id}"
    data = get_with_header(url, request)
    if data:
        if len(data) == 1:
            result = {
                "order": data[0],
                "role": "seller"
                if data[0]["seller_email"] == request.user.email
                else "buyer",
            }
            return render(request, "orders/single_order.html", result)
        return render(request, "orders/purchased_orders.html", {"orders": data})
    return render(request, "404.html", {})


@login_required
def orders(request):
    url = f"http://{request.META['HTTP_HOST']}/api/order/{request.GET.get('orderIds')}"
    data = get_with_header(url, request)
    if data:
        result = {
            "orders": data,
            "role": "seller" if data["seller_email"] == request.user.email else "buyer",
        }
        return render(request, "orders/purchased_orders.html", result)
    return render(request, "404.html", {})


@login_required
@user_passes_test(chef_check, login_url="/partner/new")
def sold_orders(request):
    url = f"http://{request.META['HTTP_HOST']}/api/sold"
    data = get_with_header(url, request)
    if data:
        return render(
            request, "orders/sold_orders.html", {"orders": data, "role": "seller"}
        )
    return render(request, "orders/no_orders.html", {})


@login_required
def purchased_orders(request):
    url = f'http://{request.META["HTTP_HOST"]}/api/purchased'
    data = get_with_header(url, request)
    if data:
        return render(
            request, "orders/purchased_orders.html", {"orders": data, "role": "buyer"}
        )
    return render(request, "orders/no_orders.html", {})


@login_required
def dashboard(request):
    if chef_check(request.user):
        url = f"http://{request.META['HTTP_HOST']}/api/kitchen/details/"
        kitchen_data = get_with_header(url, request)
        return render(request, "account/dashboard.html", {"kitchen_social_url": kitchen_data["social_url"]})
    return render(request, "account/dashboard.html")


@login_required
@user_passes_test(chef_check, login_url="/partner/new")
def upcoming(request):
    url = f'http://{request.META["HTTP_HOST"]}/api/upcoming'
    upcoming_orders = get_with_header(url, request)
    return render(
        request, "kitchen/upcoming.html", {"upcoming_orders": upcoming_orders}
    )


@login_required
def edit_profile(request):
    user_url = f'http://{request.META["HTTP_HOST"]}/api/user'
    user_d = get_with_header(user_url, request)

    return render(request, "account/edit_profile.html", {"user_info": user_d})


@login_required
@user_passes_test(chef_check, login_url="/partner/new")
def edit_kitchen(request):
    kitchen_url = f'http://{request.META["HTTP_HOST"]}/api/kitchen'
    kitchen_d = get_with_header(kitchen_url, request)

    return render(request, "kitchen/kitchen_profile.html", {"kitchen": kitchen_d})
